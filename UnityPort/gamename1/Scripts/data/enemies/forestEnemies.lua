enemies.forestEnemies = 
{
  bandit = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 16;
      creature.Stats.MaxActionPoints = 3;
      creature.Stats.Damage = 3;
      creature.Stats.CritChance = 0.2;

      creature.Animator = "Creatures/Bandit/banditAnimator";
      creature.Sprite = "Creatures/Bandit/Bandit";
      creature.AiType = "aiBandit";
      creature.Name = "Bandit";
      creature.DeathXp = 10;
      creature.SpawnData.SpawnInDesiredDirectonCoef = 2;
      creature.SpawnData.SpawnCost = 1;

      creature:AddSkill("cheapshot");
      creature:AddSkill("gangbang");
    end),
    banditSpear = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 16;
      creature.Stats.MaxActionPoints = 3;
      creature.Stats.Damage = 2;
      creature.Stats.CritChance = 0.1;

      --creature.Animator = "Creatures/Bandit/banditAnimator";
      creature.Sprite = "Creatures/BanditSpear/BanditSpear";
      creature.AiType = "aiAutoSkillUser";
      creature.Name = "Bandit spear";
      creature.DeathXp = 12;
      creature.SpawnData.SpawnInDesiredDirectonCoef = 2;
      creature.SpawnData.SpawnCost = 1.25;
      
      creature.DefaultAttackSkillId = "spearAttack";
    end),
    banditFat = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 30;
      creature.Stats.MaxActionPoints = 2;
      creature.Stats.Damage = 5;
      creature.Stats.CritChance = 0.1;
      creature.Stats.MaxStamina = 150

      creature.Stats.Protection:Add(DamageType.Physical, 0.25)

      --creature.Animator = "Creatures/BanditFat/banditAnimator";
      creature.Sprite = "Creatures/BanditFat/BanditFat";
      creature.AiType = "aiAutoSkillUser";
      creature.Name = "Bandit fat";
      creature.DeathXp = 15;
      creature.SpawnData.SpawnInDesiredDirectonCoef = 2;
      creature.SpawnData.SpawnCost = 2.75;

      creature:AddSkill("pushBack");
      creature:AddSkill("crushArmor");
    end),
  caravanProtector = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 20;
      creature.Stats.MaxActionPoints = 3;
      creature.Stats.Damage = 3;
      creature.Stats.CritChance = 0.2;

      creature.Animator = "Creatures/CaravanProtector/CaravanProtectorAnimator";
      creature.Sprite = "Creatures/CaravanProtector/CaravanProtector";
      creature.AiType = "aiHumanoid";
      creature.Name = "Caravan protector";
      creature.DeathXp = 10;
      creature.SpawnData.SpawnCost = 0.1;
    end),
  caravanMerchant = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 15;
      creature.Stats.MaxActionPoints = 4;
      creature.Stats.Damage = 1;
      creature.Stats.CritChance = 0.1;

      creature.Sprite = "Creatures/CaravanMerchant/CaravanMerchant";
      creature.Sprite = "Creatures/CaravanMerchant/CaravanMerchant";
      creature.AiType = "aiHumanoidRunAway";
      creature.Name = "Caravan merchant";
      creature.DeathXp = 3;
      creature.SpawnData.SpawnCost = 0.1;
    end),
  banditRanged = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 13;
      creature.Stats.MaxActionPoints = 3;
      creature.Stats.Damage = 2;
      creature.Stats.CritChance = 0.2;

      creature.Animator = "Creatures/BanditRanged/banditRangedAnimator";
      creature.Sprite = "Creatures/BanditRanged/banditRanged";
      creature.AiType = "aiBanditRanged";
      creature.Name = "Bandit shooter";
      creature.DeathXp = 10;
      creature.SpawnData.SpawnRange = 4;
      creature.SpawnData.SpawnCost = 1.3;

      creature.DefaultAttackSkillId = "shootArrow";
      creature:AddSkill("markTarget");
    end),
  banditLeader = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 35;
      creature.Stats.MaxActionPoints = 4;
      creature.Stats.Damage = 5;
      creature.Stats.CritChance = 0.2;

      creature.Animator = "Creatures/BanditLeader/banditLeaderAnimator";
      creature.Sprite = "Creatures/BanditLeader/BanditLeader";
      creature.AiType = "aiBandit"
      creature.Name = "Bandit leader";
      creature.DeathXp = 30
      creature.SpawnData.SpawnCost = 4;

      creature:AddSkill("slash");
    end),
  spiderRed = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 23;
      creature.Stats.MaxActionPoints = 4;
      creature.Stats.Damage = 5;
      creature.Stats.CritChance = 0.1;

      creature.Animator = "Creatures/Spider/SpiderGreenAnimator";
      creature.Sprite = "Creatures/Spider/SpiderGreen";
      creature.AiType = "aiSpider"
      creature.Name = "Spider red";
      creature.DeathXp = 15;
      creature.DefaultAttackSkillId = "poisonAttack"
      creature.SpawnData.SpawnCost = 2.1;


      creature:AddSkill("spiderwebAttack");
    end),
  spiderGreen = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 17;
      creature.Stats.MaxActionPoints = 4;
      creature.Stats.Damage = 6;
      creature.Stats.CritChance = 0.1;

      creature.Animator = "Creatures/Spider/SpiderGreenAnimator";
      creature.Sprite = "Creatures/Spider/SpiderGreen";
      creature.AiType = "aiSpider"
      creature.Name = "Spider green";
      creature.DeathXp = 20;
      creature.DefaultAttackSkillId = "poisonAttack"
      creature.SpawnData.SpawnCost = 2.1;
      
      creature:AddSkill("spiderwebAttack");
    end),
  mouseBear = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 23;
      creature.Stats.MaxActionPoints = 3;
      creature.Stats.Damage = 6;
      creature.Stats.CritChance = 0.1;
      creature.Stats.Protection:Add(DamageType.Physical, 0.35)

      creature.Animator = "Creatures/MouseBear/MouseBearAnimator";
      creature.Sprite = "Creatures/MouseBear/MouseBear";
      creature.AiType = "aiMousebear"
      creature.Name = "Mousebear";
      creature.DeathXp = 20;
      creature.SpawnData.SpawnCost = 2.7;

      creature:AddSkill("patchUp");
    end),
  mouseBearMom = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 36;
      creature.Stats.MaxActionPoints = 3;
      creature.Stats.Damage = 7;
      creature.Stats.CritChance = 0.1;
      creature.Stats.Protection:Add(DamageType.Physical, 0.5)

      --creature.Animator = "Creatures/MouseBear/MouseBearAnimator";
      creature.Sprite = "Creatures/MouseBearMom/MouseBearMom";
      creature.AiType = "aiMousebear"
      creature.Name = "Mousebear mom";
      creature.DeathXp = 60
      creature.SpawnData.SpawnCost = 5;
      
      creature:AddSkill("patchUp");
    end),
  momaSpider = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 60;
      creature.Stats.MaxActionPoints = 5;
      creature.Stats.Damage = 7;
      creature.Stats.CritChance = 0.1;
      creature.Stats.StaminaRestoring = 30

      creature.Animator = "Creatures/MomaSpider/MomaSpiderAnimator";
      creature.Sprite = "Creatures/MomaSpider/MomaSpider";
      creature.AiType = "aiMomaSpider"
      creature.DeathXp = 120
      creature.Name = "Moma spider";
      creature.SpawnData.SpawnCost = 10;
      creature:AddSkill("summonSpiderEgg");
    end),
  spiderGreenEgg = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 6;
      creature.Stats.MaxActionPoints = 1;
      creature.Stats.Damage = 0;

      creature.Sprite = "Creatures/SpiderGreenEgg/SpiderGreenEgg"
      creature.AiType = "aiSpiderEgg"
      creature.DeathXp = 0
      creature.Name = "Spider egg";
      creature.SpawnData.SpawnCost = 1;
      
      creature:AddSkill("summonSpiderGreen");
    end),
  zombie = enemies.createCreature(
    function (creature)
      creature.Stats.MaxHealth = 14;
      creature.Stats.MaxActionPoints = 2;
      creature.Stats.Damage = 3;
      creature.Stats.CritChance = 0.05;
      creature.Animator = "Creatures/Zombie/zombieAnimator";
      creature.Sprite = "Creatures/Zombie/Zombie0";
      creature.AiType = "aiAutoSkillUser";
      creature.Name = "Zombie";
      creature.DeathXp = 11;
      creature.SpawnData.SpawnCost = 1;

      creature:AddSkill("disease");
    end),
}

