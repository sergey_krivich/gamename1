enemies = {}
DamageType = GameName1.Actors.DamageType
DamageInfo = GameName1.Actors.DamageInfo

enemies.createCreature = function(fillFunction)
  local creature = GameName1.Data.UnitData()
  creature.DeathXp = 10
  creature.DeathFx = "Fx/bloodExplosion"
  creature.HitFx = "Fx/hitBloodExplosion"
  creature.Side = FightSide.Enemy
  creature.DefaultAttackSkillId = "simpleMelee"

  creature.Stats.Damage = 1;
  creature.Stats.MaxHealth = 30
  creature.Stats.MaxStamina = 100
  creature.Stats.StaminaRestoring = 10
  creature.Stats.MaxActionPoints = 3
  
  fillFunction(creature)

  creature.CurrentHp = creature.Stats.MaxHealth;
  return creature
end

require "data.enemies.forestEnemies"
require "data.enemies.playerUnits"