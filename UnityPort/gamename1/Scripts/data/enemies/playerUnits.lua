enemies.playerUnits = 
{
    warrior = enemies.createCreature(
    function (creature)
        creature.Side = FightSide.Player;

        creature.Stats.MaxHealth = 35;
        creature.Stats.MaxActionPoints = 3;
        creature.Stats.Damage = 3;
        creature.Stats.CritChance = 0.2;
        creature.Stats.Protection:Add(DamageType.Physical, 0.1)

        creature.Type = GameName1.Data.UnitType.Warrior;
        creature.Animator = "Creatures/HumanWarrior/humanWarriorAnimator";
        creature.Sprite = "Creatures/HumanWarrior/HumanWarrior2";
        creature.Name = "Warrior";
        creature:AddSkill("rapture");
        creature.SpawnData.SpawnInDesiredDirectonCoef = 1.5;
        creature.DefaultAttackSkillId = "simpleMelee"
    end),
    archer = enemies.createCreature(
    function (creature)
        creature.Side = FightSide.Player;
        
        creature.Stats.MaxHealth = 26;
        creature.Stats.MaxActionPoints = 3;
        creature.Stats.Damage = 2;
        creature.Stats.CritChance = 0.3;

        creature.Type = GameName1.Data.UnitType.Archer;
        creature.Animator = "Creatures/HumanArcher/humanArcherAnimator";
        creature.Sprite = "Creatures/HumanArcher/HumanArcher";
        creature.Name = "Arbalester";
        creature.SpawnData.SpawnRange = 1;
        creature.DefaultAttackSkillId = "shootArrow"
        creature:AddSkill("headshot");
    end),
    mage = enemies.createCreature(
    function (creature)
        creature.Side = FightSide.Player;
        creature.Stats.MaxHealth = 25;
        creature.Stats.MaxActionPoints = 3;
        creature.Stats.CritChance = 0.05;
        creature.Stats.Damage = 3;

        creature.Type = GameName1.Data.UnitType.Mage;
        creature.Animator = "Creatures/HumanMage/HumanMageAnimator";
        creature.Sprite = "Creatures/HumanMage/Mage3";
        creature.Name = "Mage";
        creature.DefaultAttackSkillId = "energyBall"
        creature.SpawnData.SpawnRange =2;

        creature:AddSkill("stoneThrow");
    end),

    warrior2 = enemies.createCreature(
    function (creature)
        creature.Side = FightSide.Player;

        creature.Stats.MaxHealth = 35;
        creature.Stats.MaxActionPoints = 3;
        creature.Stats.Damage = 3;
        creature.Stats.CritChance = 0.2;
        creature.Stats.Protection:Add(DamageType.Physical, 0.1)

        creature.Type = GameName1.Data.UnitType.Warrior;
        creature.Animator = "Creatures/HumanWarrior/humanWarriorAnimator";
        creature.Sprite = "Creatures/HumanWarrior/HumanWarrior2";
        creature.Name = "Warrior";
        creature:AddSkill("slash");
        creature.SpawnData.SpawnInDesiredDirectonCoef = 1.5;
        creature.DefaultAttackSkillId = "simpleMelee"
    end),
    archer2 = enemies.createCreature(
    function (creature)
        creature.Side = FightSide.Player;

        creature.Stats.MaxHealth = 26;
        creature.Stats.MaxActionPoints = 3;
        creature.Stats.Damage = 2;
        creature.Stats.CritChance = 0.3;

        creature.Type = GameName1.Data.UnitType.Archer;
        creature.Animator = "Creatures/HumanArcher/humanArcherAnimator";
        creature.Sprite = "Creatures/HumanArcher/HumanArcher";
        creature.Name = "Arbalester";
        creature.SpawnData.SpawnRange = 1;
        creature.DefaultAttackSkillId = "shootArrow"
        creature:AddSkill("shootNet");
    end),
    mage2 = enemies.createCreature(
    function (creature)
        creature.Side = FightSide.Player;

        creature.Stats.MaxHealth = 25;
        creature.Stats.MaxActionPoints = 3;
        creature.Stats.CritChance = 0.05;
        creature.Stats.Damage = 3;

        creature.Type = GameName1.Data.UnitType.Mage;
        creature.Animator = "Creatures/HumanMage/HumanMageAnimator";
        creature.Sprite = "Creatures/HumanMage/Mage3";
        creature.Name = "Mage";
        creature.DefaultAttackSkillId = "energyBall"
        creature.SpawnData.SpawnRange =2;
        creature:AddSkill("healOrb");
    end),
}