require("skills.targetSelectors.singleTargetSelector")

skillsTable.headshot = projectileBase:extend("headshot", {})
skillsMetaTable.headshot =
{
  Type = GameName1.Data.UnitType.Archer,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Archer},
  Description = "Precise shot to head. Deals 3x damage. Stuns shooter for 1 turn.",

  SpriteName = "skills/headshot";
  Range = 4,
  Duration = 1,

  Cooldown = 3,
  EnergyCost = 50.0,
};

function skillsTable.headshot:Inited()
  self.targetSelector = singleTargetSelector:new(self.tileMap);
  self.view = "projectiles/projectileArrow";
end

function skillsTable.headshot:GetTargetSelectorParams(data)
  return
  {
    position = data.targetTile
  };
end

function skillsTable.headshot:ApplyToCreature(creature)
  local damage = self:GetDamage();
  creature:RecieveDamageSimple(damage*3, self.creature);
end

function skillsTable.headshot:OnDoneToTiles(tiles)
  self.creature:AddStatusEffect("stun", self.creature,
    {
      duration = skillsMetaTable.headshot.Duration
    })
end
