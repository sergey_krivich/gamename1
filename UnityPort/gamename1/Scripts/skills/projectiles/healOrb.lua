require("skills.targetSelectors.singleTargetSelector")

skillsTable.healOrb = projectileBase:extend("shootArrow", {})
skillsMetaTable.healOrb = 
{
  Type = GameName1.Data.UnitType.Mage,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Mage},
  Description = "Heals target for 1x magic power",
  SpriteName = "skills/healOrb";
  Range = 2,
  Cooldown = 1,
  EnergyCost = 50.0,
};

function skillsTable.healOrb:Inited()
  self.targetSelector = singleTargetSelector:new(self.tileMap);
  self.view = "projectiles/projectileHealOrb";
end

function skillsTable.healOrb:GetTargetSelectorParams(data)
  return
  {
    position = data.targetTile
  };
end

function skillsTable.healOrb:ApplyToCreature(creature)
   creature:Heal(self.creature:GetAttackDamage());
end

function skillsTable.healOrb:OnDoneToTiles(tiles)
end
