require("skills.targetSelectors.singleTargetSelector")

skillsTable.punctureShot = projectileBase:extend("headshot", {})
skillsMetaTable.punctureShot =
{
  Type = GameName1.Data.UnitType.Archer,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Archer},
  Description = "Punctures armor ignoring 0.5x of it. Deals 1.25x weapon damage.",

  SpriteName = "skills/arrowShoot";
  Range = 4,
  Duration = 1,

  Cooldown = 3,
  EnergyCost = 50.0,
};

function skillsTable.punctureShot:Inited()
  self.targetSelector = singleTargetSelector:new(self.tileMap);
  self.view = "projectiles/projectileArrow";
end

function skillsTable.punctureShot:GetTargetSelectorParams(data)
  return
  {
    position = data.targetTile
  };
end

function skillsTable.punctureShot:ApplyToCreature(creature)
  local damageInfo = DamageInfo(self:GetDamage()*1.25, 0.5, DamageType.Physical, self.creature);
  creature:RecieveDamage(damageInfo);
end

function skillsTable.punctureShot:OnDoneToTiles(tiles)
end
