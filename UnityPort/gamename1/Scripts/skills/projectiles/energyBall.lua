require("skills.targetSelectors.singleTargetSelector")

skillsTable.energyBall = projectileBase:extend("energyBall", {})
skillsMetaTable.energyBall = 
{
  Type = GameName1.Data.UnitType.Mage,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Mage},
  Description = "Shoots small energy ball does 100% magic damage",
  SpriteName = "skills/arrowShoot";
  Range = 3
};

function skillsTable.energyBall:Inited()
  self.targetSelector = singleTargetSelector:new(self.tileMap);
  self.view = "projectiles/energyBall";
  self.speed = 2;
end

function skillsTable.energyBall:GetTargetSelectorParams(data)
  return
  {
    position = data.targetTile
  };
end

function skillsTable.energyBall:ApplyToCreature(creature)
   creature:RecieveDamageSimple(self:GetDamage(), self.creature);
end

function skillsTable.energyBall:OnDoneToTiles(tiles)
end
