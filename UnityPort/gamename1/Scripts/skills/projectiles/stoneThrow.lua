require("skills.targetSelectors.singleTargetSelector")

skillsTable.stoneThrow = projectileBase:extend("stoneThrow", {})
skillsMetaTable.stoneThrow = 
{
  Type = GameName1.Data.UnitType.Mage,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Mage},
  Description = "Throws magic stone at target dealing 0.75x magic damage. If target has full health, deals 3x damage",

  SpriteName = "skills/stoneThrow";
  Range = 4,

  Cooldown = 3,
  EnergyCost = 60.0,
};

function skillsTable.stoneThrow:Inited()
  self.targetSelector = singleTargetSelector:new(self.tileMap);
  self.view = "projectiles/projectileArrow";
end

function skillsTable.stoneThrow:GetTargetSelectorParams(data)
  return
  {
    position = data.targetTile
  };
end

function skillsTable.stoneThrow:ApplyToCreature(creature)
  local damage = self:GetDamage();
  if(creature.HealthPercent >= 0.999) then
    damage = damage * 3;
  else
    damage = damage * 0.75;
  end
  creature:RecieveDamageSimple(damage, self.creature);
end

function skillsTable.stoneThrow:OnDoneToTiles(tiles)
end
