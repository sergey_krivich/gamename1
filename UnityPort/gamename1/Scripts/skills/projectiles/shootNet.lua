require("skills.targetSelectors.singleTargetSelector")

skillsTable.shootNet = projectileBase:extend("shootArrow", {})
skillsMetaTable.shootNet = 
{
  Type = GameName1.Data.UnitType.Mage,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Archer},
  Description = "Shoots a net at the target. Reduces action count by 2x. Deals 0.25x damage.",

  SpriteName = "skills/shootNet";
  Range = 4,

  Duration = 3,
  Cooldown = 4,
  EnergyCost = 50.0,
};

function skillsTable.shootNet:Inited()
  self.targetSelector = singleTargetSelector:new(self.tileMap);
  self.view = "projectiles/projectileArrow";
end

function skillsTable.shootNet:GetTargetSelectorParams(data)
  return
  {
    position = data.targetTile
  };
end

function skillsTable.shootNet:ApplyToCreature(creature)
  local damage = self:GetDamage();
  creature:RecieveDamageSimple(damage*0.25, self.creature);

  creature:AddStatusEffect("shootingNet", self.creature,
  {
    duration = skillsMetaTable.shootNet.Duration,
    amount = 0.5
  })
end

function skillsTable.shootNet:OnDoneToTiles(tiles)
end
