require("skills.targetSelectors.singleTargetSelector")

skillsTable.shootArrow = projectileBase:extend("shootArrow", {})
skillsMetaTable.shootArrow = 
{
  Type = GameName1.Data.UnitType.Mage,
  LevelMin = 100,
  LevelMax = 100,
  AvailableClasses = {GameName1.Data.UnitType.Mage},
  Description = "Shoots arrow at target",

  SpriteName = "skills/arrowShoot";
  Range = 4
};

function skillsTable.shootArrow:Inited()
  self.targetSelector = singleTargetSelector:new(self.tileMap);
  self.view = "projectiles/projectileArrow";
end

function skillsTable.shootArrow:GetTargetSelectorParams(data)
  return
  {
    position = data.targetTile
  };
end

function skillsTable.shootArrow:ApplyToCreature(creature)
   creature:RecieveDamageSimple(self:GetDamage(), self.creature);
end

function skillsTable.shootArrow:OnDoneToTiles(tiles)
end
