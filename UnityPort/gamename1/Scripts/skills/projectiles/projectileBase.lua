require("skills.targetSelectors.rectTargetSelector")

projectileBase = skillBase:extend("projectileBase", 
{
  targetSelector = nil,
  speed = 3,
  view = "not_set",
  elapsed = 0,
})

function projectileBase:Apply(data)
  if(not self:IsInRange(data.targetTile)) then
    return false;
  end

  local projectile = GameName1.Actors.ProjectileActor(data.targetTile, self.speed, self.view);
  projectile.Position = self.creature.Position;
  projectile:Init();

  self.data = data;
  self.duration = projectile.FlightDuration;
  self.elapsed = 0;
  self.creature:AddActor(projectile);
end

function projectileBase:Update()
  self.elapsed = self.elapsed + UnityEngine.Time.deltaTime;
end

function projectileBase:IsDone()
  if(self.duration == nil) then return false end;

  if(self.elapsed >= self.duration) then
    return true;
  end

  return false;
end
function projectileBase:OnDone()
  if(self.data ~= nil) then
    self.targetSelector:Get(self:GetTargetSelectorParams(self.data),
    function (creature)
      self:ApplyToCreature(creature);
    end);

    local tiles = self.targetSelector:GetPoints(self:GetTargetSelectorParams(self.data));
    self:OnDoneToTiles(tiles)
  end
end

function projectileBase:OnDoneToTiles(tiles)
end

function projectileBase:GetTargetTiles(data)
  local tiles = self.targetSelector:GetPoints(self:GetTargetSelectorParams(data));
  return tiles;
end

--all projectile attacks should use this
function projectileBase:GetDamage()
  local targetSelector = rectTargetSelector:new(self.tileMap);
  local param = {
    position = {x = self.creature.TileX, y = self.creature.TileY},
    size = {x = 2, y = 2}
  };
  local creatures = targetSelector:Get(param);

  local damage = self.creature:GetAttackDamage();
  for _,creature in pairs(creatures) do
    if(creature.Data.Side ~= self.creature.Data.Side) then
      damage = damage * 0.5;
    end
  end

  return damage;
end

function projectileBase:GetTargetSelectorParams(data)

end


function projectileBase:ApplyToCreature(creature)

end