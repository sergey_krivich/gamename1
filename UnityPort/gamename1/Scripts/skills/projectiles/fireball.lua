require("skills.targetSelectors.rectTargetSelector")

skillsTable.fireball = projectileBase:extend("fireball", {})
skillsMetaTable.fireball = 
{
  Type = GameName1.Data.UnitType.Mage,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Mage},
  Description = "Shoots exploding fireball in 3 tile radius",

  SpriteName = "skills/fireball";
  Range = 4,
  Cooldown = 3,
  EnergyCost = 70.0,
};


function skillsTable.fireball:Inited()
  self.view = "projectiles/fireball";
  self.targetSelector = rectTargetSelector:new(self.tileMap);
end

function skillsTable.fireball:GetTargetSelectorParams(data)
  return 
  {
    position = {x = data.targetTile.X, y = data.targetTile.Y},
    size = {x = 2, y = 2}
  };
end

function skillsTable.fireball:ApplyToCreature(creature)
   creature:RecieveDamageSimple(self:GetDamage(), self.creature);
end

function skillsTable.fireball:OnDoneToTiles(tiles)
  for i,v in ipairs(tiles) do
    local fx = GameName1.Actors.FxActor("Fx/fireball_expl");
    fx.Position = GameName1.Levels.TileHelper.TileToPosition(v);
    fx:DestroyInSeconds(0.25);
    self.creature:AddActor(fx);
  end
end
