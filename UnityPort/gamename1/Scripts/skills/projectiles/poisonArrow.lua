require("skills.targetSelectors.singleTargetSelector")

skillsTable.poisonArrow = projectileBase:extend("poisonArrow", {})
skillsMetaTable.poisonArrow =
{
  Type = GameName1.Data.UnitType.Mage,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Mage},
  Description = "Shoots arrow which poisons enemy for 6 turns dealing 2 damage each turn",

  SpriteName = "skills/poisonArrow";
  Range = 4,
  Cooldown = 1,
  EnergyCost = 30.0,
};

function skillsTable.poisonArrow:Inited()
  self.targetSelector = singleTargetSelector:new(self.tileMap);
  self.view = "projectiles/projectileArrow";
end

function skillsTable.poisonArrow:GetTargetSelectorParams(data)
  return
  {
    position = data.targetTile
  };
end

function skillsTable.poisonArrow:ApplyToCreature(creature)
   creature:RecieveDamageSimple(self:GetDamage()/2, self.creature);
   creature:AddStatusEffect("effectBleed", self.creature, {damage = 2, duration = 6})
end

function skillsTable.poisonArrow:OnDoneToTiles(tiles)
end
