skillsTable.markTarget= skillBase:extend("cripple", {});
skillsMetaTable.markTarget =
{
  LevelMin = 100,
  LevelMax = 100,
  AvailableClasses = {},

  SpriteName = "skills/cripple",
  Description = "Marks target. Next hit will deal 4x damage",
  Duration = 2,
  Range = 4,
  Cooldown = 4,
  EnergyCost = 40.0,
};

function skillsTable.markTarget:Inited()

end

function skillsTable.markTarget:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:AddStatusEffect("markTarget", self.creature, {percent = 4, duration = skillsMetaTable.markTarget.Duration, ownerActorId = self.creature.Data.Id})
  end
end
