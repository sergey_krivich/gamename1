skillsTable = {}
skillsMetaTable = {}

function skillsTable.Create(name, tileMap, creature)
  local skillClass = skillsTable[name]
  local skillMeta = skillsMetaTable[name]
  if(skillClass == nil) then
    print("No skill found: " .. name)
  end
  if(skillMeta == nil) then
    print("No skillmeta found: " .. name)
  end

  local skill = skillClass:new()

  skill.SkillId = name;
  skill.creature = creature
  skill.tileMap = tileMap

  skill.Cooldown = skillMeta.Cooldown;
  skill.Range = skillMeta.Range;
  skill.EnergyCost = skillMeta.EnergyCost;
  skill.PlayDefaultAnimation = skillMeta.PlayDefaultAnimation;
  skill.PlayDefaultAnimationOutOfRange = skillMeta.PlayDefaultAnimationOutOfRange;

  skill:Inited();

  return skill
end



function skillsTable.SelectAvailable(unitType, level)
  local ret = linq.where(skillsMetaTable, function(v)
    if(not v.LevelMin or not v.LevelMax) then return false end;

    local isNeededUnitType = false;

    if(v.AvailableClasses ~= nil) then
      for _,val in pairs(v.AvailableClasses) do
        if(val == unitType) then
          isNeededUnitType = true
          break;
        end
      end
    else
      isNeededUnitType = true;
    end

    local a = level >= v.LevelMin and level <= v.LevelMax and isNeededUnitType;
    return a
  end)

	return linq.keys(ret)
end


require "skills.skillBase"
require "skills.projectiles.projectileBase"

require "skills.melee.slash"
require "skills.melee.patchUp"
require "skills.melee.mortalStrike"
require "skills.melee.pushBack"
require "skills.melee.cripple"
require "skills.melee.simpleMelee"
require "skills.melee.headKick"
require "skills.melee.rapture"
require "skills.melee.poisonAttack"
require "skills.melee.cheapshot"
require "skills.melee.gangbang"
require "skills.melee.spiderwebAttack"
require "skills.melee.disease"
require "skills.melee.crushArmor"
require "skills.melee.spearAttack"
require "skills.melee.jumpAway"

require "skills.projectiles.fireball"
require "skills.projectiles.shootArrow"
require "skills.projectiles.poisonArrow"
require "skills.projectiles.energyBall"
require "skills.projectiles.stoneThrow"
require "skills.projectiles.headshot"
require "skills.projectiles.healOrb"
require "skills.projectiles.shootNet"
require "skills.projectiles.punctureArmorShot"

require "skills.ranged.markTarget"

require "skills.summoning.summonSkill"
require "skills.summoning.summonSpiderEgg"
require "skills.summoning.summonSpiderGreen"