function rotatePoint(pointToRotate, centerPoint, angleInDegrees)
{
    local angleInRadians = angleInDegrees * (math.pi / 180);
    cosTheta = math.cos(angleInRadians);
    sinTheta = math.sin(angleInRadians);
    return Point
    {
        X =
            (cosTheta * (pointToRotate.X - centerPoint.X) -
            sinTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.X),
        Y =
            (int)
            (sinTheta * (pointToRotate.X - centerPoint.X) +
            cosTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.Y)
    };
}
