skillBase = class("SkillBase",
{


	--private
	CooldownLeft = 0;
	Cooldown = 0;
})

skillsMetaTable.skillBase =
{
  Type = GameName1.Data.UnitType.Mage,
  LevelMin = 100,
  LevelMax = 100,
  AvailableClasses = {GameName1.Data.UnitType.Mage},

	SkillId = "Not set",
	NeedsTarget = false,
	SpriteName = "empty",
	EnergyCost = 25.0,
	Cooldown = 3,
	Range = 0,
};


function skillBase:Inited()
end

function skillBase:OnTurnStart()
	self.CooldownLeft = self.CooldownLeft - 1;
	if(self.CooldownLeft < 0) then self.CooldownLeft = 0 end;
end

function skillBase:IsOnCooldown()
	return self.CooldownLeft ~= 0;
end

function skillBase:CanUse(data)
	return not self:IsOnCooldown() and self:IsInRange(data.targetTile);
end

function skillBase:Apply(data)
end

function skillBase:GetTargetTiles(data)
	return {data.targetTile};
end

function skillBase:IsDone()
	return true;
end

function skillBase:IsInRange(tile)
	if(tile == nil) then
		print("IsInRange: Tile is nil!")
		return false;
	end

	local direction = tile - self.creature.Tile;
	if (direction.Magnitude > self.Range) then
			return false;
	end

	return true;
end

function skillBase:GetSkillCost()
	if(self.EnergyCost == nil) then return 0 end

	return self.EnergyCost;
end

function skillBase:Update()
end

function skillBase:OnDone()
	self.CooldownLeft = self.Cooldown;
end