summonSkill = skillBase:extend("summonSkill", {})

function summonSkill:Inited()
  self.SpriteName = "skills/summonSkill"
  self.SummonUnit = enemies.forestEnemies.spiderGreen
  self.IgnoreObjectOnTile = false;
  self.Range = 1;
end

function summonSkill:Apply(data)
  if(self.tileMap:HasObjectsOnTile(data.targetTile) and not self.IgnoreObjectOnTile) then
   return false;
  end

  local summon = self.SummonUnit:CreateClone();
  summon.PlayerCreator = self.creature.FightPlayer;
  local creatureActor = GameName1.Actors.CreatureActor(self.tileMap, summon, data.side);
  creatureActor:SetTile(data.targetTile, true);
  self.creature:AddActor(creatureActor);

  self:OnSummon();
end

function summonSkill:GetTargetTiles(data)
  local targetSelector = singleTargetSelector:new(self.tileMap);
  local tiles = targetSelector:GetPoints({position = data.targetTile});
  return tiles;
end

function summonSkill:OnSummon()
end