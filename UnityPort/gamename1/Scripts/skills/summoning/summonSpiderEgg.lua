local summonSpiderEgg = summonSkill:extend("summonSpider", {})
skillsTable.summonSpiderEgg = summonSpiderEgg;

skillsMetaTable.summonSpiderEgg =
{
  EnergyCost = 80,
  Range = 2,
};

function skillsTable.summonSpiderEgg:Inited()
  self.SpriteName = "";
  self.SummonUnit = enemies.forestEnemies.spiderGreenEgg
end