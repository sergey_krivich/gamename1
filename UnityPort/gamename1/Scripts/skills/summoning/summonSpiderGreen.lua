local summonSpiderGreen = summonSkill:extend("summonSpiderGreen", {})
skillsTable.summonSpiderGreen = summonSpiderGreen;

skillsMetaTable.summonSpiderGreen =
{
  EnergyCost = 80,
  Range = 2,
};

function skillsTable.summonSpiderGreen:Inited()
  self.SpriteName = "";
  self.SummonUnit = enemies.forestEnemies.spiderGreen
  self.IgnoreObjectOnTile = true;
end
function skillsTable.summonSpiderGreen:OnSummon()
  self.creature:RecieveDamageSimple(self.creature.Health*10, self.creature);
end
