skillsTable.spearAttack= skillBase:extend("spearAttack", {});
skillsMetaTable.spearAttack =
{
  LevelMin = 100,
  LevelMax = 100,
  SpriteName = "skills/simpleAttackWarrior",
  AvailableClasses = {},
  Description = "Basic attack that damages enemy for 100% damage",
  Range = 2,
  Damage = 1,
  PlayDefaultAnimationOutOfRange = true
};

function skillsTable.spearAttack:Inited()

end

function skillsTable.spearAttack:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:RecieveDamageSimple(self.creature:GetAttackDamage()*skillsMetaTable.spearAttack.Damage, self.creature);
  end
end
