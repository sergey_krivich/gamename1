skillsTable.poisonAttack= skillBase:extend("poisonAttack", {});
skillsMetaTable.poisonAttack =
{
  LevelMin = 0,
  LevelMax = 10,
  SpriteName = "skills/simpleAttackWarrior",
  AvailableClasses = {},
  Description = "Deals 100% and poisons enemy for 2 turns dealing 50% damage each turn",
  Range = 1;
  Damage = 1;
  PoisDamage = 0.5;
  Duration = 2;
};

function skillsTable.poisonAttack:Inited()

end

function skillsTable.poisonAttack:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:RecieveDamageSimple(self.creature:GetAttackDamage()*skillsMetaTable.poisonAttack.Damage, self.creature);
    targetCreature:AddStatusEffect("effectBleed", self.creature,
    {
      damage = self.creature:GetAttackDamage()*skillsMetaTable.poisonAttack.PoisDamage,
      duration = skillsMetaTable.poisonAttack.Duration
    });
  end
end
