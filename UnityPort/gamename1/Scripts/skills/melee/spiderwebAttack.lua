skillsTable.spiderwebAttack = skillBase:extend("poisonAttack", {});
skillsMetaTable.spiderwebAttack =
{
  LevelMin = 0,
  LevelMax = 10,
  SpriteName = "skills/simpleAttackWarrior",
  AvailableClasses = {},
  Description = "Stuns enemy for 2 turns",
  Range = 1;
  PoisDamage = 0.5;
  Duration = 2;
  Cooldown = 4,
  EnergyCost = 40.0,
};


function skillsTable.spiderwebAttack:Inited()

end

function skillsTable.spiderwebAttack:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:AddStatusEffect("stun", self.creature,
    {
      duration = skillsMetaTable.spiderwebAttack.Duration,
      fx = "fx/spiderweb"
    });
  end
end
