skillsTable.headKick= skillBase:extend("headKick", {});
skillsMetaTable.headKick =
{
  Type = GameName1.Data.UnitType.Warrior,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Warrior},

  SpriteName = "skills/headkick";
  Description = "Stuns target for 3 turns. Deals 50% damage",
  Duration = 3;
  Range = 1;
  Damage = 0.5;
  Cooldown = 4,
  EnergyCost = 40.0,
};

function skillsTable.headKick:Inited()

end

function skillsTable.headKick:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:RecieveDamageSimple(self.creature:GetAttackDamage()*skillsMetaTable.headKick.Damage, self.creature);
    targetCreature:AddStatusEffect("stun", self.creature,
     {
       duration = skillsMetaTable.headKick.Duration
     })
  end
end
