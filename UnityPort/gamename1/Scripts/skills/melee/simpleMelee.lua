skillsTable.simpleMelee= skillBase:extend("simpleMelee", {});
skillsMetaTable.simpleMelee =
{
  Type = GameName1.Data.UnitType.Warrior,
  LevelMin = 100,
  LevelMax = 100,
  SpriteName = "skills/simpleAttackWarrior",
  AvailableClasses = {GameName1.Data.UnitType.Warrior},
  Description = "Basic attack that damages enemy for 100% damage",
  Range = 1;
  Damage = 1;
};

function skillsTable.simpleMelee:Inited()

end

function skillsTable.simpleMelee:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:RecieveDamageSimple(self.creature:GetAttackDamage()*skillsMetaTable.simpleMelee.Damage, self.creature);
  end
end
