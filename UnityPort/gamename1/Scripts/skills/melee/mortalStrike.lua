skillsTable.mortalStrike= skillBase:extend("mortalStrike", {});
skillsMetaTable.mortalStrike = 
{
  Type = GameName1.Data.UnitType.Warrior,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Warrior},

  SpriteName = "skills/mortalStrike",
  Description = "Does 200% damage if target has less then 35% hp. Does only 50% damage otherwise.",
  Range = 1,
  DamageModMax = 2,
  DamageModMin = 0.5,
  Cooldown = 2,
  EnergyCost = 70.0,
};


function skillsTable.mortalStrike:Inited()

end

function skillsTable.mortalStrike:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    local healthProportion = targetCreature.Health / targetCreature.Stats.MaxHealth;
    local damageMod = skillsMetaTable.mortalStrike.DamageModMin;
    if(healthProportion <= 0.35) then
      damageMod = skillsMetaTable.mortalStrike.DamageModMax
    end

    targetCreature:RecieveDamageSimple(self.creature:GetAttackDamage()*damageMod, self.creature);
  end
end