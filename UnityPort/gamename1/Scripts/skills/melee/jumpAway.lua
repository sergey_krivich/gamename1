skillsTable.jumpAway= skillBase:extend("jumpAway", {});
skillsMetaTable.jumpAway =
{
  Type = GameName1.Data.UnitType.Archer,
  LevelMin = 0,
  LevelMax = 10,
  SpriteName = "skills/pushBack",
  Description = "Jumps 4 tiles away",
  Range = 4,
  AvailableClasses = {GameName1.Data.UnitType.Archer},
  Cooldown = 4,
  EnergyCost = 30.0,
};

function skillsTable.jumpAway:Inited()
   self.done = false;
end

function skillsTable.jumpAway:Apply(data)
  if(self.tileMap:HasObjectsOnTileOrSolid(data.targetTile)) then
    return false;
  end

  LuaTweener.Position(self.creature, TileHelper.TileToPosition(data.targetTile), Glide.EaseType.BackInOut,1.2,
  function ()
    self.creature:SetTile(data.targetTile, true);
    self.done = true;
  end);
end

function skillsTable.jumpAway:IsDone()
  return self.done;
end