require("skills.targetSelectors.rectTargetSelector")

skillsTable.gangbang= skillBase:extend("gangbang", {});
skillsMetaTable.gangbang =
{
  Type = GameName1.Data.UnitType.Warrior,
  LevelMin = 100,
  LevelMax = 100,
  SpriteName = "skills/simpleAttackWarrior",
  AvailableClasses = {GameName1.Data.UnitType.Warrior},
  Description = "Attacks together with nearby",
  Range = 1.01,
  Damage = 1,
  Cooldown = 4,
  EnergyCost = 20.0,
  PlayDefaultAnimation = false,
};

function skillsTable.gangbang:Inited()
  self.done = false;
end

function skillsTable.gangbang:Apply(data)
  local targetSelector = rectTargetSelector:new(self.tileMap);
  local banditsAround = targetSelector:Get(
    {
      position = {x = self.creature.Tile.X, y = self.creature.Tile.Y},
      size = {x = 2, y = 2}
    });

  banditsAround = linq.whereIndexed(banditsAround, function(b) return b.Data.SkillIds:Contains("gangbang") end);
  if(#banditsAround <= 1) then return false end;

  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  local targetPosition = targetCreature.Position;
  local didGangbang = false;

  for _,actor in pairs(banditsAround) do
    local startPosition = actor.Position;
    local halfTargetPosition = UnityEngine.Vector2.Lerp(startPosition, targetPosition, 0.5);
    didGangbang = true;
    LuaTweener.Position(actor, halfTargetPosition,Glide.EaseType.BackOut,0.3,
    function ()
      if (targetCreature ~= nil) then
        targetCreature:RecieveDamageSimple(self.creature:GetAttackDamage()*skillsMetaTable.gangbang.Damage, self.creature);
      end

      LuaTweener.Position(actor, startPosition,Glide.EaseType.BackOut,0.5,
      function ()
        self.done = true;
        actor.Position = startPosition
      end);
    end);
  end

  return didGangbang;
end

function skillsTable.gangbang:IsDone()
  return self.done;
end
