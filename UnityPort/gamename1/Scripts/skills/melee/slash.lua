slashSkill = skillBase:extend("slashSkill", {})
skillsTable.slash= slashSkill;
skillsMetaTable.slash =
{
  Type = GameName1.Data.UnitType.Warrior,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Warrior},
  Description = "Make a slash attacking 3 tiles. Deals 0.5x damage initialy, but adds 0.5x damage for each additional enemy hit.",
  SpriteName = "skills/slash",
  Range = 1,
  Cooldown = 3,
  EnergyCost = 40.0,
};

function slashSkill:Inited()

end
function slashSkill:Apply(data)
  local attackTiles = self:GetTargetTiles(data)

  if(attackTiles == nil) then
    return nil;
  end

  local totalCount = -1;
  for _,attackTile in ipairs(attackTiles) do
    local targetCreature = self.tileMap:GetCreatureFromTile(attackTile);
    if (targetCreature ~= nil) then
      totalCount = totalCount + 1;
    end
  end

  for _,attackTile in ipairs(attackTiles) do
    local targetCreature = self.tileMap:GetCreatureFromTile(attackTile);
    if (targetCreature ~= nil) then
      local power = self.creature:GetAttackDamage() * (0.5 + totalCount * 0.5);
      targetCreature:RecieveDamageSimple(power, self.creature);
    end
  end
end

function slashSkill:GetTargetTiles(data)
    if (data.targetTile == nil) then
        return nil;
    end

    local direction = data.targetTile - self.creature.Tile;

    if (direction.Magnitude > 1.001 or direction.Magnitude < 0.01) then
        return nil;
    end

    local sideDirection = Point(direction.Y, direction.X);
    local attackTiles =
    {
        self.creature.Tile + direction,
        self.creature.Tile + direction + sideDirection,
        self.creature.Tile + direction - sideDirection,
    }
    return attackTiles
end
