skillsTable.crushArmor= skillBase:extend("crushArmor", {});
skillsMetaTable.crushArmor =
{
  Type = GameName1.Data.UnitType.Warrior,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Warrior},

  SpriteName = "skills/cripple",
  Description = "Crushes enemy armor, reducing it by 0.5x for 3 turns. Does 1.5x damage",
  Duration = 3,
  Range = 1,
  Damage = 1.5,
  Cooldown = 3,
  EnergyCost = 30.0,
};

function skillsTable.crushArmor:Inited()

end

function skillsTable.crushArmor:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:AddStatusEffect("crushArmor", self.creature, {percent = 0.5, duration = skillsMetaTable.crushArmor.Duration})
    targetCreature:RecieveDamageSimple(self.creature:GetAttackDamage()*skillsMetaTable.crushArmor.Damage, self.creature);
  end
end
