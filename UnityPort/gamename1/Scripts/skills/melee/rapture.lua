skillsTable.rapture= skillBase:extend("rapture", {});
skillsMetaTable.rapture =
{
  Type = GameName1.Data.UnitType.Warrior,
  LevelMin = 0,
  LevelMax = 10,
  SpriteName = "skills/rapture",
  Description = "Raptures target dealing 75% damage each turn for 3 turns",
  Duration = 3,
  AvailableClasses = {GameName1.Data.UnitType.Warrior},
  Range = 1;
  Damage = 0.75;
  Cooldown = 3,
  EnergyCost = 40.0,
};

function skillsTable.rapture:Inited()

end

function skillsTable.rapture:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:AddStatusEffect("effectBleed", self.creature,
    {
      damage = self.creature:GetAttackDamage()*skillsMetaTable.rapture.Damage,
      duration = skillsMetaTable.rapture.Duration
    })
  end
end
