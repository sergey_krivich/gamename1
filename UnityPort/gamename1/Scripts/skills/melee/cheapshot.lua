skillsTable.cheapshot= skillBase:extend("cheapshot", {});
skillsMetaTable.cheapshot =
{
  LevelMin = 0,
  LevelMax = 10,
  SpriteName = "skills/simpleAttackWarrior",
  AvailableClasses = {},
  Description = "Does 150% damage. Has 30% chance to stun.",
  Range = 1,
  Damage = 1.5,
  Cooldown = 3,
  EnergyCost = 40.0,
  StunDuration = 1,
};

function skillsTable.cheapshot:Inited()

end

function skillsTable.cheapshot:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:RecieveDamageSimple(self.creature:GetAttackDamage()*skillsMetaTable.cheapshot.Damage, self.creature);

    if(RandomTool.D:NextBool(0.3)) then
      targetCreature:AddStatusEffect("stun", self.creature,
      {
        duration = skillsMetaTable.cheapshot.StunDuration
      })
    end
  end
end
