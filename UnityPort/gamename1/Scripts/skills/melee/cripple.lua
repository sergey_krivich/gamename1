skillsTable.cripple= skillBase:extend("cripple", {});
skillsMetaTable.cripple =
{
  Type = GameName1.Data.UnitType.Warrior,
  LevelMin = 0,
  LevelMax = 10,
  AvailableClasses = {GameName1.Data.UnitType.Warrior},

  SpriteName = "skills/cripple",
  Description = "Criples target, reducing their max hp for 25% for 3 turns",
  Duration = 3,
  Range = 1,
  Damage = 0.5,
  Cooldown = 3,
  EnergyCost = 40.0,
};

function skillsTable.cripple:Inited()

end

function skillsTable.cripple:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:AddStatusEffect("wound", self.creature, {percent = 0.75, duration = skillsMetaTable.cripple.Duration})
    targetCreature:RecieveDamageSimple(self.creature:GetAttackDamage()*skillsMetaTable.cripple.Damage, self.creature);
  end
end
