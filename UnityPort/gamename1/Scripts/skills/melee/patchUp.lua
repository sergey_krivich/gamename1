skillsTable.patchUp= skillBase:extend("patchUp", {});
skillsMetaTable.patchUp =
{
  Type = GameName1.Data.UnitType.Warrior,
  LevelMin = 0,
  LevelMax = 10,
  SpriteName = "skills/patchUp",
  Description = "Heals for 5 health",
  Range = 1,
  AvailableClasses = {GameName1.Data.UnitType.Warrior},
  HealAmount = 5,
  Cooldown = 3,
  EnergyCost = 45.0,
};

function skillsTable.patchUp:Inited()

end

function skillsTable.patchUp:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:Heal(skillsMetaTable.patchUp.HealAmount);
  end
end