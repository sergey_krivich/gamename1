skillsTable.pushBack= skillBase:extend("pushBack", {});
skillsMetaTable.pushBack = 
{
  Type = GameName1.Data.UnitType.Warrior,
  LevelMin = 0,
  LevelMax = 10,
  SpriteName = "skills/pushBack",
  Description = "Pushes target back for 3 tiles. Does 1x damage",
  Range = 1,
  AvailableClasses = {GameName1.Data.UnitType.Warrior},
  PushAmount = 3,
  DamageMod = 1,
  Cooldown = 3,
  EnergyCost = 30.0,
};

function skillsTable.pushBack:Inited()
   self.done = false;
end

function skillsTable.pushBack:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:RecieveDamageSimple(self.creature:GetAttackDamage()*skillsMetaTable.pushBack.DamageMod, self.creature);

    local direction = data.targetTile - self.creature.Tile;

    local push = 0;
    local startTile = self.creature.Tile;
    local freeTile = nil;

    while self.tileMap:InRange(startTile + direction)do
      startTile = startTile + direction;
      push = push + 1;

      if(not self.tileMap:Get(startTile + direction):HasAnyTileObjects()) then
        freeTile = startTile
      end

      if(push >= skillsMetaTable.pushBack.PushAmount) then
        break;
      end
    end

    LuaTweener.Position(targetCreature, TileHelper.TileToPosition(freeTile), Glide.EaseType.BackOut,0.3,
    function ()
      targetCreature:SetTile(freeTile, true);
      self.done = true;
    end);

  else
    return false;
  end
end

function skillsTable.pushBack:IsDone()
  return self.done;
end 