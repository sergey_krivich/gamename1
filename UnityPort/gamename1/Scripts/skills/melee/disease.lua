skillsTable.disease= skillBase:extend("disease", {});
skillsMetaTable.disease =
{
  LevelMin = 100,
  LevelMax = 100,
  AvailableClasses = {},

  SpriteName = "skills/cripple",
  Description = "Makes target ill reducing damage by 0.15x",
  Duration = 3,
  Range = 1,
  Damage = 0.5,
  EnergyCost = 30.0,
  Cooldown = 1,
};

function skillsTable.disease:Inited()

end

function skillsTable.disease:Apply(data)
  local targetCreature = self.tileMap:GetCreatureFromTile(data.targetTile);
  if (targetCreature ~= nil) then
    targetCreature:AddStatusEffect("disease", self.creature, {percent = 0.85, duration = skillsMetaTable.disease.Duration})
    targetCreature:RecieveDamageSimple(self.creature:GetAttackDamage()*skillsMetaTable.disease.Damage, self.creature);
  end
end
