targetSelectorBase = class("SkillBase",
{
})

function targetSelectorBase:init(tileMap)
	self.tileMap = tileMap;
end

--selects creatures from tile map and does some operation on them
function targetSelectorBase:Get(data, handler)
	local points = self:GetPoints(data)

	if(points == nil) then
		print("points nil")
	end

	local creatures = {}
	for _,v in ipairs(points) do
		local creature = self.tileMap:GetCreatureFromTile(v)
		if(creature ~= nil) then
			table.insert(creatures, creature);
		end
	end

	if(handler ~= nil) then
		for _,c in ipairs(creatures) do
			handler(c)
		end
	end

	return creatures;
end

function targetSelectorBase:GetPoints(data)
	return {};
end
