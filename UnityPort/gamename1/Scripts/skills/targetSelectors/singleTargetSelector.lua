require("skills.targetSelectors.targetSelectorBase")

singleTargetSelector = targetSelectorBase:extend("singleTargetSelector", {})

function singleTargetSelector:GetPoints(data)
  return {data.position};
end