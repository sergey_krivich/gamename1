DevConfig = {
  FastStart = true,--auto create local server on start
  FastEvents =true,--auto select first event
  FastPreEvent = true,--auto skip hero view screen
  FastSkillLearn = true,--auto skill learn
  AutoGame = false,--gives player ai which plays battle
  LaunchEvent = "",--will always start with this event, empty if not used
  XpMult = 1.0,--by how much recieved xp is multiplied
  DropMult = 1.1,--by how much chance to recieve item is multiplied
  DifficultyMult = 2.1,--how fast difficulty goes up
  OnLaunched = function()
    consoleHelper.AddItem("goldSword");
    consoleHelper.AddItem("hpPotion");
  end
}

DevSlowConfig = {
  FastStart = true,--auto create local server on start
  FastEvents =false,--auto select first event
  FastPreEvent = false,--auto skip hero view screen
  FastSkillLearn = false,--auto skill learn
  AutoGame = false,--gives player ai which plays battle
  LaunchEvent = "",--will always start with this event, empty if not used
  XpMult = 1.0,--by how much recieved xp is multiplied
  DropMult = 1.1,--by how much chance to recieve item is multiplied
  DifficultyMult = 2.1,--how fast difficulty goes up
  OnLaunched = function()
    consoleHelper.AddItem("goldSword");
    consoleHelper.AddItem("hpPotion");
  end
}


ReleaseConfig = {
  FastStart = false,--auto create local server on start
  FastEvents =false,--auto select first event
  FastPreEvent = false,--auto skip hero view screen
  FastSkillLearn = false,--auto skill learn
  AutoGame = false,--gives player ai which plays battle
  LaunchEvent = "",--will always start with this event, empty if not used
  XpMult = 1.0,--by how much recieved xp is multiplied
  DropMult = 1.1,--by how much chance to recieve item is multiplied
  DifficultyMult = 2.1,--how fast difficulty goes up
  OnLaunched = function()
  end
}



Config = DevConfig