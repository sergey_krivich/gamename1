statusEffectTable = {}
statusEffectMetaTable = {}

function statusEffectTable.Create(name, creature, creatureCreator, tileMap)
  local effectClass = statusEffectTable[name]
  if(effectClass == nil) then
    print("Effect not found: " .. name);
  end

	if(tileMap == nil) then print(name .. " tileMap nil") end
  if(creature == nil) then print(name .. " creature nil") end

  local effect = effectClass:new()

	effect.tileMap = tileMap
  effect.creature = creature
  effect.creatureCreator = creatureCreator

  return effect;
end

require "statusEffects.statusEffectBase"
require "statusEffects.effectBleed"
require "statusEffects.wound"
require "statusEffects.stun"
require "statusEffects.shootingNet"
require "statusEffects.disease"
require "statusEffects.markTarget"
require "statusEffects.crushArmor"
