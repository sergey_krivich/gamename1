statusEffectBase = class("statusEffectBase",
{
})

function statusEffectBase:Init(data)
  if(data == nil) then
    print("status effect data nil")
    data = {}
  end
  self.data = data;
  if(not self.data.duration) then self.data.duration = 1 end
end

--on affected creature step start
function statusEffectBase:OnStepStart()
  self:DoOnStepStart();
end

--after effect was just applied
function statusEffectBase:OnStart()
  self:DoOnStart();
end

--after effect ended completely
function statusEffectBase:OnEnd()
  self:DoOnEnd();
  self:DestroyFx();
end

--these should be overridden
function statusEffectBase:DoOnStepStart()
end
function statusEffectBase:DoOnStart()
end
function statusEffectBase:DoOnEnd()
end

--Each frame
function statusEffectBase:OnUpdate()
end


function statusEffectBase:ApplyStatsChange(stats)
end

function statusEffectBase:OnTakeDamage(damageInfo)
end

function statusEffectBase:GetDuration()
  return self.data.duration;
end

function statusEffectBase:AddFx(name)
  if(self.data.fx ~= nil) then name = self.data.fx end
  if(name == nil) then return false end;

  local fx = GameName1.Actors.FxActor(name, self.creature);
  fx.Position = GameName1.Levels.TileHelper.TileToPosition(self.creature.Tile);
  self.creature:AddActor(fx);
  self.fxActor = fx;
end

function statusEffectBase:DestroyFx(name)
  if(self.fxActor) then
    self.fxActor:Destroy();
  end
end