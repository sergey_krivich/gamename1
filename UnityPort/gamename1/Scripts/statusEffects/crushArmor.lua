statusEffectTable.crushArmor = statusEffectBase:extend("crushArmor", {});

statusEffectMetaTable.crushArmor =
{
  Description = "Armor crushed"
}

function statusEffectTable.crushArmor:DoOnStart()
  self:AddFx("Fx/diseaseStatusEffect");
end

function statusEffectTable.crushArmor:ApplyStatsChange(stats)
  stats.Protection:Mul(DamageType.Physical, self.data.percent);
end
