statusEffectTable.wound = statusEffectBase:extend("wound", {});

statusEffectMetaTable.wound =
{
  Description = "Wounded"
}

function statusEffectTable.wound:ApplyStatsChange(stats)
  stats.MaxHealth = stats.MaxHealth * self.data.percent;
end
