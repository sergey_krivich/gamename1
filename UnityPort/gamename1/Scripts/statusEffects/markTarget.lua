statusEffectTable.markTarget = statusEffectBase:extend("markTarget", {});

statusEffectMetaTable.markTarget =
{
  Description = "Marked"
}

function statusEffectTable.markTarget:DoOnStart()
  self:AddFx("Fx/diseaseStatusEffect");
end

function statusEffectTable.markTarget:OnTakeDamage(damageInfo)
  if(damageInfo.Dealer.Data.Id == self.data.ownerActorId and not self.didDamage) then
    self.didDamage = true;
    self.creature:RecieveDamage(damageInfo*4)
  end
end
