statusEffectTable.shootingNet = statusEffectBase:extend("shootingNet", {});

statusEffectMetaTable.shootingNet =
{
  Description = "Neted",
}

function statusEffectTable.shootingNet:DoOnStart()
  self:AddFx("Fx/spiderweb");
end

function statusEffectTable.shootingNet:ApplyStatsChange(stats)
  stats.MaxActionPoints = stats.MaxActionPoints * self.data.amount
end
