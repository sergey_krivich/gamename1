statusEffectTable.disease = statusEffectBase:extend("disease", {});

statusEffectMetaTable.disease =
{
  Description = "Diseased"
}

function statusEffectTable.disease:DoOnStart()
  self:AddFx("Fx/diseaseStatusEffect");
end

function statusEffectTable.disease:ApplyStatsChange(stats)
  stats.Damage = stats.Damage * self.data.percent;
end
