statusEffectTable.stun = statusEffectBase:extend("effectBleed", {});

function statusEffectTable.stun:DoOnStart()
  self:AddFx("Fx/stun");
end

function statusEffectTable.stun:DoOnStepStart()
  self.creature:Skip();
end
