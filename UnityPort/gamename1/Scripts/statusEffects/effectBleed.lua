statusEffectTable.effectBleed = statusEffectBase:extend("effectBleed", {});

function statusEffectTable.effectBleed:DoOnStart()
  self:AddFx("Fx/bleedStatusEffect");
end

function statusEffectTable.effectBleed:DoOnStepStart()
  self.creature:RecieveDamageSimple(self.data.damage, self.creatureCreator);
end
