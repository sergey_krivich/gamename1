class = require "oop.30log"
linq = require "linq"

require "mathHelper"
require "consoleHelper"
require "config"
require "gameState"

local function main()
    TileHelper = GameName1.Levels.TileHelper

    require "data.enemies.enemies"

    require "statusEffects.statusEffectData"
    require "skills.skillsData"
    require "ai.aiData"
    require "levels.levelData"
    require "events.eventsData"

    require "consoleHelper"
end

main()
