aiTable.aiSpiderEgg = aiBase:extend("aiHumanPlayer", {
  elapsedTime = 0,
  timeToHatch = 3
})
function aiTable.aiSpiderEgg:Update()
  self.elapsedTime = self.elapsedTime + 1;

  if(self.elapsedTime >= self.timeToHatch) then
    self.creature:UseSkill("summonSpiderGreen", {targetTile = self.creature.Tile, side = self.creature.Data.Side});
  else
    self.creature:Skip();
  end
end