aiTable = {}

function aiTable.Create(name, fightManager, fightPlayer, creature, tileMap)
	local cl = aiTable[name]

	if(cl == nil) then print(name .. " nil") end
	if(creature == nil) then print(name .. "creature nil") end
	if(fightManager == nil) then print(name .. "fightManager nil") end
	if(fightPlayer == nil) then print(name .. "fightPlayer nil") end
	if(tileMap == nil) then print(name .. "tileMap nil") end

	local ai = cl:new()

	ai.creature = creature
	ai.fightManager = fightManager
	ai.fightPlayer = fightPlayer
	ai.tileMap = tileMap

	return ai
end

require "ai.aiBase"
require "ai.aiHumanoid"
require "ai.aiHumanoidRanged"
require "ai.aiHumanPlayer"
require "ai.aiHumanDummy"
require "ai.aiSpiderEgg"
require "ai.aiMomaSpider"
require "ai.aiHumanoidRunAway"
require "ai.aiBandit"
require "ai.aiMousebear"
require "ai.aiSpider"
require "ai.aiAutoSkillUser"
require "ai.aiBanditRanged"