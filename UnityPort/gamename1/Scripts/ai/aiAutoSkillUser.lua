aiAutoSkillUser = aiBase:extend("aiAutoSkillUser", {notMovedTimes = 0})
aiTable["aiAutoSkillUser"] = aiAutoSkillUser;

function aiAutoSkillUser:Update()
    local closestTarget = self.fightManager:GetClosestOppositeCreature(self.fightPlayer, self.creature.Tile)
    if(closestTarget == nil) then return nil end

    local x = closestTarget.TileX
    local y = closestTarget.TileY

    local allSkills = self.creature.Abilities:GetAvailableSkillsNoMain();
    local usedSkill = false;
    for skill in Slua.iter(allSkills) do
      if(self.creature:UseSkill(skill.SkillId, {targetTile = closestTarget.Tile})) then
        usedSkill = true;
      end
    end

  local d = closestTarget.Tile:Distance(self.creature.Tile);
  if(d > 1 and not usedSkill) then
    if(not self.creature:Move(x, y, 1)) then
      local dir = RandomTool.D:RandomDir2D();

      if(not self.creature:Move(self.creature.TileX + dir.x, self.creature.TileY + dir.y, 0)) then
        self.notMovedTimes = self.notMovedTimes + 1;
        if(self.notMovedTimes > 50) then
          self.notMovedTimes = 0;
          self.creature:Skip()
        end
      end
    end
  end
  if(d < 1.01 and not usedSkill and not self.creature:Move(x, y, 0)) then
    self.creature:Skip()
  end
end