aiMousebear = aiBase:extend("aiMousebear", {
	runAwayTurns = 0
})
aiTable["aiMousebear"] = aiMousebear;

function aiMousebear:Update()
	local closestTarget = self.fightManager:GetClosestOppositeCreature(self.fightPlayer, self.creature.Tile)
	if(closestTarget == nil) then return nil end

	local x = closestTarget.TileX
	local y = closestTarget.TileY

	if(self.creature.HealthPercent > 0.3 or self.runAwayTurns > 12) then
		if(not self.creature:Move(x, y, 0)) then
			self.creature:Skip()
		end
	else
		local dirX = -mathHelper.sign(x - self.creature.TileX);
		local dirY = -mathHelper.sign(y - self.creature.TileY);
		self.runAwayTurns = self.runAwayTurns + 1;
		
		if(self:DistanceToOther(closestTarget) > 4) then
			self.creature:UseSkill("patchUp", {targetTile = self.creature.Tile})
		else
			if(not self.creature:Move(self.creature.TileX + dirX, self.creature.TileY + dirY, 0)) then
				if(not self.creature:UseSkill("patchUp", {targetTile = self.creature.Tile})) then
					if(not self.creature:Move(x, y, 0)) then
						self.creature:Skip()
					end
				end
			end
		end
	end
end
