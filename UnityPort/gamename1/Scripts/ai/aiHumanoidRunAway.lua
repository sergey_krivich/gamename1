aiHumanoidRunAway = aiBase:extend("aiSimple", {})
aiTable["aiHumanoidRunAway"] = aiHumanoidRunAway;

function aiHumanoidRunAway:Update()
	local closestTarget = self.fightManager:GetClosestOppositeCreature(self.fightPlayer, self.creature.Tile)
	if(closestTarget == nil) then return nil end

	local x = closestTarget.TileX
	local y = closestTarget.TileY

	local dirX = -mathHelper.sign(x - self.creature.TileX);
	local dirY = -mathHelper.sign(y - self.creature.TileY);

	if(not self.creature:Move(self.creature.TileX + dirX, self.creature.TileY + dirY, 0)) then
		self.creature:Skip()
	end
end
