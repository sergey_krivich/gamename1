aiHumanoid = aiBase:extend("aiSimple", {})
aiTable["aiHumanoid"] = aiHumanoid;

function aiHumanoid:Update()
	local closestTarget = self.fightManager:GetClosestOppositeCreature(self.fightPlayer, self.creature.Tile)
	if(closestTarget == nil) then return nil end

	local x = closestTarget.TileX
	local y = closestTarget.TileY

	if(not self.creature:Move(x, y, 0)) then
		self.creature:Skip()
	end
end
