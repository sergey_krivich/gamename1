aiTable.aiHumanPlayer = aiBase:extend("aiHumanPlayer", {})

function aiTable.aiHumanPlayer:Update()
  self:UpdateInput();
end

function aiTable.aiHumanPlayer:UpdateInput()
  if (self.creature ~= null) then
    local input = UnityEngine.Input;
    if (input.GetMouseButtonDown(0)) then
      self:OnClickDown();
    elseif (input.GetMouseButtonUp(0)) then
      self:OnClickUp();
    elseif(input.GetMouseButtonUp(1)) then
      self.creature:ResetSkill();
    elseif (self:GetMovementInput(UnityEngine.KeyCode.LeftArrow)) then
      self:MoveInDirection(-1, 0);
    elseif (self:GetMovementInput(UnityEngine.KeyCode.RightArrow)) then
      self:MoveInDirection(1, 0);
    elseif (self:GetMovementInput(UnityEngine.KeyCode.UpArrow)) then
      self:MoveInDirection(0, 1);
    elseif (self:GetMovementInput(UnityEngine.KeyCode.DownArrow)) then
      self:MoveInDirection(0, -1);
    elseif (self:GetMovementInput(UnityEngine.KeyCode.S)) then
      self:SendSkipCmd();
    elseif (input.GetKeyDown(UnityEngine.KeyCode.E)) then
      self.creature:Use();
    elseif(input.GetKeyDown(UnityEngine.KeyCode.Alpha1)) then
      self:TrySelectSkill(0)
    elseif(input.GetKeyDown(UnityEngine.KeyCode.Alpha2)) then
      self:TrySelectSkill(1)
    elseif(input.GetKeyDown(UnityEngine.KeyCode.Alpha3)) then
      self:TrySelectSkill(2)
    elseif(input.GetKeyDown(UnityEngine.KeyCode.Alpha4)) then
      self:TrySelectSkill(3)
    elseif(input.GetKeyDown(UnityEngine.KeyCode.Alpha5)) then
      self:TrySelectSkill(4)
    elseif(input.GetKeyDown(UnityEngine.KeyCode.Alpha6)) then
      self:TrySelectSkill(5)
    end
  end
end

function aiTable.aiHumanPlayer:OnClickUp()
  if (UnityEngine.EventSystems.EventSystem.current:IsPointerOverGameObject()
      or UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject ~= nil) then
    return nil;
  end

  local posDiff = UnityEngine.Input.mousePosition - self.mousePosition;
  if(posDiff.magnitude > 50) then
    return nil
  end

  if(self.creature.Abilities.SelectedSkillId == nil) then
    local selectedTile = GameName1.Levels.TileHelper.GetTileUnderMouse();
    local creature = self.tileMap:GetCreatureFromTile(selectedTile);
    if(creature ~= nil and self.fightPlayer:IsThisPlayerCreature(creature)) then
      self:SelectCreature(creature)
    else
      self:MoveToMouse();
    end
  else
    self:UseSkill()
  end
end

function aiTable.aiHumanPlayer:OnClickDown()
  self.mousePosition = UnityEngine.Input.mousePosition;
end

function aiTable.aiHumanPlayer:MoveToMouse()
  local selectedTile = GameName1.Levels.TileHelper.GetTileUnderMouse();
  self:SendMoveCmd(selectedTile);
end

function aiTable.aiHumanPlayer:SelectCreature(creature)
    local input = GameName1.Network.Commands.ChangeCreatureCmd.Input();
    input.CreatureId = creature.Id
    GameName1.Network.Commands.LuaCommandRunner.ChangeCreatureCmd(input);
end

function aiTable.aiHumanPlayer:UseSkill()
  local tileUnderMouse = GameName1.Levels.TileHelper.GetTileUnderMouse();

  local input = GameName1.Network.Commands.UseSkillCmd.Input();
  input:SetTarget(tileUnderMouse);
  input.CreatureId = self.creature.Id;
  input.SkillId = self.creature.Abilities.SelectedSkillId;

  GameName1.Network.Commands.LuaCommandRunner.UseSkillCmd(input);
end

function aiTable.aiHumanPlayer:GetMovementInput(key)
  return UnityEngine.Input.GetKeyDown(key);
end

function aiTable.aiHumanPlayer:MoveInDirection(x, y)
  x = x + self.creature.Tile.X;
  y = y + self.creature.Tile.Y;

  self:SendMoveCmd(Point(x, y));
end

function aiTable.aiHumanPlayer:SendMoveCmd(target)
  local input = GameName1.Network.Commands.MoveCreatureCmd.Input()
  input.CreatureId = self.creature.Id;
  input.Target = target;

  GameName1.Network.Commands.LuaCommandRunner.MoveCmd(input);
end

function aiTable.aiHumanPlayer:SendSkipCmd()
  local input = GameName1.Network.Commands.CreatureSkipTurnCmd.Input()
  input.CreatureId = self.creature.Id;

  GameName1.Network.Commands.LuaCommandRunner.SkipTurnCmd(input);
end

function aiTable.aiHumanPlayer:TrySelectSkill(skillIndex)
  local available = self.creature.Abilities:GetAvailableSkills();
  local skill = available[skillIndex];
  if(skill == nil) then return false end;

  self.creature:SelectSkill(skill.SkillId);
  return true;
end
