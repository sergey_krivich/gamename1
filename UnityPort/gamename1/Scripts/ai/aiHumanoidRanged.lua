local aiHumanoidRanged = aiBase:extend("aiHumanoidRanged", {})
aiTable.aiHumanoidRanged = aiHumanoidRanged;

function aiHumanoidRanged:Update()
	local closestTarget = self.fightManager:GetClosestOppositeCreature(self.fightPlayer, self.creature.Tile)
	if(closestTarget == nil) then return nil end

	local x = closestTarget.TileX
	local y = closestTarget.TileY

	local maxRangeSkill = self:GetMaxRangedSkill();
	local d = closestTarget.Tile:Distance(self.creature.Tile);

	if(d <= maxRangeSkill.Range) then
		if(not self.creature:UseSkill(maxRangeSkill.SkillId, {targetTile = closestTarget.Tile})) then
			self.creature:Skip()
		end
	else
		if(not self.creature:Move(x, y, maxRangeSkill.Range)) then
			self.creature:Skip()
		end
	end
end

function aiHumanoidRanged:GetMaxRangedSkill()
	local skills = self.creature.Abilities:GetLuaSkills();

	local maxRangedSkill = linq.max(linq.toTable(skills),
	function (v)
		return v.Range;
	end, 0)

	return maxRangedSkill
end
