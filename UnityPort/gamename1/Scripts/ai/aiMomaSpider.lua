local aiMomaSpider = aiBase:extend("aiSimple", {})
aiTable["aiMomaSpider"] = aiMomaSpider;

function aiMomaSpider:Update()
	local closestTarget = self.fightManager:GetClosestOppositeCreature(self.fightPlayer, self.creature.Tile)
	if(closestTarget == nil) then return nil end

  local randomNearTile = Point(self.creature.TileX + RandomTool.D:NextSign(), self.creature.TileY + RandomTool.D:NextSign());

  if(not self.creature:UseSkill("summonSpiderEgg", {targetTile = randomNearTile, side = self.creature.Data.Side})) then

    if(not self.targetPoint or self.targetPoint == self.creature.Tile) then
      self.targetPoint = self.tileMap:GetFreeRandomPoint();
    end

    if(not self.creature:Move(self.targetPoint)) then
      self.creature:Skip()
    end
  end
end
