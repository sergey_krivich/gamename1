aiSpider = aiBase:extend("aiSpider", {})
aiTable["aiSpider"] = aiSpider;

function aiSpider:Update()
	local closestTarget = self.fightManager:GetClosestOppositeCreature(self.fightPlayer, self.creature.Tile)
	if(closestTarget == nil) then return nil end

	local x = closestTarget.TileX
	local y = closestTarget.TileY

  if(self.creature:UseSkill("spiderwebAttack", {targetTile = closestTarget.Tile})) then
  elseif(not self.creature:Move(x, y, 0)) then
    self.creature:Skip()
  end
end