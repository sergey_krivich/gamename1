local aiBanditRanged = aiBase:extend("aiBanditRanged", {})
aiTable.aiBanditRanged = aiBanditRanged;

function aiBanditRanged:Update()
	local closestTarget = self.fightManager:GetClosestOppositeCreature(self.fightPlayer, self.creature.Tile)
	if(closestTarget == nil) then return nil end

	local x = closestTarget.TileX
	local y = closestTarget.TileY

	local attackSkill = self.creature.Abilities:GetLuaSkill(self.creature.Data.DefaultAttackSkillId);
	local d = closestTarget.Tile:Distance(self.creature.Tile);

	local allSkills = self.creature.Abilities:GetAvailableSkillsNoMain();
	local usedSkill = false;
	for skill in Slua.iter(allSkills) do
		if(self.creature:UseSkill(skill.SkillId, {targetTile = closestTarget.Tile})) then
			usedSkill = true;
		end
	end

	if(not usedSkill and d <= attackSkill.Range) then
		if(not self.creature:UseSkill(attackSkill.SkillId, {targetTile = closestTarget.Tile})) then
			self.creature:Skip()
		end
	else
		if(not self.creature:Move(x, y, attackSkill.Range)) then
			self.creature:Skip()
		end
	end
end