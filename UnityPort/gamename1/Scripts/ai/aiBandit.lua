aiBandit = aiBase:extend("aiBandit", {})
aiTable["aiBandit"] = aiBandit;

function aiBandit:Update()
	local closestTarget = self.fightManager:GetClosestOppositeCreature(self.fightPlayer, self.creature.Tile)
	if(closestTarget == nil) then return nil end

	local x = closestTarget.TileX
	local y = closestTarget.TileY

  if(self.creature:UseSkill("gangbang", {targetTile = closestTarget.Tile})) then
  elseif(self.creature:UseSkill("cheapshot", {targetTile = closestTarget.Tile})) then
  elseif(self.creature:UseSkill("slash", {targetTile = closestTarget.Tile})) then
  elseif(not self.creature:Move(x, y, 0)) then
    self.creature:Skip()
  end
end