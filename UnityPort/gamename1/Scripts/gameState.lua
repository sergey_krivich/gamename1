GameState = {}
GameState.Get = function()
  local gameState = LuaStaticHelper.GetGameState();
  if(gameState == nil) then print("could not get game state!"); end
  return gameState;
end