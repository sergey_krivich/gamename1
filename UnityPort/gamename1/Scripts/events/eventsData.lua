eventsTable = {}
eventsMetaTable = {}

function eventsTable.Create(name)
	local cl = eventsTable[name]
  if(cl == nil) then print("event not found " .. name) end
	local ev = cl:new()
  ev:Init();
	return ev
end

function eventsTable.SelectAvailable(difficulty)
  local ret = linq.where(eventsMetaTable, function(v)
    local a = difficulty >= v.MinDifficulty and difficulty <= v.MaxDifficulty;
    return a
  end)

	return linq.keys(ret)
end

require("events.difficulty")
require("events.items")
require("events.rewardsGen")
require("events.eventBase")
require("events.forest.banditForestAttack")
require("events.forest.banditForestAttack_2")
require("events.forest.spiderForestAttack")
require("events.forest.caravanRobberyForest")
require("events.forest.mouseBearAttack")
require("events.forest.momaSpiderForestAttack")
require("events.forest.cave")
require("events.forest.trapDoor")