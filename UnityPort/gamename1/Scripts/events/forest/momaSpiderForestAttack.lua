eventsTable.momaSpiderForestAttack = eventBase:extend("momaSpiderForestAttack", {
  Description = "You stumble across a huge moma spider! It is producing more spider eggs!"
});

eventsMetaTable.momaSpiderForestAttack = {
  MinDifficulty = 5,
  MaxDifficulty = 20,
}

function eventsTable.momaSpiderForestAttack:Build()
  self:Choice("Fight it")
    :Fight(0.5, "forest", "You attack", self:GetFight(), rewards.rewardSimple(0));

  self:Choice("Try to take valuables in webs")
    :Outcome(0.3, "You take all you could unnoticed", rewards.rewardSimple(0))
    :Fight(0.7, "forest", "As you touch some gold in web, moma immidiatly notices you and attacks!", self:GetFight(), rewards.rewardSimple(0))

  self:Choice("Try to sneak by unnoticed")
    :Outcome(0.9, "You sneak by unnoticed", rewards.rewardSimple(0))
    :Fight(0.1, "forest", "You try to sneak by, but moma notices you. It thinks you are food!", self:GetFight(), rewards.rewardSimple(0))
end

function eventsTable.momaSpiderForestAttack:GetFight()



  return {
    From = 9 + GameState.Get().Difficulty/1.8,
    To = 11 + GameState.Get().Difficulty/1.6,
    Max = 24,
    Types = {
      {Enemy = enemies.forestEnemies.momaSpider, Chance = 1, Min = 1},
      {Enemy = enemies.forestEnemies.spiderGreenEgg, Chance = 0.2},
      {Enemy = enemies.forestEnemies.spiderGreen, Chance = 0.2},
    }
  };
end