eventsTable.spiderForestAttack = eventBase:extend("spiderForestAttack", {
  Description = "Spiders crawl down on your heads!"
});

eventsMetaTable.spiderForestAttack = {
  MinDifficulty = 1,
  MaxDifficulty = 20,
}

function eventsTable.spiderForestAttack:Build()
  self:Choice("Fight")
    :Fight(0.5, "forest", "You attack", self:GetFight())
    :SetLevelParams({SpawnRange = 0})
    :FightOutcome(0.1, "You gather some stuff from web cocoons", rewards.rewardSimple());


  self:Choice("Flee")
    :Outcome(0.5, "You start to run and seem to lose them in sight", rewards.rewardSimple(0))
end

function eventsTable.spiderForestAttack:GetFight()
  return {
    From = 4 + GameState.Get().Difficulty/1.7,
    To = 4.5 + GameState.Get().Difficulty/1.6,
    Max = 24,
    Types = {
      {Enemy = enemies.forestEnemies.spiderGreen, Chance = 1, Min = 1},
      {Enemy = enemies.forestEnemies.spiderGreenEgg, Chance = 0.4},
    }
  };
end