eventsTable.caravanRobberyForest = eventBase:extend("caravanRobberyForest", {
  Description = "You see bandits robbing caravan ahead of the road! Caravan has some protection, but it is probably not enough."
});

eventsMetaTable.caravanRobberyForest = {
  MinDifficulty = 2,
  MaxDifficulty = 20,
}

function eventsTable.caravanRobberyForest:Build()

  self:Choice("Help caravan and fight bandits")
    :Fight(0.5, "forest", "You are rushing to help!", self:GetFight(FightSide.Player))
    :FightOutcome(0.1, "Merchants thanks you and gives you a nice reward", rewards.rewardSimple(), {self:UnitAliveCondition(true, "Caravan merchant")})
    :FightOutcome(0.05, "Caravan protector sees the might of your group, and decides to join you.", rewards.rewardRandomLowLevelUnit(), {self:UnitAliveCondition(true, "Caravan protector")})
    :FightOutcome(0.2, "Merchants thanks but says he has nothing to give you. You scrap what is left from bandits", rewards.rewardSimple(), {self:UnitAliveCondition(true, "Caravan merchant")})
    :FightOutcome(0.5, "You scrap all that left from destroyed caravan", rewards.rewardSimple(), {self:UnitAliveCondition(false, "Caravan merchant")})
    :SetLevelParams({GenFx = self.GenFx})

  self:Choice("Wait bandits to finish off caravan guard, and rob them!")
    :Fight(0.5, "forest", "You see caravan merchants getting killed mercilessly, and come out out of bushes to finish the job!", self:GetFight(FightSide.Enemy))
    :SetLevelParams({GenFx = self.GenFx});


  self:Choice("Ignore them")
    :Outcome(1, "As you walk away you hear screams for help. Are those merchant's screams? Or bandits'? You will never know...")
end

function eventsTable.caravanRobberyForest.GenFx(level)
  local cart = GameName1.Actors.FxActor("Props/CaravanCart");
  cart.Position = GameName1.Levels.TileHelper.TileToPosition(level:GetSpawnPoint(1).Tile);
  cart.Depth = 0.02;
  return {cart};
end

function eventsTable.caravanRobberyForest:UnitAliveCondition(alive, name)
  return{
    Check = function (data)
      local player = data:OtherFightPlayerWithSide(FightSide.Player);
      local aliveCreatures = linq.toTable(player.AliveCreatures);
      if(linq.any(aliveCreatures, function(v) return v.Data.Name == "Caravan merchant" end)) then
        return alive;
      end

      return not alive;
    end
  }
end

function eventsTable.caravanRobberyForest:GetFight(caravanSide)
  local caravanMod = 1
  if(caravanSide == FightSide.Enemy) then caravanMod = 0 end

    return {
      From = 2.5 + GameState.Get().Difficulty/1.6,
      To = 3.5 + GameState.Get().Difficulty/1.5,
      Max = 24,
      Types = {
        {Enemy = enemies.forestEnemies.bandit, Chance = 0.8},
        {Enemy = enemies.forestEnemies.banditSpear, Chance = 0.7},
        {Enemy = enemies.forestEnemies.banditRanged, Chance = 0.6},
        {Enemy = enemies.forestEnemies.banditFat, Chance = 0.6},
        {Enemy = enemies.forestEnemies.caravanProtector, Min = RandomTool.D:NextInt(1,2)*caravanMod, Side = caravanSide, Max = 0},
        {Enemy = enemies.forestEnemies.caravanMerchant, Min = 1*caravanMod, Side = caravanSide, Max = 0}
      }
    };
end