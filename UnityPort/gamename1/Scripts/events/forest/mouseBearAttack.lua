eventsTable.mouseBearAttack = eventBase:extend("mouseBearAttack", {
  Description = "Mousbears are attacking you!"
});

eventsMetaTable.mouseBearAttack = {
  MinDifficulty = 1,
  MaxDifficulty = 10,
}

function eventsTable.mouseBearAttack:Build()
  self:Choice("Fight")
    :Fight(0.5, "forest", "You attack", self:GetFight());

  self:Choice("Flee")
    :Outcome(0.5, "You start to run and seem to lose them in sight")
    :Fight(0.5, "forest", "You try to run, but mousebears keep up with you!", self:GetFight())
    :SetLevelParams({SpawnRange = 2});
end

function eventsTable.mouseBearAttack:GetFight()
  return {
    From = 6 + GameState.Get().Difficulty/1.8,
    To = 7 + GameState.Get().Difficulty/1.6,
    Max = 24,
    Types = {
      {Enemy = enemies.forestEnemies.mouseBear, Chance = 1},
      {Enemy = enemies.forestEnemies.mouseBearMom, Chance = 0.05},
    }
  };
end