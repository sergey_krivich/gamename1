eventsTable.banditForestAttack_2 = eventBase:extend("banditForestAttack_2", {
  Description = "You were ambushed by big group of bandits!"
});

eventsMetaTable.banditForestAttack_2 = {
  MinDifficulty = 3,
  MaxDifficulty = 10,
}

function eventsTable.banditForestAttack_2:Build()
  self:Choice("Fight")
    :Fight(0.5, "forest", "You attack", self:GetFight(), rewards.rewardSimple())
    :SetLevelParams({SpawnRange = 3});

  self:Choice("Flee")
    :Outcome(0.3, "You start to run and seem to lose bandits off your tail", rewards.rewardSimple())
    :Fight(0.7, "forest", "You try to run, but bandits encircle you!", self:GetFight(), rewards.rewardSimple())
    :SetLevelParams({SpawnRange = 2});
end

function eventsTable.banditForestAttack_2:GetFight()
    return {
      From = 5 + GameState.Get().Difficulty/1.8,
      To = 5.5 + GameState.Get().Difficulty/1.6,
      Max = 24,
      Types = {
        {Enemy = enemies.forestEnemies.bandit, Chance = 0.8},
        {Enemy = enemies.forestEnemies.banditSpear, Chance = 0.7},
        {Enemy = enemies.forestEnemies.banditRanged, Chance = 0.6},
        {Enemy = enemies.forestEnemies.banditFat, Chance = 0.8},
        {Enemy = enemies.forestEnemies.banditLeader, Chance = 0.2, Min = 1},
      }
    };
end