eventsTable.banditForestAttack = eventBase:extend("banditForestAttack", {
  Description = "You were ambushed by a group of bandits!"
});

eventsMetaTable.banditForestAttack = {
  MinDifficulty = 0,
  MaxDifficulty = 20,
}

function eventsTable.banditForestAttack:Build()

  self:Choice("Fight")
    :Fight(0.5, "forest", "You fight!", self:GetFight())
    :SetLevelParams({SpawnRange = 3})
    :FightOutcome(0.1, "You gather what's left from bandits and leave their corpses to rot", rewards.rewardSimple())

  self:Choice("Flee")
    :Outcome(0.5, "You start to run and seem to lose bandits off your tail")
    :Fight(0.5, "forest", "You try to run, but bandits reach you!", self:GetFight())
    :SetLevelParams({SpawnRange = 2});
end

function eventsTable.banditForestAttack:GetFight()
  return {
      From = 2.5 + GameState.Get().Difficulty/1.7,
      To = 2.5 + GameState.Get().Difficulty/1.5,
      Max = 20,
      Types = {
        {Enemy = enemies.forestEnemies.bandit, Chance = 0.8},
        {Enemy = enemies.forestEnemies.banditSpear, Chance = 0.7},
        {Enemy = enemies.forestEnemies.banditRanged, Chance = 0.65},
        {Enemy = enemies.forestEnemies.banditFat, Chance = 0.65},
      }
    };
end

