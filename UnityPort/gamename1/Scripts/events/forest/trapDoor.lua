eventsTable.trapDoor = eventBase:extend("trapDoor", {
  Description = "You stumble across a hidden trap door. You wonder what could be hidden in there?"
});

eventsMetaTable.trapDoor = {
  MinDifficulty = 1,
  MaxDifficulty = 20,
}

function eventsTable.trapDoor:Build()

  self:Choice("Open and look what's in there")
    :Outcome(0.4, "You remove grass from the door and open it. You find some valuables inside.", rewards.rewardSimple(0))
    :Outcome(0.6, "You open the door but there seems to be nothing in there")
    :Outcome(0.01, "You open the door and a trap is triggered! {0} was damaged for {1} health")--todo: damage to creatures
    :Fight(0.2, "cave", "You open the door and something grabs you and pull you in!", self:GetFightZombie())
    :FightOutcome(1, "You find some valuables in cave", rewards.rewardSimple())
    :Fight(0.2, "cave","You open the door and huge spider pulls you in!", self:GetFightSpiders())
    :FightOutcome(1, "You find some valuables in cave", rewards.rewardSimple())
    :SetLevelParams({SpawnRange = 1});

  self:Choice("Ignore it")
    :Outcome(0.5, "You ignore it, but have a feeling that you could have found something valuable in there")
    :Outcome(0.5, "Your travel goes on")
end


function eventsTable.trapDoor:GetFightZombie()
  return {
    From = 4 + GameState.Get().Difficulty/1.7,
    To = 6 + GameState.Get().Difficulty/1.6,
    Max = 24,
    Types = {
      {Enemy = enemies.forestEnemies.zombie, Chance = 1, Min = 1},
    }
  };
end

function eventsTable.trapDoor:GetFightSpiders()
  return {
    From = 4 + GameState.Get().Difficulty/1.7,
    To = 4.5 + GameState.Get().Difficulty/1.6,
    Max = 24,
    Types = {
      {Enemy = enemies.forestEnemies.spiderGreen, Chance = 1, Min = 1},
      {Enemy = enemies.forestEnemies.spiderGreenEgg, Chance = 0.4},
    }
  };
end