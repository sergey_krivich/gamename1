eventsTable.cave = eventBase:extend("trapDoor", {
  Description = "You see a dark cave ahead, should you investigate what is inside?"
});

eventsMetaTable.cave = {
  MinDifficulty = 1,
  MaxDifficulty = 20,
}

function eventsTable.cave:Build()

  self:Choice("Enter ")
    :Outcome(0.5, "You enter the cave. In the darkness you can see something shining from your torch!", rewards.rewardSimple())
    :Outcome(0.6, "You walk around a cave, but there is nothing in particular intersting")
    :Fight(0.1, "cave", "You enter a cave, but turns out its mousebear lair!", self:GetFightMouseBear(), rewards.rewardSimple())
    :Fight(0.1, "cave","You enter a cave, but turns out its spider lair!", self:GetFightSpiders(), rewards.rewardSimple())
    :SetLevelParams({SpawnRange = 3});

  self:Choice("Continue movement")
    :Outcome(1, "You continue movement")
end

function eventsTable.cave:GetFightMouseBear()

  return {
    From = 3.5 + GameState.Get().Difficulty/1.6,
    To = 4.5 + GameState.Get().Difficulty/1.5,
    Max = 24,
    Types = {
      {Enemy = enemies.forestEnemies.mouseBear, Chance = 1},
      {Enemy = enemies.forestEnemies.mouseBearMom, Chance = 0.8},
    }
  };
end

function eventsTable.cave:GetFightSpiders()

  return {
    From = 4 + GameState.Get().Difficulty/1.6,
    To = 4.5 + GameState.Get().Difficulty/1.5,
    Max = 24,
    Types = {
      {Enemy = enemies.forestEnemies.spiderGreen, Chance = 1, Min = 1},
      {Enemy = enemies.forestEnemies.spiderGreenEgg, Chance = 0.4},
    }
  };
end