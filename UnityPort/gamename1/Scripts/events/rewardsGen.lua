rewards = {};

local function GetRewardFunction(f)
  return function ()
    local rewardData = f(GameState.Get().Difficulty);

    if(rewardData == nil) then
      print("reward data is nill")
    end

    local reward = GameName1.Data.EventOutcomeReward()

    for _,v in ipairs(rewardData) do
      if(v.Chance == nil or RandomTool.D:NextBool(v.Chance*Config.DropMult)) then
        if(v.Type ~= nil) then
          local count = RandomTool.D:NextSingle(v.CountMin, v.CountMax);
          local item = items.GetItemType(v.Type);
          reward:AddItem(item, count)
        end

        if(v.Unit ~= nil) then
          reward:AddUnit(v.Unit);
        end
      end
    end

    return reward;
  end
end

local function Item(name, min, max, chance)
  if(min == nil or max == nil or name == nil) then
    print("Something is nil in item function!")
  end

  return
  {
    Type = name,
    CountMin = min,
    CountMax = max,
    Chance = chance
  }
end

local function Unit(type, chance)
  return
  {
    Unit = type,
    Chance = chance
  }
end

rewards.rewardSimple = GetRewardFunction(function (difficulty)
  return {
    Item("currency", 1, 2 + difficulty*2, 0.9),
    Item("hpPotion", 1, 2 + difficulty/20, 0.5),
    Item("goldSword", 1, 1, 0.002),
    Item("leatherArmor", 1, 1, 0.04),
    Item("rustyArmor", 1, 1, 0.04),
    Item("shortSword", 1, 1, 0.04),
    Item("simpleArbalet", 1, 1, 0.04),
    Item("cheapRobe", 1, 1, 0.04),
    Item("staff", 1, 1, 0.04),
    Item("magicStaff", 1, 1, 0.004),
}end)

rewards.rewardRandomLowLevelUnit = GetRewardFunction(function (difficulty)
  return{
    Unit(enemies.playerUnits.warrior)
  };
end)
