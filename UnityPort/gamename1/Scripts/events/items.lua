items = {
  currency = {
    Group = GameName1.Items.ItemGroup.Currency,
    Icon = "Items/gold",
    Description = "Use it to buy stuff",
    Name = "Currency"
  },
  hpPotion = 
  {
    Group = GameName1.Items.ItemGroup.HpPotion,
    Icon = "Items/HpPotion",
    Description = "Restores some of your health",
    Name = "Health potion"
  },
  --armors warrior
  rustyArmor = 
  {
    Group = GameName1.Items.ItemGroup.Armor,
    Icon = "Items/Armors/Warrior/rustyArmor",
    Description = "Probably taken from some dead fellow",
    Name = "Rusty armor",
    UnitTypes = {GameName1.Data.UnitType.Warrior},
    Effects = {
      MaxHealth = 3,
      ProtectionPhysical = 0.1,
    }
  },
  --weapons warrior
  goldSword =
  {
    Group = GameName1.Items.ItemGroup.Weapon,
    Icon = "Items/Weapons/Warrior/sword",
    Description = "This sword belonged to the king!",
    Name = "Gold sword",
    UnitTypes = {GameName1.Data.UnitType.Warrior},
    Effects = {
      Damage = 5,
      CritChance = 0.1,
    }
  },
  shortSword =
  {
    Group = GameName1.Items.ItemGroup.Weapon,
    Icon = "Items/Weapons/Warrior/shortSword",
    Description = "The simpliest sword available in Kronys",
    Name = "Short sword",
    UnitTypes = {GameName1.Data.UnitType.Warrior},
    Effects = {
      Damage = 5,
      CritChance = 0.1,
    }
  },

  --armors archer
  leatherArmor =
  {
    Group = GameName1.Items.ItemGroup.Armor,
    Icon = "Items/Armors/Archer/leatherArmor",
    Description = "Weak leather armor, provides almost no protection",
    Name = "Leather armor",
    UnitTypes = {GameName1.Data.UnitType.Archer},
    Effects = {
      MaxHealth = 3,
      ProtectionPhysical = 0.05,
    }
  },
  --weapons archer
  simpleArbalet =
  {
    Group = GameName1.Items.ItemGroup.Weapon,
    Icon = "Items/Weapons/Archer/simpleArbalet",
    Description = "Unreliable arbalet does not have much power, hard to reload, can break at any time.",
    Name = "Broken arbalet",
    UnitTypes = {GameName1.Data.UnitType.Archer},
    Effects = {
      Damage = 2,
    }
  },
  --armors mage
  cheapRobe =
  {
    Group = GameName1.Items.ItemGroup.Armor,
    Icon = "Items/Armors/Mage/cheapRobe",
    Description = "Does not provide any protection or magic boost, but makes you more comfortable",
    Name = "Cheap robe",
    UnitTypes = {GameName1.Data.UnitType.Mage},
    Effects = {
      MaxStamina = 5,
    }
  },
  --weapons mage
  staff =
  {
    Group = GameName1.Items.ItemGroup.Weapon,
    Icon = "Items/Weapons/Mage/staff",
    Description = "Does not help much to cast magic, but you can make some cool moves with it",
    Name = "Staff",
    UnitTypes = {GameName1.Data.UnitType.Mage},
    Effects = {
      Damage = 1,
    }
  },
  magicStaff =
  {
    Group = GameName1.Items.ItemGroup.Weapon,
    Icon = "Items/Weapons/Mage/magicStaff",
    Description = "Cheap magic staff available in all cheapest stores",
    Name = "Magic Staff",
    UnitTypes = {GameName1.Data.UnitType.Mage},
    Effects = {
      Damage = 3,
    }
  },
}

function items.GetItemType(name)
  local item = items[name];

  if(item == nil) then
    print("item nil " .. name)
  end

  local result = GameName1.Items.ItemType();
  result.Group = item.Group;
  result.Icon = item.Icon;
  result.Description = item.Description;
  result.Name = item.Name;
  result.Id = name;

  if(item.Effects ~= nil) then
    result.BonusStatsAdd = GameName1.Actors.CreatureStats();
    result.BonusStatsAdd.MaxHealth = item.Effects.MaxHealth or 0;
    result.BonusStatsAdd.MaxStamina = item.Effects.MaxStamina or 0;
    result.BonusStatsAdd.StaminaRestoring = item.Effects.StaminaRestoring or 0;
    result.BonusStatsAdd.MaxActionPoints = item.Effects.MaxActionPoints or 0;
    result.BonusStatsAdd.CritChance = item.Effects.CritChance or 0;
    result.BonusStatsAdd.Damage = item.Effects.Damage or 0;

    result.BonusStatsAdd.Protection:Add(DamageType.Physical, item.Effects.ProtectionPhysical or 0);
    result.BonusStatsAdd.Protection:Add(DamageType.Magic, item.Effects.ProtectionMagic or 0);
    result.BonusStatsAdd.Protection:Add(DamageType.Fire, item.Effects.ProtectionFire or 0);
  end

  if(item.UnitTypes ~= nil) then
    for _,unitType in ipairs(item.UnitTypes) do
      result:AddAvailableUnitType(unitType);
    end
  end

  return result;
end