Difficulty = {}

function Difficulty:LinearGrowth(min, max, coef)
  local value = min + GameState.Get().Difficulty/coef;
  if(value > max) then value = max end
  return value;
end