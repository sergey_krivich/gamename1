eventBase = class("EventBase",
{
  Description = "Empty description",
  Choices = {},
  LastOutcome = nil,
})

function eventBase:Init()
end

function eventBase:Choice(description)
  local choice =  GameName1.Data.EventChoice();
  choice.Description = description;

  self.Choices[#self.Choices+1] = choice;
  return self;
end

function eventBase:Fight(chance, location, description, fightData, reward)
  local outcomeFight = self:AddOutcome(chance, description, reward)
  outcomeFight:SetFight();
  outcomeFight.Fight.LevelName = location;

  self.LastOutcome = outcomeFight;

  local quotedResult = self:GetFightQuated(fightData.From, fightData.To, fightData.Max, fightData.Types)
  for k,v in pairs(quotedResult) do
    for _ = 1, v.Count do
      outcomeFight.Fight:AddCreatureSide(k, v.Side, v.SpawnPointIndex);
    end
  end

  return self;
end

function eventBase:GetFightQuated(fromQuota, toQuota, maxQuota, types)
  local currentQuota = RandomTool.D:NextSingle(fromQuota, toQuota);
  if(currentQuota > maxQuota) then currentQuota = maxQuota end
  local result = {};

  --fill min requested values first
  for _,v in pairs(types) do
    if(v.Min ~= nil) then
      result[v.Enemy] = self:CreateDefaultEnemyResult(v);
      result[v.Enemy].Count = v.Min;

      local cost =v.Cost or v.Enemy.SpawnData.SpawnCost;
      currentQuota = currentQuota - cost;
    end
  end

  --add other units with quota left
  while(currentQuota > 0) do
    if(#types <= 0) then break end;

    local index = RandomTool.D:NextInt(1, #types + 1);
    local item = types[index];
    local cost = item.Cost or item.Enemy.SpawnData.SpawnCost;
    
    if(cost <= currentQuota and not self:MaxTypeCountReached(item.Max, result[item.Enemy])) then
      if(item.Chance == nil or RandomTool.D:NextBool(item.Chance)) then
        currentQuota = currentQuota - cost;
        if(result[item.Enemy] == nil) then
          result[item.Enemy] = self:CreateDefaultEnemyResult(item);
        end

        result[item.Enemy].Count = result[item.Enemy].Count + 1;
      end
    else
      table.remove(types, index);
    end
  end

  return result;
end

function eventBase:MaxTypeCountReached(max, enemyType)
  if(max == nil or enemyType == nil) then return false end;
  return max <= enemyType.Count;
end

function eventBase:CreateDefaultEnemyResult(item)
  return 
  {
    Count = 0,
    Side = item.Side or FightSide.Enemy,
    SpawnPointIndex = item.SpawnPointIndex or 1;
  };
end


function eventBase:SetLevelParams(levelParams)
  self.LastOutcome.Fight.LevelParams = levelParams;
  return self;
end

function eventBase:Outcome(chance, description, reward)
  self:AddOutcome(chance,description,reward)
  return self;
end

function eventBase:AddOutcome(chance, description, reward)
  local currentChoice = self.Choices[#self.Choices];
  local outcome = self:BuildOutcome(chance, description, reward);
  currentChoice:AddOutcome(outcome);
  self.LastOutcome = outcome;
  return outcome;
end

function eventBase:FightOutcome(chance, description, reward, conditions)
  if(self.LastOutcome == nil ) then
    print("self.LastOutcome = nil")
  end
  local fight = self.LastOutcome.Fight
  if(fight == nil) then
    print("Trying to add fight outcome to non fight outcome!")
    return self;
  end

  local outcome = self:BuildOutcome(chance, description, reward, conditions);
  fight:AddOutcome(outcome);

  return self;
end

function eventBase:BuildOutcome(chance, description, reward, conditions)
  local outcome = GameName1.Data.EventOutcome();
  outcome.Description = description;
  outcome.Reward = reward;
  outcome.Chance = chance;

  if(conditions ~= nil) then
    for k,v in pairs(conditions) do
      if(v.Check == nil) then print("outcome.Check == nil") end
      outcome:AddCondition(v);
    end
  end

  return outcome;
end

function eventBase:Build()
end

function eventBase:GetChoices()
  self:Build();
  return self.Choices;
end
