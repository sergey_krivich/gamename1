require("levels.levelGenerators.noise")
require("levels.levelGenerators.noise")

caveGenerator = {}

function caveGenerator.Generate(data)
  perlin:load();

  local tileMap = GameName1.Tiles.TileMap(data.Width, data.Height);
  tileMap:FillWith(data.Wall)

  local minerPrototype = {
    x = math.floor(data.Width / 2),
    y = math.floor(data.Height / 2),
    size = RandomTool.D:NextInt(1,2)
  }

  local cellPrice = {}
  local offset = RandomTool.D:NextInt(0, 100000);
  local usePerling = RandomTool.D:NextBool();

  for i=0,data.Width - 1 do
    cellPrice[i] = {};
    for j=0,data.Height - 1 do
      cellPrice[i][j] = RandomTool.D:NextInt(0, 1000);
      if(usePerling) then
        local genValue = perlin:fBm(i+offset,j, 0.4, 16, 9, 0.5)*10;
        cellPrice[i][j] = genValue;
      end
    end
  end

  local miners = {}
  for _= 1, RandomTool.D:NextInt(2,8) do
    local copy = linq.shallow_copy(minerPrototype);
    table.insert(miners, copy)
  end

  local minerMaxSize = RandomTool.D:NextInt(1,3);
  for _,m in ipairs(miners) do
    m.size = RandomTool.D:NextInt(1,minerMaxSize)
  end

  local iter = 0;
  while(#miners > 0 and iter < 200) do
    for k,m in ipairs(miners) do
      local minTile = caveGenerator.GetMinTile(cellPrice, m.x, m.y);
      local dirX = minTile.x - m.x;
      local dirY = minTile.y - m.y;

      dirX = mathHelper.sign(dirX);
      dirY = mathHelper.sign(dirY);

      m.x = m.x + dirX;
      m.y = m.y + dirY;

      ---print("x " .. tostring(m.x) .. " y " .. tostring(m.y))

      caveGenerator.CarveTile(cellPrice, tileMap, data.Floor, m.x, m.y, m.size);

      if(m.x < data.Border or m.y < data.Border or m.x >= data.Width - data.Border or m.y >= data.Height - data.Border) then
        table.remove(miners, k)
      end
    end

    iter = iter + 1;
  end

  return tileMap;
end

function caveGenerator.GetMinTile(cellPrice, x, y)
  local getPrice = function(i,j)
    if(cellPrice[i] ~= nil) then
      if(cellPrice[i][j] ~= nil) then return {v = cellPrice[i][j], p = {x = i,y = j}}; end
    end
    return {v = 0, p = {x = i, y = j}};
  end

  local prices = {
    getPrice(x - 1, y),
    getPrice(x + 1, y),
    getPrice(x, y + 1),
    getPrice(x, y - 1)
  }


  local max = linq.min(prices, function(p) return p.v end, prices[1].v);
  local maxPrices = linq.whereIndexed(prices, function(p) return p.v == max.v end);

  local randomMaxIndex = RandomTool.D:NextInt(#maxPrices) + 1;
  return maxPrices[randomMaxIndex].p;
end

function caveGenerator.CarveTile(cellPrice, tileMap, tileData, x, y, size)
  for i=x, x + size - 1 do
    for j=y, y + size - 1 do
      if(tileMap:InRange(i,j)) then
        tileMap:SetTileByDataIndex(tileData, i,j)
        cellPrice[i][j] = cellPrice[i][j] + 1;
        cellPrice[i][j] = cellPrice[i][j] * 2;
      end
    end
  end
end