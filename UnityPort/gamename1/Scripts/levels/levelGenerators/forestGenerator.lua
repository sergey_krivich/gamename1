require("levels.levelGenerators.noise")

ForestGenerator = {}

function ForestGenerator.Generate(data)
  perlin:load();

  local tileMap = GameName1.Tiles.TileMap(data.Width, data.Height);
  tileMap:FillWith(data.Grass)


  local forest = ForestGenerator.Fill(data, {offset = RandomTool.D:NextInt(0,100000)}, RandomTool.D:NextSingle(14,19));
  local mounts = ForestGenerator.Fill(data, {offset = RandomTool.D:NextInt(0,100000)}, RandomTool.D:NextSingle(15,19.5));
  local mounts2 = ForestGenerator.Fill(data, {offset = RandomTool.D:NextInt(0,100000)}, RandomTool.D:NextSingle(16.5,20.5));
  local ponds = ForestGenerator.Fill(data, {offset = RandomTool.D:NextInt(0,100000)}, RandomTool.D:NextSingle(18,20));
  local river = ForestGenerator.GetRiver(data, {});
  local hasRiver = RandomTool.D:NextBool(0.5);

  local used = {};
  for j=1,data.Height do
    for i=1,data.Width do
      local index = ForestGenerator.Index(data,i,j);
      if forest[index] == 1 then
        tileMap:SetTileByDataIndex(data.Forest, i-1, j-1);
      end

      if ponds[index] == 1 then
        if(RandomTool.D:NextBool(0.1)) then
          tileMap:SetTileByDataIndex(data.Pond, i-1, j-1);
        end
      end

      if((mounts[index] == 1
      and ForestGenerator.InRange(data, i,j, data.MountRangeMax) and river[index] ~= 1)
      or ForestGenerator.InRange(data, i,j,data.MountRangeMin) or mounts2[index] == 1) then
        tileMap:SetTileByDataIndex(data.Mount, i-1, j-1);
      end

      if hasRiver then
        if river[index] == 1 then
          tileMap:SetTileByDataIndex(data.River, i-1, j-1);
        end
        if river[index] == 2 then
          tileMap:SetTileByDataIndex(data.Bridge, i-1, j-1);
        end
      end

    end
  end

  return tileMap;
end
function ForestGenerator.Index(data, i, j)
  if(data.Width > data.Height) then
    return math.floor((i-1)*(data.Width) + (j-1));
  else
    return math.floor((j-1)*(data.Height) + (i-1));
  end
end

function ForestGenerator.InRange(data, i,j, range)
  return (i <= range or j <= range) or (i > data.Width - range or j > data.Height - range)
end

function ForestGenerator.Fill(data, fillData, threshhold)
  local map = {}

  for j=1,data.Height do
    for i=1,data.Width do
      local genValue = perlin:fBm(i+fillData.offset,j, 0.4, 16, 9, 0.5)*10;
      if(genValue > threshhold) then
        map[ForestGenerator.Index(data, i,j)] = 1;
      else
        map[ForestGenerator.Index(data, i,j)] = 0;
      end
    end
  end

  return map;
end

function ForestGenerator.GetRiver(data, fillData)
  local RIVER = 1;
  local BRIDGE = 2;

  local map = {}
  local riverX = 1
  local riverY = RandomTool.D:NextInt(data.Height/4 - 1,data.Height/1.5);

  local vertical = RandomTool.D:NextBool(0.5);
  if(vertical) then
    riverX = RandomTool.D:NextInt(data.Width/4 - 1,data.Width/1.5)
    riverY = 1;
  end

  local bridgeCountCoef = 1;
  if(vertical) then -- todo: refactor copy pasta if this sticks
    for j=1,data.Height do
      map[ForestGenerator.Index(data, riverX, j)] = RIVER;
      
      if(j > data.Height / 4 and j < data.Height/1.5) then
        local addBridge = RandomTool.D:NextBool(0.5/bridgeCountCoef);

        if(map[ForestGenerator.Index(data, riverX, j - 1)] == BRIDGE) then
          addBridge = false;
        end

        if(addBridge) then
          bridgeCountCoef = bridgeCountCoef + 1;
          map[ForestGenerator.Index(data, riverX, j)] = BRIDGE;
        end
      end
    end
  else
    for i=1,data.Width do
      map[ForestGenerator.Index(data, i, riverY)] = RIVER;

      if(i > data.Width / 4 and i < data.Width/1.5) then
        local addBridge = RandomTool.D:NextBool(0.5/bridgeCountCoef);

        if(map[ForestGenerator.Index(data, i - 1, riverY)] == BRIDGE) then
          addBridge = false;
        end

        if(addBridge) then
          bridgeCountCoef = bridgeCountCoef + 1;
          map[ForestGenerator.Index(data, i, riverY)] = BRIDGE;
        end
      end
    end
  end
  return map;
end