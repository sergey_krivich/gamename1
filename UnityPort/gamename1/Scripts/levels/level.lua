levelBase = class("LevelBase",
{

})

function levelBase:Init(levelParams)
  self.levelParams = levelParams
end

function levelBase:GenLevel()
end

function levelBase:OnStepStart()
end

function levelBase:GenFx(level)
  if(self.levelParams and self.levelParams.GenFx) then
    return self.levelParams.GenFx(level);
  end
end

function levelBase:SetMask(tile, masks)
  for i,m in ipairs(masks) do
    local mask = GameName1.Tiles.Mask();
    for j,v1 in ipairs(m.Array) do
      for k,v2 in ipairs(v1) do
        mask:SetMask(j - 1,k - 1, v2);
      end
    end
    mask.SpriteIndex = m.Sprite;
    if(m.TileCount ~= nil) then
      mask.TileCount = m.TileCount;
    end
    tile:AddMask(mask)
  end
end

function levelBase:GenWaypoints(level, tileMap)
  local centerX = tileMap.Width/2;
  local centerY = tileMap.Height/2;

  local dir = 1;
  local max = tileMap.Width/4;

  if(self.levelParams ~= nil) then
    max = self.levelParams.SpawnRange or max;
  end

  for i=1,2 do
    local x = centerX;
    local y = centerY;
    local newx = centerX;
    local newy = centerY;

    dir = dir * -1;
    local offsetCount = 0;
    while (offsetCount < max) do
      if(RandomTool.D:NextBool()) then
        newx = x + dir;
      else
        newy = y + dir;
      end

      if(tileMap:IsSolid(newx, newy) or not tileMap:InRange(newx, newy)) then
        break;
      else
        x = newx;
        y = newy
      end

      offsetCount = offsetCount + 1;
    end

    --for whatever reason numbers become rounded differently in build and in editor
    local point = Point(math.floor(x), math.floor(y));

    local rX = tileMap.Width;
    local rY = tileMap.Height;

    if(i == 1) then rX, rY = 0, 0 end;

    local direction = UnityEngine.Vector2(rX - x, rY - y);
    UnityEngine.Vector2.Normalize(direction)

    local algorithm = GameName1.Levels.SpawnAlgorithmType.Column;
    if(i == 2) then algorithm = GameName1.Levels.SpawnAlgorithmType.Random end--first way point belongs to players

    level:AddSpawnPoint(point, direction, algorithm);
  end

end


function levelBase:Get9SidedMask()
  return {
        {
      Array = {
        {2, 1, 2},
        {1, 1, 1},
        {2, 1, 2}
      },
      TileCount = 8,
      Sprite = GameName1.Tiles.SpriteIndex.Middle
    },
    {
      Array = {
        {2, 0, 2},
        {0, 1, 0},
        {2, 0, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Default
    },
    {
      Array = {
        {1, 1, 1},
        {1, 1, 1},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Bottom
    },
    {
      Array = {
        {2, 2, 2},
        {1, 1, 1},
        {1, 1, 1}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Top
    },
    {
      Array = {
        {1, 1, 2},
        {1, 1, 2},
        {1, 1, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Right
    },
    {
      Array = {
        {2, 1, 1},
        {2, 1, 1},
        {2, 1, 1}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Left
    },
    {
      Array = {
        {2, 1, 1},
        {2, 1, 1},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.BottomLeft
    },
    {
      Array = {
        {1, 1, 2},
        {1, 1, 2},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.BottomRight
    },
    {
      Array = {
        {2, 2, 2},
        {1, 1, 2},
        {1, 1, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.TopRight
    },
    {
      Array = {
        {2, 2, 2},
        {2, 1, 1},
        {2, 1, 1}
      },
      Sprite = GameName1.Tiles.SpriteIndex.TopLeft
    },
  };
end

function levelBase:GetRoadMask()
  return {
    --bridge
    {
      Array = {
        {2, 1, 2},
        {-3, 1, 2},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.TopLeft
    },
    {
      Array = {
        {2, -3, 2},
        {1, 1, 2},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.TopLeft
    },
    {
      Array = {
        {2, 1, 2},
        {2, 1, -3},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Right
    },
    {
      Array = {
        {2, 1, -3},
        {2, 1, 2},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Right
    },
    {
      Array = {
        {-3, 1,2},
        {2, 1, 2},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Right
    },
    {
      Array = {
        {2, 2, 2},
        {-3, 1, 2},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Right
    },
    {
      Array = {
        {2, 2, 2},
        {2, 1, 1},
        {2, -3, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.BottomRight
    },
    {
      Array = {
        {2, 2, 2},
        {2, 1, 2},
        {2, -3, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Top
    },
    {
      Array = {
        {2, 2, 2},
        {2, -3, 2},
        {2, 1, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Top
    },
    {
      Array = {
        {2, 2, 2},
        {2, 1, -3},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Right
    },
    {
      Array = {
        {2, -3, 2},
        {2, 1, 2},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Top
    },
    --

    {
      Array = {
        {2, 1, 2},
        {1, 1, 2},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.TopLeft
    },
    {
      Array = {
        {2, 1, 2},
        {2, 1, 1},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.TopRight
    },
    {
      Array = {
        {2, 2, 2},
        {2, 1, 1},
        {2, 1, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.BottomRight
    },
    {
      Array = {
        {2, 2, 2},
        {1, 1, 2},
        {2, 1, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.BottomLeft
    },
    {
      Array = {
        {2, 2, 2},
        {1, 1, 2},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Right
    },
    {
      Array = {
        {2, 1, 2},
        {2, 1, 2},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Top
    },
    {
      Array = {
        {2, 2, 2},
        {2, 1, 1},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Right
    },
    {
      Array = {
        {2, 2, 2},
        {2, 1, 2},
        {2, 1, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Top
    },
  };
end

function levelBase:GetBridgeMask()
  return {
    {
      Array = {
        {2, 2, 2},
        {-2, 1, -2},
        {2, 2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.TopLeft
    },
    {
      Array = {
        {2, -2, 2},
        {2, 1, 2},
        {2, -2, 2}
      },
      Sprite = GameName1.Tiles.SpriteIndex.Default
    },
  }
end

function levelBase:GetSimpleMask()
  return {
  }
end