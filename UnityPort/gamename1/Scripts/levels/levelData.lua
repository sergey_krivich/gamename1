levelTable = {}

function levelTable.Create(name)
  local levelClass = levelTable[name]
  if(levelClass == nil) then print("level not found: ".. name); end

  local level = levelClass:new()
  return level
end

require("levels.level")
require("levels.forest")
require("levels.cave")