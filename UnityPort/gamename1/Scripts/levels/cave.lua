require("levels.levelGenerators.caveGenerator")

caveLevel = levelBase:extend("caveLevel", {})
levelTable["cave"] = caveLevel;

function caveLevel:GenLevel()
  local level = GameName1.Levels.Level();

  local wall = GameName1.Tiles.TileData();
  wall.ProtectionLevel = 0.55;
  wall.Type = 1;
  wall.Sprite = "wall";
  wall.IsSolid = true;
  self:SetMask(wall, self:Get9SidedMask());

  local floor = GameName1.Tiles.TileData();
  floor.Sprite = "floor";
  floor.Type = 0;
  floor.ProtectionLevel = 0;

  local tileMap = caveGenerator.Generate({
      Width = 25,
      Height = 25,
      Wall = wall,
      Floor = floor,
      Border = 3,
    });

  level:SetTileMap(tileMap);
  self:GenWaypoints(level, tileMap);
  level.SpriteAtlas = "cave";
  level:SetLightSettings(78/255, 114/255, 163/255, 1.5);
  return level;
end

function caveLevel:GenWaypoints(level, tileMap)
  for i=1,2 do
    local direction = UnityEngine.Vector2(0, 0);
    local point = tileMap:GetFreeRandomPoint();
    local algorithm = GameName1.Levels.SpawnAlgorithmType.Fill;
    level:AddSpawnPoint(point, direction,algorithm);
  end
end