require("levels.levelGenerators.forestGenerator")

forestLevel = levelBase:extend("forestLevel", {})
levelTable["forest"] = forestLevel;

function forestLevel:GenLevel()
  local level = GameName1.Levels.Level();

  local forest = GameName1.Tiles.TileData();
  forest.ProtectionLevel = 0.25;
  forest.Type = 1;
  forest.Sprite = "forest";
  forest.WalkCost = 2;
  self:SetMask(forest, self:Get9SidedMask());

  local mount = GameName1.Tiles.TileData();
  mount.ProtectionLevel = 0.55;
  mount.Type = 4;
  mount.Sprite = "mount";
  mount.IsSolid = true;
  self:SetMask(mount, self:Get9SidedMask());

  local river = GameName1.Tiles.TileData();
  river.Type = 2;
  river.IsSolid = false;
  river.Sprite = "river";
  river.WalkCost = 2.0;
  river.ProtectionLevel = -0.5;
  self:SetMask(river, self:GetRoadMask());

  local bridge = GameName1.Tiles.TileData();
  bridge.ProtectionLevel = -0.25;
  bridge.Type = 3;
  bridge.IsSolid = false;
  bridge.Sprite = "bridge";
  self:SetMask(bridge, self:GetBridgeMask());

  local grass = GameName1.Tiles.TileData();
  grass.Sprite = "grass";
  grass.Type = 0;
  grass:AddVariation(0, 1);
  grass:AddVariation(1, 0.025);
  grass:AddVariation(2, 0.025);
  grass:AddVariation(3, 0.8);
  grass:AddVariation(4, 0.025);
  grass.ProtectionLevel = 0;


  local pond = GameName1.Tiles.TileData();
  pond.Type = 4;
  pond.IsSolid = false;
  pond.Sprite = "pond";
  pond.WalkCost = 2.0;
  pond.ProtectionLevel = -0.5;
  pond:AddVariation(0, 1);
  pond:AddVariation(1, 0.5);
  
  self:SetMask(pond, self:GetSimpleMask());

  local tileMap = ForestGenerator.Generate({
      Width = 23 + RandomTool.D:NextInt(8),
      Height =23 + RandomTool.D:NextInt(8),
      Mount = mount,
      Forest = forest,
      River = river,
      Bridge = bridge,
      Grass = grass,
      Pond = pond,
      MountRangeMax = RandomTool.D:NextInt(4,6),
      MountRangeMin = 2,
    });--

  level:SetTileMap(tileMap);
  self:GenWaypoints(level, tileMap);

  local colorDay = UnityEngine.Color(1,1,1,1);
  local colorMidDay = UnityEngine.Color(255/255, 188/255, 169/255, 1);
  local colorNight = UnityEngine.Color(74/255, 68/255, 255/255, 1);

  level.SpriteAtlas = "forestTileMap";
  if(RandomTool.D:NextBool(0.7)) then
    level:SetLightSettings(colorDay.r, colorDay.g, colorDay.b, 1.5+ RandomTool.D:NextSingle()/4);
  elseif(RandomTool.D:NextBool(0.7)) then
    level:SetLightSettings(colorMidDay.r, colorMidDay.g, colorMidDay.b, 1.5 + RandomTool.D:NextSingle()/4);
  else
    level:SetLightSettings(colorNight.r, colorNight.g, colorNight.b, 2.2);
  end

  return level
end