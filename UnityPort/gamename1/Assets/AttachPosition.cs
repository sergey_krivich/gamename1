﻿using System.Collections.Generic;
using UnityEngine;

public class AttachPosition : MonoBehaviour
{
  public List<GameObject> ToAttach;
  public int DestroyDetachedIn = 5;

  void Awake()
  {
    foreach (var go in ToAttach)
    {
      go.transform.SetParent(null);
    }
  }

  void LateUpdate()
  {
    foreach (var go in ToAttach)
    {
      go.transform.position = transform.position;
    }
  }

  void Destroyed()
  {
    foreach (var go in ToAttach)
    {
      Destroy(go, DestroyDetachedIn);
      var particles = go.GetComponent<ParticleSystem>();
      particles.Stop();
    }
  }
}
