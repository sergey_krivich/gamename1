﻿using UnityEngine;
using System.Collections;

public class PoolInSeconds : MonoBehaviour
{
    public float TimeToDestroy;
    private readonly Timer _timer = new Timer();

	void Start ()
	{
        Reset();
	}

    private void Update()
    {
        _timer.Update(Time.deltaTime);
        if (!_timer.IsRunning)
        {
            Reset();
            GameObjectPool.I.PoolObject(gameObject);
        }
    }

    private void OnDisable()
    {
        Reset();
    }

    private void Reset()
    {
        _timer.Duration = TimeToDestroy;
        _timer.Reset();
        _timer.Run();
    }
}
