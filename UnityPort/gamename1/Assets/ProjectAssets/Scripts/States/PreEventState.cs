﻿using System;
using Assets.ProjectAssets.Scripts.States;
using GameName1.Data;
using GameName1.Items;
using GameName1.Network;
using GameName1.Network.Commands;
using GameName1.Travel;
using UnityEngine;

namespace GameName1.States
{
  public class PreEventState:StateBase
  {
    public event Action<UnitData> UnitDataChanged; 

    private readonly ReadyGate _gate;

    public PreEventState()
      :base("preEvent")
    {
      _gate = new ReadyGate();
    }

    protected override void OnActivate()
    {
      GameObject.FindObjectOfType<PartyStatusView>()
        .Set(this);

      GameObject.FindObjectOfType<PreEventView>()
        .Set(this);

      _gate.Subscribe();


      AppRoot.I.EventDispatcher.Subscribe<HealUnitWithItemCmd.UnitHealedEventData>(UnitHealed);

      RestoreUnitHp();

#if UNITY_EDITOR
      if (AppRoot.I.Config.FastPreEvent)
      {
        ContinueAdventure();
      }
#endif
    }

    private static void RestoreUnitHp()
    {
      foreach (var player in AppRoot.I.Players.GetAll())
      {
        foreach (var playerCreature in player.Creatures)
        {
          playerCreature.AddHpPercent(0.05f);
        }
      }
    }

    public void ContinueAdventure()
    {
      _gate.SendReadyCmd();

      AppRoot.I.SetState(new WaitOtherPlayersState(new EventChoiceState(), _gate));
    }

    public void UnitHealed(HealUnitWithItemCmd.UnitHealedEventData unitHealed)
    {
      if (UnitDataChanged != null)
      {
        UnitDataChanged(unitHealed.Unit);
      }
    }

    public void HealUnit(PlayerProfile player, UnitData data)
    {
      var healthPotion = player.Inventory.GetItemByGroup(ItemGroup.HpPotion);

      if (healthPotion == null)
      {
        return;
      }

      if (data.CurrentHp >= data.Stats.MaxHealth)
      {
        return;
      }

      new HealUnitWithItemCmd(new HealUnitWithItemCmd.Input
      {
        ItemId = healthPotion.Id,
        CreatureId = data.Id,
        PlayerId = player.Id
      }).Execute();
    }
  }
}
