﻿using GameName1.Data;
using UnityEngine;

namespace GameName1.States
{
  public partial class FightState
  {
    private void UpdateCheats()
    {
      if (Input.GetKeyDown(KeyCode.K))
      {
        KillEnemies();
      }

      if (Input.GetKeyDown(KeyCode.L))
      {
        KillPlayers();
      }

      if (Input.GetKeyDown(KeyCode.G))
      {
        RegenLevel();
      }
    }

    private void KillEnemies()
    {
      foreach (var player in _otherBattleFightPlayers)
      {
        foreach (var aliveCreature in player.AliveCreatures)
        {
          aliveCreature.RecieveDamageSimple(100000f, aliveCreature);
        }
      }
    }

    private void KillPlayers()
    {
      foreach (var player in _humanFightPlayers)
      {
        foreach (var aliveCreature in player.AliveCreatures)
        {
          aliveCreature.RecieveDamageSimple(100000f, aliveCreature);
        }
      }
    }

    private void RegenLevel()
    {
      var levelWrapper = AppRoot.I.LuaLoader.LevelLoader.GetLevel(_fightInfo.LevelName);
      levelWrapper.Init(_fightInfo.LevelParams);

      var level = levelWrapper.GenLevel();
      Sprites.LoadSpriteAtlas(level.SpriteAtlas);
      _world.GoTo(level);
      _tileMapView.SetTileMap(_world.TileMap, _world.Level.SpriteAtlas);
    }
  }
}