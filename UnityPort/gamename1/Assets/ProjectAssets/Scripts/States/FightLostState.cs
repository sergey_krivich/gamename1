namespace GameName1.States
{
  public class FightLostState : StateBase
  {
    protected override void OnActivate()
    {
      var window = WindowControl.I.Show<FightLostView>();//todo: add prefab
      window.ContinueClicked += WindowContinueClicked;
    }

    private void WindowContinueClicked()
    {
      AppRoot.I.RestartGame();
      AppRoot.I.SetState(new StateMainMenu());
    }
  }
}