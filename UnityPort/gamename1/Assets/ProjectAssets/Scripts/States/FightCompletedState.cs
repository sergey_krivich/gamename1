﻿using System.Collections.Generic;
using System.Linq;
using Assets.ProjectAssets.Scripts.States;
using GameName1.Data;
using GameName1.Fight;
using GameName1.Network;
using UnityEngine;

namespace GameName1.States
{
  public class FightCompletionData
  {
    public EventOutcomeReward Reward;
    public FightInfo FightInfo;
    public List<FightPlayer> HumanFightPlayers;
    public List<FightPlayer> OtherFightPlayers;
  }

  public class FightCompletedState:StateBase
  {
    private readonly FightCompletionData _completionData;
    private ReadyGate _readyGate;

    public FightCompletedState(FightCompletionData completionData)
    {
      _completionData = completionData;

      _readyGate = new ReadyGate();
      _readyGate.Subscribe();
    }

    protected override void OnActivate()
    {
      base.OnActivate();

      AddXp();

      var window = WindowControl.I.Show<FightCompletedView>();
      window.SetResult(new FightCompletedView.Result
      {
        Reward = _completionData.Reward
      });

      window.OnContinueClick += () =>
      {
        TryGoToEventChoiceState();
      };
    }

    protected override void OnDeactivate()
    {
      WindowControl.I.Hide<FightCompletedView>();
      WindowControl.I.Hide<LevelUpView>();
    }

    private void TryGoToEventChoiceState()
    {
      if (!TryShowLevelUpWindow())
      {
        ApplyCreatureChange();
        SetReady();
      }
    }

    private void ApplyCreatureChange()
    {
      foreach (var player in _completionData.HumanFightPlayers)
      {
        foreach (var playerCreature in player.Creatures)
        {
          playerCreature.Data.CurrentHp = Mathf.RoundToInt(playerCreature.Health);
          if (playerCreature.Data.CurrentHp <= 0)
          {
            playerCreature.Data.CurrentHp = 1;
          }
        }
      }
    }

    private void SetReady()
    {
      var outcome = EventOutcomeChooser.EventOutcome(_completionData.FightInfo.Outcomes, new OutcomeCheckData()
      {
        HumanFightPlayers = _completionData.HumanFightPlayers,
        OtherFightPlayers = _completionData.OtherFightPlayers
      });

      StateBase state;
      if (outcome == null)
        state = new PreEventState();
      else
        state = new EventChoiceState(null, outcome, true);

      _readyGate.SendReadyCmd();
      AppRoot.I.SetState(new WaitOtherPlayersState(state, _readyGate));
    }

    private bool TryShowLevelUpWindow()
    {
      foreach (var profile in AppRoot.I.Players.AllLocal)
      {
        foreach (var data in profile.Creatures.ToList())
        {
          if (!data.SkillSlots.Any()) continue;

          var skillSlot = data.SkillSlots.FirstOrDefault();
          var availableSkills = AppRoot.I.LuaLoader
            .SkillLoader.SelectAvailable(data.Type, skillSlot.SlotLevel);

          availableSkills = availableSkills
            .Where(s => s != data.DefaultAttackSkillId
                        && !data.SkillIds.Contains(s))
            .ToList();

          if (!availableSkills.Any())
            continue;

          var window = WindowControl.I.Show<LevelUpView>();

          window.SetData(new LevelUpView.Data
          {
            CharacterSprite = data.Sprite,
            AvailableSkills =RandomTool.U.ChooseItems(availableSkills, 4),
            AlreadyLearned = data.SkillIds
          });

          data.SkillSlots.RemoveAt(0);

          var localData = data;
          window.OnSkillSelected += skill => { SelectSkill(localData, skill); };

          if (AppRoot.I.Config.FastSkillLearn)
          {
            SelectSkill(localData, availableSkills.FirstOrDefault());
          }

          return true;
        }
      }

      return false;
    }

    private void SelectSkill(UnitData localData, string skill)
    {
      localData.AddSkill(skill);
      TryGoToEventChoiceState();
    }

    private void AddXp()
    {
      int totalFightXp = 0;

      foreach (var enemies in _completionData.FightInfo.Creatures)
      {
        totalFightXp += enemies.DeathXp;
      }

      totalFightXp /= AppRoot.I.Players.GetAll().Count;
      totalFightXp = (int)(totalFightXp * AppRoot.I.Config.XpMult);
      foreach (var profile in AppRoot.I.Players.GetAll())
      {
        profile.AddXp(totalFightXp);
      }
    }

  }
}
