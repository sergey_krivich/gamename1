﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Data;

public class EventOutcomeChooser
{
  public static EventOutcome EventOutcome(List<EventOutcome> outcomes, OutcomeCheckData data)
  {
    if (outcomes.Count == 0)
    {
      return null;
    }

    var validOutcomes = outcomes.Where(o => o.CheckConditions(data)).ToList();
    var outcome = RandomTool.D.NextWeightedChoice(validOutcomes,
      validOutcomes.Select(o => o.Chance).ToList());

    return outcome;
  }
}