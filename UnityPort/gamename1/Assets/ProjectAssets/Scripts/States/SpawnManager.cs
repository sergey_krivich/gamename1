using System.Collections.Generic;
using GameName1.Actors;
using GameName1.Extensions;
using GameName1.Levels;
using GameName1.Tiles;
using UnityEngine;

namespace GameName1.States
{
  public class SpawnManager
  {
    private readonly Level _level;
    private readonly TileMap _tileMap;

    public SpawnManager(Level level)
    {
      _level = level;
      _tileMap = level.TileMap;
    }

    public void SetColumn(List<CreatureActor> creatures)
    {
      int yOffset = 0;

      foreach (var creature in creatures)
      {
        var spawnPoint = _level.GetSpawnPoint(creature.Data.SpawnPointIndex);
        Point current = spawnPoint.Tile;
        Vector2 startTile = current.ToVector();

        var xOffset = (int)creature.Data.Type;

        var direction = spawnPoint.Direction;
        var hasDirection = direction.magnitude >= 0.01f;


        int tries = 0;

        Vector2 currentTileVec = startTile;

        while (true)
        {
          var isFree = _tileMap.IsFree(current);
          var hasNoCreature = _tileMap.GetCreatureFromTile(current) == null;

          if (!hasDirection)
          {
            direction = RandomTool.D.NextUnitVector2();
          }
          var dirRight = new Vector2(direction.y, -direction.x);


          if (isFree && hasNoCreature)
          {
            xOffset = 0;
            yOffset = 0;
            break;
          }

          currentTileVec = startTile + direction * xOffset + dirRight * yOffset;
          current = currentTileVec.ToSizeRound();

          if (tries++ > 1000)
          {
            Debug.LogError("infinit cycle at spawn!");
            currentTileVec = _tileMap.GetFreeRandomPoint().ToVector();
            break;
          }

          yOffset++;

          if (yOffset > creatures.Count / 2f)
          {
            yOffset = 0;
            xOffset++;

            xOffset *= -1;
          }
        }

        creature.SetTile(currentTileVec.ToSizeRound(), true);
      }
    }

    public void SetFill(List<CreatureActor> creatures)
    {
      var openList = new Queue<Point>();
      var closedList = new HashSet<Point>();

      foreach (var creatureActor in creatures)
      {
        var spawnPoint = _level.GetSpawnPoint(creatureActor.Data.SpawnPointIndex);
        AddItemToOpenList(openList, closedList, spawnPoint.Tile);

        bool positionSet = false;

        while (openList.Count > 0)
        {
          var current = openList.Dequeue();

          AddItemToOpenList(openList, closedList, current + new Point(1, 0));
          AddItemToOpenList(openList, closedList, current + new Point(-1, 0));
          AddItemToOpenList(openList, closedList, current + new Point(0, 1));
          AddItemToOpenList(openList, closedList, current + new Point(0, -1));

          if (!_tileMap.HasObjectsOnTile(current))
          {
            creatureActor.SetTile(current, true);
            positionSet = true;
            break;
          }
        }

        if (!positionSet)
        {
          creatureActor.SetTile(_tileMap.GetFreeRandomPoint(), true);
        }
      }
    }

    private void AddItemToOpenList(Queue<Point> openList, HashSet<Point> closedList, Point point)
    {
      if(!_tileMap.InRange(point))
        return;

      if(!_tileMap.IsFree(point))
        return;

      if (!closedList.Contains(point))
      {
        openList.Enqueue(point);
        closedList.Add(point);
      }
    }

    public void SetRandom(List<CreatureActor> creatures)
    {
      foreach (var creature in creatures)
      {
        var spawnPoint = _level.GetSpawnPoint(creature.Data.SpawnPointIndex);
        Point current = spawnPoint.Tile;
        Vector2 startTile = current.ToVector();

        var direction = spawnPoint.Direction;
        var hasDirection = direction.magnitude >= 0.01f;

        int tries = 0;

        Vector2 currentTileVec = startTile;

        while (true)
        {
          var isFree = _tileMap.IsFree(current);
          var hasNoCreature = _tileMap.GetCreatureFromTile(current) == null;
          var inSpawnRange = (currentTileVec - startTile).magnitude >= creature.Data.SpawnData.SpawnRange;

          if (!hasDirection)
          {
            direction = RandomTool.D.NextUnitVector2();
          }

          var dirRight = new Vector2(direction.y, -direction.x);

          if (isFree && hasNoCreature && inSpawnRange)
          {
            break;
          }

          if (creature.Data.SpawnData.SpawnRange > 0)
          {
            currentTileVec += direction * RandomTool.D.NextSingle(0, 1f);
            currentTileVec += dirRight * RandomTool.D.NextSign();
          }
          else
          {
            var coef = creature.Data.SpawnData.SpawnInDesiredDirectonCoef;
            var desiredDirection = RandomTool.D.NextUnitVector2() + direction * coef;
            desiredDirection.Normalize();
            currentTileVec += desiredDirection;
          }
          current = currentTileVec.ToSizeRound();


          if (tries++ > 1000)
          {
            currentTileVec = _tileMap.GetFreeRandomPoint().ToVector();
            Debug.LogError("infinit cycle at spawn!");
            break;
          }
        }


        creature.SetTile(currentTileVec.ToSizeRound(), true);
      }
    }
  }
}