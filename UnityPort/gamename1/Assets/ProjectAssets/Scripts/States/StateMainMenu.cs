﻿using GameName1.Network;
using GameName1.Network.Commands;
using GameName1.Travel;
using GameName1.UI;
using UnityEngine;

namespace GameName1.States
{
  public class StateMainMenu : StateBase
  {
    private int _playerIndex = 0;
    private MainMenuView _mainMenuView;

    public StateMainMenu()
      : base("MainMenu")
    {
    }


    protected override void OnActivate()
    {
      base.OnActivate();

      NetworkManager.I.Connected += NetworkManagerOnConnected;
      AppRoot.I.Players.Added += OnAdded;

      _mainMenuView = WindowControl.I.Show<MainMenuView>();
      _mainMenuView.OnSendClicked += MainMenuViewOnSendClicked;
      _mainMenuView.OnJoinClicked += MainMenuViewOnJoinClicked;
      _mainMenuView.OnCreateClicked += MainMenuViewOnCreateClicked;
    }

    private void MainMenuViewOnJoinClicked(string adress)
    {
      if (string.IsNullOrEmpty(adress))
      {
        adress = "localhost";
      }

      NetworkManager.I.Join(adress);
    }

    private void MainMenuViewOnCreateClicked()
    {
      NetworkManager.I.Create();
    }

    private void NetworkManagerOnConnected()
    {
      CreatePlayerProfile();
    }

    private void MainMenuViewOnSendClicked()
    {
      CreatePlayerProfile();
      _playerIndex++;
    }

    private void StartGame()
    {
      AppRoot.I.Config.OnLaunched();

      AppRoot.I.SetState(new PreEventState());
    }

    private void CreatePlayerProfile()
    {
      new CreatePlayerProfileCmd(new CreatePlayerProfileCmd.Input
      {
        Id = RandomTool.U.NextInt(),
        Nickname = NetworkManager.I.IsServer && _playerIndex == 0 ? "Boris" : "SomeGuy"
      }, true).Execute();
    }

    private void OnAdded(PlayerProfile playerProfile)
    {
      if (AppRoot.I.Players.GetAll().Count >= 2)
      {
        StartGame();
      }
    }
  }
}
