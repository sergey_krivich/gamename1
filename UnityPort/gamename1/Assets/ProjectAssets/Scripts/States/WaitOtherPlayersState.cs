﻿
using GameName1.Network;

namespace Assets.ProjectAssets.Scripts.States
{
  public class WaitOtherPlayersState:StateBase
  {
    private readonly StateBase _nextState;
    private readonly ReadyGate _gate;

    public WaitOtherPlayersState(StateBase nextState, ReadyGate gate)
    {
      _nextState = nextState;
      _gate = gate;
    }

    protected override void OnActivate()
    {
      WindowControl.I.Show<WaitingForOtherPlayersView>();
    }

    protected override void OnDeactivate()
    {
      _gate.Unsubscribe();
    }

    public override void Update()
    {
      if (_gate.AllReady)
      {
        AppRoot.I.SetState(_nextState);
      }
    }
  }
}
