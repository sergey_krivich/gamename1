﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.ProjectAssets.Scripts.States;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Items;
using GameName1.Network.Commands;
using GameName1.Travel;
using GameName1.UI;
using UnityEngine;
using Event = GameName1.Data.Event;


namespace GameName1.States
{
  public class ChoiceMadeEventData
  {
    public PlayerProfile PlayerProfile;
    public int Choice;
  }

  public class EventChoiceState : StateBase
  {
    private readonly bool _goToPreEvent;
    private EventOutcome _outcome;
    private Event _travelEvent;
    private EventDispatcher.EventHandle _choiceMadeHandler;

    public List<ChoiceMadeEventData> PlayerChoices { get; private set; }

    public event Action<int> ChoiceAdded;
    private bool _choiceMade;

    public Event TravelEvent
    {
      get { return _travelEvent; }
      private set
      {
        _travelEvent = value;

        if (TravelEventChanged != null)
          TravelEventChanged();
      }
    }

    public EventOutcome Outcome
    {
      get { return _outcome; }
      private set
      {
        _outcome = value;

        if (_outcome != null)
        {
          AddOutcomeRewards();

          if (OutcomeChanged != null)
            OutcomeChanged();
        }
      }
    }

    public event Action TravelEventChanged;
    public event Action OutcomeChanged;

    public EventChoiceState(Event travelEvent = null, EventOutcome outcome = null, bool goToPreEvent = false)
      : base("eventChoice")
    {
      _goToPreEvent = goToPreEvent;
      Outcome = outcome;
      TravelEvent = travelEvent;
      PlayerChoices = new List<ChoiceMadeEventData>();
      if (TravelEvent == null && Outcome == null)
      {
        GetRandomEvent();
      }
    }

    protected override void OnActivate()
    {
      base.OnActivate();
      InitViews();

      _choiceMadeHandler = AppRoot.I.EventDispatcher.Subscribe<ChoiceMadeEventData>(OnPlayerChoiceMade);
    }

    protected override void OnDeactivate()
    {
      base.OnDeactivate();
      AppRoot.I.EventDispatcher.Unsubscribe(_choiceMadeHandler);
    }

    private void OnPlayerChoiceMade(ChoiceMadeEventData data)
    {
      PlayerChoices.Add(data);

      if (ChoiceAdded != null)
      {
        ChoiceAdded(data.Choice);
      }

      var choiceIndex = PlayerChoices[0].Choice;
      if (PlayerChoices.Count == AppRoot.I.Players.Count)
      {
        int choice = PlayerChoices.TrueForAll(c => c.Choice == choiceIndex)
          ? choiceIndex
          : RandomTool.D.NextChoice(PlayerChoices).Choice;

        if (Outcome == null)
        {
          SelectEvent(choice);
        }
        else
        {
          OutcomeContinue();
        }
      }
    }

    private void SelectEvent(int choice)
    {
      var eventChoice = TravelEvent.Choices[choice];
      Outcome = EventOutcomeChooser.EventOutcome(eventChoice.Outcomes, new OutcomeCheckData());

      if (!string.IsNullOrEmpty(Outcome.EventId))
      {
        TravelEvent = GetTravelEvent(Outcome.EventId);
      }

      CleanUpChoices();
    }

    private void InitViews()
    {
      //todo
      GameObject
        .FindObjectOfType<EventChoiceView>()
        .SetState(this);
    }

    public void MakeChoice(int index)
    {
      if (_choiceMade)
      {
        return;
      }

      _choiceMade = true;

      foreach (var playerProfile in AppRoot.I.Players.AllLocal)
      {
        new MakeEventChoiceCmd(new MakeEventChoiceCmd.Input
        {
          PlayerProfileId = playerProfile.Id,
          Choice = index
        }).Execute();
      }
    }

    private void OutcomeContinue()
    {
      if (Outcome.IsFight)
      {
        StartFight();
      }
      else
      {
        if (_goToPreEvent)
        {
          AppRoot.I.SetState(new PreEventState());
        }
        else
        {
          Outcome = null;
          GetRandomEvent();
        }
      }
    }

    private Event GetTravelEvent(string name)
    {
      return AppRoot.I.LuaLoader.EventsLoader.GetEvent(name);
    }

    private void GetRandomEvent()
    {
      var eventsLoader = AppRoot.I.LuaLoader.EventsLoader;
      var available = eventsLoader.SelectAvailable(AppRoot.I.GameStateData.Difficulty);

      if (available.Count == 0)
      {
        Debug.LogError("No available events!");
        return;
      }

      CleanUpChoices();

      var resultEvent = "";
      var tries = 0;
      while (string.IsNullOrEmpty(resultEvent)
             || resultEvent == AppRoot.I.GameStateData.PreviousEvent)
      {
        resultEvent = RandomTool.D.NextChoice(available);
        if(tries++ > 1000)
          break;
      }

      if (!string.IsNullOrEmpty(AppRoot.I.Config.LaunchEvent))
        resultEvent = AppRoot.I.Config.LaunchEvent;

      if (resultEvent == null)
      {
        Debug.LogError("Travel event name did not load!");
        return;
      }

      AppRoot.I.GameStateData.PreviousEvent = resultEvent;
      TravelEvent = GetTravelEvent(resultEvent);

      if (TravelEvent == null)
      {
        Debug.LogError("Travel event did not load!");
        return;
      }
    }

    private void CleanUpChoices()
    {
      _choiceMade = false;
      PlayerChoices.Clear();
    }

    private void StartFight()
    {
      if (Outcome.IsFight)
      {
        AppRoot.I.SetState(new FightState(Outcome.Fight, Outcome.Reward));
      }
    }

    private void AddOutcomeRewards()
    {
      if (Outcome.Reward == null)
      {
        return;
      }

      var players = AppRoot.I.Players.GetAll();

      foreach (var item in Outcome.Reward.Items)
      {
        var player = RandomTool.D.NextChoice(players);
        player.Inventory.AddItem(item);
      }

      foreach (var unit in Outcome.Reward.Units)
      {
        var player = RandomTool.D.NextChoice(players);
        player.AddCreature(unit);
      }
    }
  }
}