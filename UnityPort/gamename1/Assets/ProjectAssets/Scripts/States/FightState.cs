﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Fight;
using GameName1.Levels;
using GameName1.Travel;
using GameName1.UI;
using GameName1.View;
using UnityEngine;


namespace GameName1.States
{
  public class FightStateCommandRunner
  {
    private readonly Queue<Action> _cmds = new Queue<Action>();

    public void Schedule(Action cmd)
    {
      _cmds.Enqueue(cmd);
    }

    public void ExecuteNextCmd()
    {
      if(_cmds.Count == 0)
        return;

      var cmd = _cmds.Dequeue();
      cmd();
    }
  }

  public partial class FightState : StateBase
  {
    public event Action PlayerStepStarted;

    public FightStateCommandRunner CommandRunner;

    public FightManager FightManager
    {
      get { return _fightManager; }
    }

    public World World
    {
      get { return _world; }
    }

    public List<FightPlayer> HumanFightPlayers
    {
      get { return _humanFightPlayers; }
    }

    public static FightState I { get; private set; }
    public bool FightEnded {
      get { return _fightFinished; }
    }

    private World _world;

    private readonly FightInfo _fightInfo;
    private readonly EventOutcomeReward _reward;
    private List<FightPlayer> _otherBattleFightPlayers;
    private FightManager _fightManager;
    private List<FightPlayer> _humanFightPlayers;

    private bool _fightFinished;
    private SpawnManager _spawnManager;
    private TileMapView _tileMapView;

    public FightState(FightInfo fightInfo, EventOutcomeReward reward)
      : base("fightScene")
    {
      _fightInfo = fightInfo;
      _reward = reward;

      CommandRunner = new FightStateCommandRunner();
    }

    protected override void OnActivate()
    {
      I = this;
      var levelWrapper = AppRoot.I.LuaLoader.LevelLoader.GetLevel(_fightInfo.LevelName);
      levelWrapper.Init(_fightInfo.LevelParams);

      _fightManager = new FightManager();

      _world = new World();

      var level = levelWrapper.GenLevel();
      Sprites.LoadSpriteAtlas(level.SpriteAtlas);
      _world.GoTo(level);
      _spawnManager = new SpawnManager(level);

      var fx = levelWrapper.GenFx(level);
      foreach (var actor in fx)
      {
        _world.AddActor(actor);
      }

      InitFightManager();
      InitViews();

      _fightManager.Start();

      _world.ActorManager.ActorAdded += ActorManagerOnActorAdded;

      AudioManager.I.Play("Forest");
    }

    protected override void OnDeactivate()
    {
      I = null;
    }

    private void ActorManagerOnActorAdded(Actor actor)
    {
      if (actor is CreatureActor)
      {
        var creature = actor as CreatureActor;
        creature.Data.PlayerCreator.AddCreature(creature);
      }
    }

    private void InitViews()
    {
      GameObject
        .FindObjectOfType<FightStateView>()
        .SetState(this);
      GameObject
        .FindObjectOfType<SkillBarView>()
        .SetState(this);
      GameObject
        .FindObjectOfType<HudView>()
        .SetState(this);

      GameObject
        .FindObjectOfType<SelectedCreatureView>()
        .SetTileMap(_world.TileMap);

      _tileMapView = GameObject
        .FindObjectOfType<TileMapView>();

      _tileMapView.SetTileMap(_world.TileMap, _world.Level.SpriteAtlas);

      var light = GameObject.FindObjectOfType<Light>();
      light.intensity = _world.Level.LightIntensity;
      light.color = _world.Level.LightColor;
    }

    private void DeinitViews()
    {
      GameObject.Destroy(GameObject
        .FindObjectOfType<SkillBarView>()
        .gameObject);

      GameObject.Destroy(GameObject
        .FindObjectOfType<SelectedCreatureView>()
        .gameObject);

      GameObject.Destroy(GameObject
        .FindObjectOfType<HudView>()
        .gameObject);
    }

    private void CreateOtherPlayer()
    {
      _otherBattleFightPlayers = new List<FightPlayer>();

      foreach (var sideGroup in _fightInfo.Creatures.GroupBy(c => c.Side))
      {
        var creatures = sideGroup.ToList();

        var otherBattleFightPlayer = new LuaAIPlayer(
          World.TileMap,
          AppRoot.I.LuaLoader.AiLoader,
          _fightManager)
        {
          Side = sideGroup.Key
        };

        var creaturesActors = ConvertCreatureDataToActors(creatures.ToList());
        otherBattleFightPlayer.SetCreatures(creaturesActors);

        var spawnPointIndex = otherBattleFightPlayer.AliveCreatures.First().Data.SpawnPointIndex;//todo: group by spawn point and spawn on different spawn points if needed
        var spawnPoint = _world.Level.GetSpawnPoint(spawnPointIndex);
        SpawnCreatures(spawnPoint, otherBattleFightPlayer.AliveCreatures);

        _otherBattleFightPlayers.Add(otherBattleFightPlayer);
      }
    }

    private List<CreatureActor> ConvertCreatureDataToActors(List<UnitData> units)
    {
      var actors = new List<CreatureActor>();

      foreach (var unit in units)
      {
        var creatureActor = new CreatureActor(_world.TileMap, unit);

        actors.Add(creatureActor);
        _world.AddActor(creatureActor);
      }

      return actors;
    }

    private void CreatePlayers()
    {
      _humanFightPlayers = new List<FightPlayer>();

      var listCreaturesAll = new List<CreatureActor>();

      foreach (var profile in AppRoot.I.Players.GetAll())
      {
        var aiType = GetAiType(profile);
        var humanPlayer = new LuaAIPlayer(World.TileMap, AppRoot.I.LuaLoader.AiLoader, _fightManager, aiType);

        if (profile.Local)
        {
          humanPlayer.MarkLocal();
        }

        humanPlayer.StepStarted += OnPlayerStepStarted;

        var creaturesActors = ConvertCreatureDataToActors(profile.Creatures);
        humanPlayer.SetCreatures(creaturesActors);


        humanPlayer.ActivateCreatures(true);

        _humanFightPlayers.Add(humanPlayer);
        listCreaturesAll.AddRange(humanPlayer.AliveCreatures);
      }

      var spawnPointIndex = listCreaturesAll.First().Data.SpawnPointIndex;
      var spawnPoint = _world.Level.GetSpawnPoint(spawnPointIndex);

      SpawnCreatures(spawnPoint, listCreaturesAll);
    }

    private static string GetAiType(PlayerProfile profile)
    {
      string aiType;
      if (profile.Local)
      {
        aiType = "aiHumanPlayer";
        if (AppRoot.I.Config.AutoGame)
        {
          aiType = "aiAutoSkillUser";
        }
      }
      else
        aiType = "aiHumanDummy";
      return aiType;
    }

    private void SpawnCreatures(SpawnPoint spawnPoint, List<CreatureActor> listCreaturesAll)
    {
      switch (spawnPoint.SpawnAlgorithm)
      {
        case SpawnAlgorithmType.Column:
          _spawnManager.SetColumn(listCreaturesAll.OrderBy(c => c.Data.Type).ToList());
          break;
        case SpawnAlgorithmType.Random:
          _spawnManager.SetRandom(listCreaturesAll);
          break;
        case SpawnAlgorithmType.Fill:
          _spawnManager.SetFill(listCreaturesAll.OrderBy(c => c.Data.Type).Reverse().ToList());
          break;
        default:
          throw new ArgumentOutOfRangeException();
      }
    }

    private void InitFightManager()
    {
      CreatePlayers();
      CreateOtherPlayer();

      var players = new List<FightPlayer>();
      players.AddRange(_humanFightPlayers);
      players.AddRange(_otherBattleFightPlayers);

      _fightManager.SetPlayers(players);
    }

    public override void Update()
    {
      if (PopupTextThrower.I != null)
        PopupTextThrower.I.Update();

      if (_fightFinished)
        return;

      if (_fightManager == null)
        return;

      UpdateCommandRunner();

      _fightManager.Update();
      _world.Update();

      CheckGameEnd();
      UpdateCheats();
    }

    private void UpdateCommandRunner()
    {
      if (FightManager.CurrentCreatureActor == null 
        || !FightManager.CurrentCreatureActor.ActionRunning)
      {
        CommandRunner.ExecuteNextCmd();
      }
    }

    private void CheckGameEnd()
    {
      if (_fightManager.FightEnded)
      {
        if (_fightManager.MainPlayerLost)
        {
          AppRoot.I.SetState(new FightLostState());
        }
        else
        {
          AppRoot.I.StartCoroutine(GoToFightCompleted());
        }
        _fightFinished = true;
        DeinitViews();
        // AppRoot.I.LuaLoader.CleanCache();
      }
    }

    private IEnumerator GoToFightCompleted()
    {
      yield return new WaitForSeconds(0.25f);

      AppRoot.I.GameStateData.Difficulty += AppRoot.I.Config.DifficultyMult;

      AppRoot.I.SetState(new FightCompletedState(new FightCompletionData
        {
          FightInfo = _fightInfo,
          HumanFightPlayers = _humanFightPlayers,
          OtherFightPlayers = _otherBattleFightPlayers,
          Reward = _reward
        }), false);
    }

    private void OnPlayerStepStarted()
    {
      if (PlayerStepStarted != null)
        PlayerStepStarted();
    }
  }
}