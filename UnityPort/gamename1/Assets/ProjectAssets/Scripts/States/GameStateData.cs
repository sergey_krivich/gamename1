﻿using SLua;

namespace GameName1
{
  [CustomLuaClass]
  public class GameStateData
  {
    //dificulty rises after every battle
    public float Difficulty;
    public string PreviousEvent;
  }
}
