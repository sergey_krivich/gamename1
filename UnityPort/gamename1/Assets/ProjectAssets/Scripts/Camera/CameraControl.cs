﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using UnityEngine;
using TileHelper = GameName1.Levels.TileHelper;
using Color = UnityEngine.Color;

public class CameraControl : MonoBehaviour
{
  public float Speed;
  public Transform Target;
  public float Rotation;
  public float Zoom = 1f;
  public Camera Camera;

  public float Distance = 10f;

  public float MinSpeed;

  private readonly List<RumbleData> _rumbles = new List<RumbleData>(16);
  private readonly List<RumbleSinData> _sinRumbles = new List<RumbleSinData>(16);

  private readonly RumbleData _rumbleMain = new RumbleData();
  private bool __DEBUG_BOUNDS = false;
  public int pixelScale = 1;

  private Vector3 _currentPosition;

  private void Awake()
  {
    I = this;
    Camera = GetComponent<Camera>();
    _currentPosition = transform.position;
  }

  private void Update()
  {
    if (Target == null)
    {
      return;
    }

    Vector3 targetPos = Target.position + new Vector3(0, 0, -Distance);


    Vector3 moveVector = -_currentPosition + Vector3.Lerp(_currentPosition, targetPos, Speed * Time.deltaTime);
    if (moveVector.magnitude < MinSpeed*Time.deltaTime)
    {
      moveVector = moveVector.normalized*MinSpeed*Time.deltaTime;
    }


    if (Vector3.Distance((moveVector + _currentPosition), targetPos) > Vector3.Distance(_currentPosition, targetPos))
    {
      moveVector = Vector3.zero;
      _currentPosition = targetPos;
    }
    else
    {
      _currentPosition = _currentPosition + moveVector; // Vector3.Lerp(curPos, targetPos, Speed*Time.deltaTime);
    }

    //Camera.orthographicSize = Screen.height * ((halfScreen / pixelsPerUnit) / (Mathf.Sqrt(Screen.dpi)/ pixelScale));

    _currentPosition += UpdateRumbles();

    transform.position = _currentPosition + UpdateSinRumbles();
  }


  public void Rumble(float seconds, float amount, int pushesCount)
  {
    _rumbleMain._rumble = true;
    _rumbleMain._rumbleTime = seconds;
    _rumbleMain._rumbleAmount = amount;
    _rumbleMain._onePushTime = seconds/pushesCount;
    _rumbleMain._rumbleAmountStep = 0;
  }

  public void Rumble(float seconds, float startAmount, float endAmount, int pushesCount)
  {
    _rumbleMain._rumble = true;
    _rumbleMain._rumbleTime = seconds;
    _rumbleMain._rumbleAmount = startAmount;
    _rumbleMain._onePushTime = seconds/pushesCount;
    _rumbleMain._rumbleAmountStep = (endAmount - startAmount)/pushesCount;
  }

  public void RumbleAdditive(float seconds, float amount, int pushesCount)
  {

    _rumbles.Add(new RumbleData
    {
      _rumble = true,
      _rumbleTime = seconds,
      _rumbleAmount = amount,
      _onePushTime = seconds/pushesCount,
      _rumbleAmountStep = 0,
      _temp = 0
    });

  }

  public void RumbleAdditive(float seconds, float startAmount, float endAmount, int pushesCount)
  {
    _rumbles.Add(new RumbleData
    {
      _rumble = true,
      _rumbleTime = seconds,
      _rumbleAmount = startAmount,
      _onePushTime = seconds/pushesCount,
      _rumbleAmountStep = (endAmount - startAmount)/pushesCount,
      _temp = 0
    });
  }

  public void RumbleDirectionSin(float seconds, float startAmount, float endAmount, float amplitudeStart, float amplitudeEnd, Vector2 direction)
  {
    _sinRumbles.Add(new RumbleSinData
    {
      _rumble = true,
      _rumbleTime = seconds,
      _rumbleAmount = startAmount,
      _rumbleEndAmount = startAmount,
      _direction = direction,
      _rumbleAmplitude = amplitudeStart,
      _rumbleEndAmplitude = amplitudeEnd
    });
  }

  private Vector3 UpdateRumbles()
  {
    Vector3 offset = new Vector3();

    foreach (var rumble in _rumbles.ToList())
    {
      offset += UpdateRumble(rumble);
    }

    offset += UpdateRumble(_rumbleMain);

    _rumbles.RemoveAll(p => !p._rumble);

    return offset;
  }


  private Vector3 UpdateSinRumbles()
  {
    Vector3 offset = new Vector3();

    foreach (var rumble in _sinRumbles.ToList())
    {
      offset += UpdateSinRumble(rumble);
    }

    _sinRumbles.RemoveAll(p => !p._rumble);
    return offset;
  }

  private Vector3 UpdateRumble(RumbleData rumble)
  {
    Vector3 offset =new Vector3();
    rumble._rumbleTime -= Time.deltaTime;

    if (rumble._rumbleTime >= 0)
    {
      rumble._temp += Time.deltaTime;
      if (rumble._temp >= rumble._onePushTime)
      {
        short diapazon;
        if (RandomTool.U.NextInt(0, 2) == 1)
          diapazon = -1;
        else
          diapazon = 1;

        Vector2 direction = new Vector2((float) RandomTool.U.NextDouble()*diapazon,
          (float) RandomTool.U.NextDouble()*diapazon);
        direction.Normalize();

        rumble._rumbleAmount += rumble._rumbleAmountStep;
        direction *= (rumble._rumbleAmount);

        offset += (Vector3)direction;

        rumble._temp -= rumble._onePushTime;
      }
    }
    else
    {
      rumble._rumble = false;
    }

    return offset;
  }

  private Vector3 UpdateSinRumble(RumbleSinData rumble)
  {
    Vector3 offset = new Vector3();
    rumble._elapsed += Time.deltaTime;

    if (rumble._elapsed >= rumble._rumbleTime)
    {
      float currentRumbleAmount = Mathf.Lerp(rumble._rumbleAmount, rumble._rumbleEndAmount,
        rumble._elapsed/rumble._rumbleTime);

      float currentRumbleAmplitude = Mathf.Lerp(rumble._rumbleAmplitude, rumble._rumbleEndAmplitude,
        rumble._elapsed/rumble._rumbleTime);


      offset += (Vector3) (rumble._direction*Mathf.Sin(rumble._elapsed*currentRumbleAmplitude)*currentRumbleAmount);
    }
    else
    {
      rumble._rumble = false;
    }

    return offset;
  }

  private class RumbleData
  {
    public bool _rumble;
    public double _rumbleTime;
    public float _rumbleAmount;
    public float _rumbleAmountStep;
    public double _onePushTime;
    public double _temp;
  }

  private class RumbleSinData
  {
    public bool _rumble;
    public float _rumbleTime;
    public float _elapsed;
    public Vector2 _direction;

    public float _rumbleAmount;
    public float _rumbleEndAmount;

    public float _rumbleAmplitude;
    public float _rumbleEndAmplitude;
  }

  public static CameraControl I { get; private set; }

  public Rect Bounds
  {
    get
    {
      float height = 2f*Camera.orthographicSize;
      float width = height*Camera.aspect;


      Rect bounds = new Rect();

      float scrWidth = width;
      float scrHeight = height;

      bounds.x = ((transform.position.x - scrWidth/2));
      bounds.y = ((transform.position.y - scrHeight/2));
      bounds.width = (scrWidth);
      bounds.height = (scrHeight);

      bounds.x += ((bounds.width - (bounds.width/Zoom))/2);
      bounds.y += ((bounds.height - (bounds.height/Zoom))/2);
      bounds.width = (bounds.width/Zoom);
      bounds.height = (bounds.height/Zoom);

      if (Rotation%Mathf.PI*2 != 0)
      {
        Vector2 lt = new Vector2(-bounds.width/2, -bounds.height/2);
        Vector2 rt = new Vector2(bounds.width/2, -bounds.height/2);
        Vector2 rb = new Vector2(bounds.width/2, bounds.height/2);
        Vector2 lb = new Vector2(-bounds.width/2, bounds.height/2);
        Quaternion rot = Quaternion.AngleAxis(Rotation, new Vector3(0, 0, 1));

        lt = rot*lt;
        rt = rot*rt;
        rb = rot*rb;
        lb = rot*lb;

        lt.x += bounds.x + bounds.width/2;
        lt.y += bounds.y + bounds.height/2;
        rt.x += bounds.x + bounds.width/2;
        rt.y += bounds.y + bounds.height/2;
        rb.x += bounds.x + bounds.width/2;
        rb.y += bounds.y + bounds.height/2;
        lb.x += bounds.x + bounds.width/2;
        lb.y += bounds.y + bounds.height/2;
        bounds.x = (int) Math.Round(Math.Min(Math.Min(lt.x, rt.x), Math.Min(rb.x, lb.x)));
        bounds.y = (int) Math.Round(Math.Min(Math.Min(lt.y, rt.y), Math.Min(rb.y, lb.y)));
        bounds.width = (int) Math.Round(Math.Max(Math.Max(lt.x, rt.x), Math.Max(rb.x, lb.x))) - bounds.x;
        bounds.height = (int) Math.Round(Math.Max(Math.Max(lt.y, rt.y), Math.Max(rb.y, lb.y))) - bounds.y;
      }

      var topLeft = new Vector3(bounds.xMin, bounds.yMin);
      var topRight = new Vector3(bounds.xMax, bounds.yMin);
      var bottomLeft = new Vector3(bounds.xMin, bounds.yMax);
      var bottomRight = new Vector3(bounds.xMax, bounds.yMax);

      if (__DEBUG_BOUNDS)
      {
        Debug.DrawLine(topLeft, topRight, Color.red);
        Debug.DrawLine(topLeft, bottomLeft, Color.red);
        Debug.DrawLine(topRight, bottomRight, Color.red);
        Debug.DrawLine(bottomLeft, bottomRight, Color.red);
      }

      return bounds;
    }
  }

  public Rectangle TileBounds
  {
    get
    {
      Rect bounds = Bounds;
      Rectangle result = new Rectangle();

      result.X = (int) (Mathf.Round(bounds.x/TileHelper.TileSize));
      result.Y = (int) (Mathf.Round(bounds.y/GameName1.Levels.TileHelper.TileSize));
      result.Width = (int) (Mathf.Round(bounds.width/GameName1.Levels.TileHelper.TileSize) + 2);
      result.Height = (int) (Mathf.Round(bounds.height/GameName1.Levels.TileHelper.TileSize) + 2);

      return result;
    }
  }
}
