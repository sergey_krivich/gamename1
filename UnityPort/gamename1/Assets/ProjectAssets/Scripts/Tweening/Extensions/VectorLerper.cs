using System;
using UnityEngine;

namespace Glide
{
    class VectorLerper : Lerper
    {
        Vector4 from, to, range;
        int vectorSize;

        public override void Initialize(object fromValue, object toValue, Behavior behavior)
        {
            vectorSize = Math.Min(GetVectorSize(fromValue), GetVectorSize(toValue));

            if (GetVectorSize(fromValue) == 0)
            {
                throw new ArgumentException("Argument \"fromValue\" is not a vector type");
            }
            if (GetVectorSize(toValue) == 0)
            {
                throw new ArgumentException("Argument \"toValue\" is not a vector type");
            }

            from = GetVector(fromValue);
            to = GetVector(toValue);
            range = to - from;
        }

        public override object Interpolate(float t, object current, Behavior behavior)
        {
            var x = from.x + range.x * t;
            var y = from.y + range.y * t;
            var z = from.z + range.z * t;
            var w = from.w + range.w * t;

            if (behavior.HasFlag(Behavior.Round))
            {
                x = (float)Math.Round(x);
                y = (float)Math.Round(y);
                z = (float)Math.Round(z);
                w = (float)Math.Round(w);
            }

            var currentVal = GetVector(current);
            if (range.x != 0) currentVal.x = x;
            if (range.y != 0) currentVal.y = y;
            if (range.z != 0) currentVal.z = z;
            if (range.w != 0) currentVal.w = w;
            return ClampVector(currentVal, vectorSize);
        }

        int GetVectorSize(object vectorObject)
        {
            if (vectorObject is Vector2)
            {
                return 2;
            }
            else if (vectorObject is Vector3)
            {
                return 3;
            }
            else if (vectorObject is Vector4)
            {
                return 4;
            }
            return 0;
        }

        Vector4 GetVector(object vectorObject)
        {
            if (vectorObject is Vector2)
            {
                return (Vector2)vectorObject;
            }
            else if (vectorObject is Vector3)
            {
                return (Vector3)vectorObject;
            }
            else if (vectorObject is Vector4)
            {
                return (Vector4)vectorObject;
            }
            return Vector4.zero;
        }

        object ClampVector(Vector4 vector, int vectorSize)
        {
            if(vectorSize == 2)
            {
                return new Vector2(vector.x, vector.y);
            }
            else if(vectorSize == 3)
            {
                return new Vector3(vector.x, vector.y, vector.z);
            }
            return vector;
        }
    }
}
