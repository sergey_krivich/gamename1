﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameName1.Items;
using UnityEngine;

public class AudioManager : MonoBehaviour,IAudioManager
{
    private List<AudioSource> Clips = new List<AudioSource>();

    private void Start()
    {
        Clips = GetComponentsInChildren<AudioSource>().ToList();
        if (_i != null && !_fromSingletone)
        {
            Debug.LogError("Audio manager loaded twice");
        }
        _i = this;
    }

    public void Play(string sound)
    {
        var data = Clips.Find(p => p.gameObject.name == sound);
        if (data != null)
        {
            data.Play();
        }
        else
        {
            foreach (var audioSource in Clips)
            {
                Debug.Log(audioSource.gameObject.name);
                
            }
            Debug.LogWarning("No sound: " + sound);
        }
    }

    public void PlayWeaponHit()
    {
      Play("SwordHit");
    }


    private bool _fromSingletone;
    private static IAudioManager _i;
    public static IAudioManager I {
        get
        {
            if (_i == null)
            {
                GameObject go = new GameObject("AudioManager");
                var audioManager = go.AddComponent<AudioManager>();
                audioManager._fromSingletone = true;

                _i = audioManager;
            }

            return _i;
        }
    }
}

public interface IAudioManager
{
    void Play(string sound);
    void PlayWeaponHit();
}