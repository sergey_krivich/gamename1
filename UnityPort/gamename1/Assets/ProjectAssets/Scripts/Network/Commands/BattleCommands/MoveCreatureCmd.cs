﻿using System;
using GameName1.Actors;
using GameName1.States;
using SLua;
using UnityEngine;

namespace GameName1.Network.Commands
{
  [CustomLuaClass]
  public class MoveCreatureCmd:GameCmd<MoveCreatureCmd.Input>
  {
    [Serializable]
    [CustomLuaClass]
    public class Input
    {
      public int CreatureId;
      public Point Target;
    }

    public MoveCreatureCmd(Input data) 
      : base(data)
    {
    }

    protected override void Apply()
    {
      FightState.I.CommandRunner.Schedule(() =>
      {
        CreatureActor creature = ActorManager.I.GetById<CreatureActor>(Data.CreatureId);
        if (creature != null)
        {
          if (creature.Move(Data.Target.X, Data.Target.Y))
          {

          }
        }
      });
    }
  }
}
