﻿using SLua;

namespace GameName1.Network.Commands
{
  [CustomLuaClass]
  public static class LuaCommandRunner
  {
    [StaticExport]
    public static void MoveCmd(MoveCreatureCmd.Input input)
    {
      new MoveCreatureCmd(input).Execute();
    }

    [StaticExport]
    public static void UseSkillCmd(UseSkillCmd.Input input)
    {
      new UseSkillCmd(input).Execute();
    }

    [StaticExport]
    public static void ChangeCreatureCmd(ChangeCreatureCmd.Input input)
    {
      new ChangeCreatureCmd(input).Execute();
    }

    [StaticExport]
    public static void SkipTurnCmd(CreatureSkipTurnCmd.Input input)
    {
      new CreatureSkipTurnCmd(input).Execute();
    }
  }
}
