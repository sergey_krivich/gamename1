﻿using System;
using GameName1.Actors;
using GameName1.States;
using SLua;

namespace GameName1.Network.Commands
{
  [CustomLuaClass]
  public class CreatureSkipTurnCmd : GameCmd<CreatureSkipTurnCmd.Input>
  {
    [Serializable]
    [CustomLuaClass]
    public class Input
    {
      public int CreatureId;
    }

    public CreatureSkipTurnCmd(Input data)
      : base(data)
    {
    }

    protected override void Apply()
    {
      FightState.I.CommandRunner.Schedule(() =>
      {
        CreatureActor creature = ActorManager.I.GetById<CreatureActor>(Data.CreatureId);
        if (creature != null)
        {
          creature.Skip();
        }
      });
    }
  }
}
