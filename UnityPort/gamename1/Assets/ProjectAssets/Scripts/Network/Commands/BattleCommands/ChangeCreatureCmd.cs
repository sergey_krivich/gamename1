﻿using System;
using GameName1.Actors;
using GameName1.States;
using SLua;

namespace GameName1.Network.Commands
{
  [CustomLuaClass]
  public class ChangeCreatureCmd:GameCmd<ChangeCreatureCmd.Input>
  {
    [CustomLuaClass]
    [Serializable]
    public class Input
    {
      public int CreatureId;
    }

    public ChangeCreatureCmd(Input data) : base(data)
    {
    }

    protected override void Apply()
    {
      FightState.I.CommandRunner.Schedule(() =>
      {
        var creature = ActorManager.I.GetById<CreatureActor>(Data.CreatureId);
        FightState.I.FightManager.CurrentFightPlayer.SelectCreature(creature);
      });
    }
  }
}
