﻿using System;
using GameName1.States;
using GameName1.Travel;

namespace GameName1.Network.Commands
{
  public class MakeEventChoiceCmd : GameCmd<MakeEventChoiceCmd.Input>
  {
    [Serializable]
    public class Input
    {
      public long PlayerProfileId;
      public int Choice;
    }

    public MakeEventChoiceCmd(Input data) 
      : base(data)
    {
    }

    protected override void Apply()
    {
      AppRoot.I.EventDispatcher.Post(new ChoiceMadeEventData
      {
        PlayerProfile = AppRoot.I.Players.Get(Data.PlayerProfileId),
        Choice = Data.Choice
      });
    }
  }
}
