﻿using System;
using GameName1.Data;
using GameName1.Items;
using UnityEngine;

namespace GameName1.Network.Commands
{
  public class HealUnitWithItemCmd:GameCmd<HealUnitWithItemCmd.Input>
  {
    [Serializable]
    public class Input
    {
      public long PlayerId;
      public int CreatureId;
      public string ItemId;
    }

    public class UnitHealedEventData
    {
      public UnitData Unit;
    }

    public HealUnitWithItemCmd(Input data) 
      : base(data)
    {
    }

    protected override void Apply()
    {
      var player = AppRoot.I.Players.Get(Data.PlayerId);
      var creature =player.GetCreature(Data.CreatureId);

      var item = player.Inventory.GetItemById(Data.ItemId);
      if (item.Type.Group != ItemGroup.HpPotion)
      {
        Debug.LogError("Should be potion item.Type.Group: "+ item.Type.Group);
        return;
      }

      player.Inventory.RemoveItem(Data.ItemId, 1);

      creature.AddHpPercent(0.25f);
      AppRoot.I.EventDispatcher.Post(new UnitHealedEventData
      {
        Unit = creature
      });
    }
  }
}
