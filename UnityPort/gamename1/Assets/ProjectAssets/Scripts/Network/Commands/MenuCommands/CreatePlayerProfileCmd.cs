﻿using System;
using GameName1.Actors;
using GameName1.Travel;
using GameName1.Data;
using UnityEngine;
using SLua;

namespace GameName1.Network.Commands
{
  public class CreatePlayerProfileCmd:GameCmd<CreatePlayerProfileCmd.Input>
  {
    private readonly bool _local;

    [Serializable]
    public class Input
    {
      public string Nickname;
      public long Id;
    }

    public CreatePlayerProfileCmd(Input data) 
      : base(data)
    {
    }

    public CreatePlayerProfileCmd(Input data, bool local)
      : base(data)
    {
      _local = local;
    }

    protected override void Apply()
    {
      var profile = new PlayerProfile(Data.Id, Data.Nickname, _local);
      var enemies = AppRoot.I.LuaLoader.State["enemies"] as LuaTable;
      var playerUnits = enemies["playerUnits"] as LuaTable;

      if (Data.Nickname == "Boris")
      {
        profile.AddCreature(playerUnits["warrior"] as UnitData);
        profile.AddCreature(playerUnits["archer"] as UnitData);
        profile.AddCreature(playerUnits["mage"] as UnitData);
      }
      else
      {
        profile.AddCreature(playerUnits["warrior2"] as UnitData);
        profile.AddCreature(playerUnits["archer2"] as UnitData);
        profile.AddCreature(playerUnits["mage2"] as UnitData);
      }

      AppRoot.I.Players.Add(profile);
    }
  }
}
