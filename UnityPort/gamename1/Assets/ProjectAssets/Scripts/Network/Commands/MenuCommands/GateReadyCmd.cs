﻿using System;

namespace GameName1.Network.Commands
{
  public class GateReadyCmd:GameCmd<GateReadyCmd.Input>
  {
    [Serializable]
    public class Input
    {
      public long PlayerId;
    }

    public GateReadyCmd(Input data) 
      : base(data)
    {
    }

    protected override void Apply()
    {
      AppRoot.I.EventDispatcher.Post(new ReadyEventData
      {
        PlayerId = Data.PlayerId
      }, true);
    }
  }
}
