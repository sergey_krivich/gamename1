﻿using SLua;

namespace GameName1.Network.Commands
{
  [CustomLuaClass]
  public abstract class GameCmd
  {
    public abstract void Execute();

    public void ExecuteCommandLocal()
    {
      Apply();
    }

    protected abstract void Apply();
    public abstract string Serialize();
    public abstract void Desirealize(string data);
  }

  [CustomLuaClass]
  public abstract class GameCmd<T>: GameCmd where T:class
  {
    public T Data { get; private set; }

    public GameCmd(T data)
    {
      Data = data;
    }

    public override void Execute()
    {
      NetworkManager.I.SendCmd(this);
      Apply();
    }

    public override string Serialize()
    {
      return Serializer.SerializeObject(Data);
    }

    public override void Desirealize(string data)
    {
      Data = Serializer.DeserializeObject<T>(data);
    }
  }
}
