﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using GameName1.Network.Commands;
using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;

namespace GameName1.Network
{
  public interface INetworkBase
  {
    event Action Connected;

    void Launch();
    void Stop();
    void SendCmd(GameCmd cmd);
  }

  public abstract class NetworkBase<T> : INetworkBase where T : NetBase
  {
    public event Action Connected;

    private int _sent;

    protected readonly object Lock = new object();

    private Thread _thread;
    private bool _running;
    private readonly List<GameCmd> _pendingCmds = new List<GameCmd>();

    private T _net;
    protected T Net { get { return _net; } }

    public virtual void Launch()
    {
      _running = true;
      _thread = new Thread(Start);
      _thread.Start();
    }

    public virtual void Stop()
    {
      _running = false;
      _pendingCmds.Clear();
      Net.Stop();
    }

    public virtual void SendCmd(GameCmd cmd)
    {
      lock (Lock)
      {
        _pendingCmds.Add(cmd);
      }
    }

    protected virtual void Start()
    {
      Debug.Log("start");
      try
      {
        Init();
        UpdateLoop();
      }
      catch (Exception e)
      {
        Debug.LogError(e);
      }
      finally
      {
        Net.Stop();
      }
    }

    protected virtual void Init()
    {
      var listener = CreateListener();
      _net = CreateNet(listener);
    }

    private EventBasedNetListener CreateListener()
    {
      EventBasedNetListener listener = new EventBasedNetListener();
      listener.NetworkReceiveEvent += ListenerOnNetworkReceiveEvent;
      listener.PeerConnectedEvent += OnListenerOnPeerConnectedEvent;
      return listener;
    }

    protected abstract void SendPendingMessages(NetDataWriter writer);

    protected virtual T CreateNet(EventBasedNetListener listener)
    {
      return null;

    }

    protected void NetworkRecieveEvent(NetDataReader reader)
    {
      try
      {
        var messages = new List<Message>();

        while (!reader.EndOfData)
        {
          var message = new Message();
          message.Read(reader);
          messages.Add(message);
        }

        var cmds = new List<GameCmd>();
        cmds = messages.Select(m =>
        {
          var type = Type.GetType(m.Type);
          var cmd = Activator.CreateInstance(type, new object[] {null}) as GameCmd;
          cmd.Desirealize(m.Data);
          return cmd;
        }).ToList();

        Dispatcher.Invoke(() =>
        {
          foreach (var gameCmd in cmds)
          {
            gameCmd.ExecuteCommandLocal();
          }
        });

      }
      catch (Exception e)
      {
        Debug.LogError(e);
      }
    }

    protected void UpdateLoop()
    {
      while (_running)
      {
        UpdatePendingCmds();
        UpdateNetwork();
        Thread.Sleep(100);
      }
    }

    private void UpdatePendingCmds()
    {
      List<GameCmd> commandsToSend;
      lock (Lock)
      {
        commandsToSend = _pendingCmds.ToList();
        _pendingCmds.Clear();
      }

      if (commandsToSend.Count > 0)
      {
        NetDataWriter writer = new NetDataWriter();
        foreach (var cmd in commandsToSend)
        {
          try
          {
            var message = new Message(_sent++, cmd.Serialize(), cmd.GetType().FullName);
            message.Write(writer);
          }
          catch (Exception e)
          {
            Debug.LogError(e);
          }
        }

        SendPendingMessages(writer);
      }
    }

    private void UpdateNetwork()
    {
      _net.PollEvents();
    }

    protected virtual void OnListenerOnPeerConnectedEvent(NetPeer peer)
    {
      Debug.Log("We got connection: " + peer.EndPoint);

      Dispatcher.Invoke(() =>
      {
        if (Connected != null)
        {
          Connected();
        }
      });
    }

    private void ListenerOnNetworkReceiveEvent(NetPeer peer, NetDataReader reader)
    {
      NetworkRecieveEvent(reader);
    }
  }
}
