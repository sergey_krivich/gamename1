﻿using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;

namespace GameName1.Network
{
  public class NetworkClient : NetworkBase<NetClient>
  {
    private readonly string _ip;
    NetPeer _server;

    public NetworkClient(string ip)
    {
      _ip = ip;
    }

    protected override NetClient CreateNet(EventBasedNetListener listener)
    {
      var net = new NetClient(listener, "SomeConnectionKey");
      if (!net.Start())
      {
        Debug.LogError("Net not started");
      }
      net.Connect(_ip, 9050);
      return net;
    }

    protected override void SendPendingMessages(NetDataWriter writer)
    {
      _server.Send(writer, SendOptions.ReliableOrdered);
    }

    protected override void OnListenerOnPeerConnectedEvent(NetPeer peer)
    {
      base.OnListenerOnPeerConnectedEvent(peer);
      Debug.Log("CLient connected " +peer.EndPoint);
      _server = peer;
    }
  }
}
