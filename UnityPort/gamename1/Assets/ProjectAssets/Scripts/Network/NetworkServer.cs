﻿using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;

namespace GameName1.Network
{
  public class NetworkServer : NetworkBase<NetServer>
  {
    protected override NetServer CreateNet(EventBasedNetListener listener)
    {
      var net = new NetServer(listener, 2, "SomeConnectionKey");
      if (!net.Start(9050))
      {
        Debug.LogError("Could not create server!");
      }
      return net;
    }

    protected override void SendPendingMessages(NetDataWriter writer)
    {
      Net.SendToClients(writer, SendOptions.ReliableOrdered);
    }
  }
}
