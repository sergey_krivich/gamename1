﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LiteNetLib.Utils;

namespace GameName1.Network
{
  public class Message
  {
    public string Data { get; set; }
    public int Id { get; set; }
    public string Type { get; set; }

    public Message()
    {
      
    }

    public Message(int id, string data, string type)
    {
      Id = id;
      Data = data;
      Type = type;
    }


    public void Write(NetDataWriter writer)
    {
      writer.Put(Data);
      writer.Put(Type);
      writer.Put(Id);
    }

    public void Read(NetDataReader reader)
    {
      Data = reader.GetString(8096);
      Type = reader.GetString(8096);
      Id = reader.GetInt();
    }
  }
}
