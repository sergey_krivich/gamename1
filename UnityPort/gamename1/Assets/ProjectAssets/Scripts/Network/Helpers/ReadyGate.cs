﻿using System.Collections.Generic;
using GameName1.Network.Commands;
using UnityEngine;

namespace GameName1.Network
{
  public class ReadyEventData
  {
    public long PlayerId;
  }

  public class ReadyGate
  {
    private readonly List<long> _readyPlayers = new List<long>();
    private EventDispatcher.EventHandle _handler;
    private bool _sentReadyCmd;

    public bool AllReady
    {
      get { return _readyPlayers.Count >= AppRoot.I.Players.Count; }
    }

    public void AddReady(long playerId)
    {
      _readyPlayers.Add(playerId);
    }

    public void Subscribe()
    {
      _handler = AppRoot.I.EventDispatcher.Subscribe<ReadyEventData>(OnReady);
    }

    public void Unsubscribe()
    {
      AppRoot.I.EventDispatcher.Unsubscribe(_handler);
    }

    public void OnReady(ReadyEventData data)
    {
      AddReady(data.PlayerId);
    }

    public void SendReadyCmd()
    {
      if(_sentReadyCmd)
        return;

      _sentReadyCmd = true;

      foreach (var playerProfile in AppRoot.I.Players.AllLocal)
      {
        new GateReadyCmd(new GateReadyCmd.Input
        {
          PlayerId = playerProfile.Id
        }).Execute();
      }
    }
  }
}
