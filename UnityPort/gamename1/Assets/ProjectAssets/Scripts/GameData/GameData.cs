﻿using System;
using UnityEngine;

namespace GameName1.Data
{
  public class GameData : MonoBehaviour
  {
    public static GameData I { get; private set; }

    public void Awake()
    {
      I = this;
    }

    public TileData TileData;
  }


  [Serializable]
  public class TileData
  {
    public GameObject TilePrefab;
    public GameObject AvailableTile;
  }
}