﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuplicateSprite : MonoBehaviour
{

  public SpriteRenderer Target;

	void LateUpdate ()
	{
	  GetComponent<SpriteRenderer>().sprite = Target.sprite;
	}
}
