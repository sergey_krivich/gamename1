﻿using System;
using UnityEngine;
using GameName1.Actors;
using GameName1.Levels;
using GameName1.UI;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace GameName1.View
{
  public class CreatureView : ActorView<CreatureActor>
  {
    [SerializeField] private SpriteRenderer _sprite;
    [SerializeField] private SpriteRenderer _outline;
    [SerializeField] private GameObject _moveArrow;
    [SerializeField] private Slider _healthBar;
    [SerializeField] private Slider _staminaBar;
    [SerializeField] private Animator _animator;

    private CreatureActor _actor;
    private Vector3 _defaultArrowPosition;

    private float _elapsed;

    private float _rumbleAmount;
    private float _rumbleTimeOffset;

    protected override void OnAdded(CreatureActor actor)
    {
      Assert.IsNotNull(actor);
      Assert.IsNotNull(_sprite);

      _actor = actor;
      _actor.HealthChanged += ActorOnHealthChanged;
      _actor.StaminaChanged += ActorOnStaminaChanged;

      if (!string.IsNullOrEmpty(actor.Data.Animator))
      {
        var animator = ResourceManager.LoadAnimator(actor.Data.Animator);
        Assert.IsNotNull(animator, actor.Data.Animator);
        _animator.runtimeAnimatorController = animator;
        _animator.speed = RandomTool.U.NextSingle(0.7f, 1.4f);
      }
      else if (!string.IsNullOrEmpty(actor.Data.Sprite))
      {
        var sprite = ResourceManager.LoadSprite(actor.Data.Sprite);
        Assert.IsNotNull(sprite, actor.Data.Sprite);
        _sprite.sprite = sprite;
      }
      else
      {
        Debug.LogError("No visual for actor: " + actor.Data.Name + ", " + actor.Data.Id);
      }

      ActorOnHealthChanged(0);
      ActorOnStaminaChanged(0);

      foreach (var player in State.FightManager.Players)
      {
        player.SelectedCreatureChanged += PlayerOnSelectedCreatureChanged;
      }

      _defaultArrowPosition = _moveArrow.transform.localPosition;
    }

    void OnDestroy()
    {
      foreach (var player in State.FightManager.Players)
      {
        player.SelectedCreatureChanged -= PlayerOnSelectedCreatureChanged;
      }
    }

    private void PlayerOnSelectedCreatureChanged(CreatureActor creature)
    {
      if (creature == _actor)
      {
        if (!_moveArrow.activeInHierarchy)
        {
          _elapsed = 0f;
          _moveArrow.transform.localPosition = _defaultArrowPosition;
          _moveArrow.SetActive(true);
        }
      }
      else
      {
        _moveArrow.SetActive(false);
      }
    }

    private void Update()
    {
      var tileUnderMouse = TileHelper.GetTileUnderMouse();
      var creature = World.TileMap.GetCreatureFromTile(tileUnderMouse);

      var highlight = creature == _actor;
      _outline.color = Color.Lerp(_outline.color, highlight
          ? new Color(1, 1f, 1f, 1f)
          : new Color(1, 1f, 1f, 0f),
        Time.deltaTime * 5f);
      _elapsed += Time.deltaTime;
      if (_moveArrow.activeInHierarchy)
      {
        _moveArrow.transform.localPosition += new Vector3(0,Mathf.Sin(_elapsed * 4.5f)*0.15f,0)*Time.deltaTime;
      }

      if (State.FightManager.CurrentCreatureActor != _actor)
      {
        _moveArrow.SetActive(false);
      }

        var rotation = transform.localRotation;
      if(_rumbleAmount > 0)
      {
        rotation.z = Mathf.LerpAngle(rotation.z, Mathf.Sin((_rumbleTimeOffset * _rumbleAmount)) *_rumbleAmount, Time.deltaTime*10f);
        _rumbleTimeOffset += Time.deltaTime;
        _rumbleAmount -= Time.deltaTime;
      }
      else
      {
        rotation.z = Mathf.Lerp(rotation.z, 0f, Time.deltaTime * 3f);
      }
        transform.localRotation = rotation;
    }

    private void ActorOnHealthChanged(float change)
    {
      if (change < 0)
      {
        PopupTextThrower.I.CreatePopUp(change.ToString("F1"), new Color(1, 0, 0, 1),
          new Color(1, 0, 0, 0), _actor.Position, RandomTool.U.NextUnitVector2(), 0.025f,
          0.001f, 3f, 1.5f);

        CameraControl.I.RumbleAdditive(0.1f, 0.02f, 0.001f, 10);
        AudioManager.I.PlayWeaponHit();

        _rumbleAmount = RandomTool.U.NextSingle(0.3f, 0.8f);
        _rumbleTimeOffset = RandomTool.U.NextSingle(0, 40);
      }
      else if (change > 0 && !_actor.IsDead)
      {
        PopupTextThrower.I.CreatePopUp(change.ToString("F0"), new Color(0, 1, 0, 1),
          new Color(1, 0, 0, 0), _actor.Position, RandomTool.U.NextUnitVector2(), 0.025f,
          0.001f, 3f, 1.5f);
      }

      _healthBar.value = _actor.Health / _actor.Stats.MaxHealth;
      _healthBar.gameObject.SetActive(_healthBar.value <= 0.99f);
    }

    private void ActorOnStaminaChanged(float value)
    {
      _staminaBar.value = _actor.Stamina / _actor.Stats.MaxStamina;
      _staminaBar.gameObject.SetActive(_staminaBar.value <= 0.99f);
    }
  }
}
