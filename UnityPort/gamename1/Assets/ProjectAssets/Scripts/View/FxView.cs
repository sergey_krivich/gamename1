﻿using GameName1.Actors;
using GameObjectExtensions;
using UnityEngine;

namespace GameName1.View
{
  public class FxView: ActorView<FxActor>
  {
    private GameObject _subview;

    protected override void OnAdded(FxActor actor)
    {
      _subview = Resources.Load<GameObject>("prefabs/" + actor.View).Create();
      _subview.gameObject.transform.SetParent(gameObject.transform, false);
      transform.position = transform.position + new Vector3(0, 0, actor.Depth);

      TryAttachTo(actor);
    }

    protected override void OnRemoved()
    {
      _subview.SendMessage("Destroyed", SendMessageOptions.DontRequireReceiver);
    }

    private void TryAttachTo(FxActor actor)
    {
      if (actor.AttachTo == null) return;

      var view = Binder.GetView(actor.AttachTo);
      if (view == null) return;

      gameObject.transform.SetParent(view.GameObject.transform, true);
      //transform.position = view.GameObject.transform.position + new Vector3(0, 0, actor.Depth);
    }
  }
}
