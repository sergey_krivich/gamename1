﻿using GameName1.Data;
using GameName1.Tiles;
using GameObjectExtensions;
using UnityEngine;
using UnityEngine.UI;

namespace GameName1.View
{
  public class TileMapView : UIView
  {
    public GameObject TilePrefab;

    private TileMap _tileMap;
    private GameObject _root;

    public void SetTileMap(TileMap tileMap, string atlas)
    {
      if(_root != null)
        Destroy(_root);

      _tileMap = tileMap;

      _root = new GameObject("Tilemap");
      for (var i = 0; i < _tileMap.Width; i++)
      {
        for (var j = 0; j < _tileMap.Height; j++)
        {
          var tileGo = TilePrefab.Create();
          var spriteRenderer = tileGo.GetComponentInChildren<SpriteRenderer>();

          var tile = _tileMap[i, j];

          var spriteFullName = tile.TileData.Sprite + "_" + (int) tile.SpriteIndex;

          if (tile.TileData.SpriteVariations.Count > 1)
          {
            var spriteIndex = RandomTool.D.NextWeightedChoice(tile.TileData.SpriteVariations,
              tile.TileData.SpriteVariationsChance);

            spriteFullName += "_" + spriteIndex;
          }
          spriteRenderer.sprite = Sprites.GetSprite(atlas, spriteFullName);
          tileGo.transform.position = new Vector3(i * Levels.TileHelper.TileSize, (j + 1) * Levels.TileHelper.TileSize, 1);

          tileGo.transform.parent = _root.transform;
        }
      }
    }
  }
}