﻿using System;
using System.Collections.Generic;
using GameName1.Actors;
using GameName1.States;
using GameObjectExtensions;
using UnityEngine;

namespace GameName1.View
{
  public class ActorBinder : MonoBehaviour
  {
    [SerializeField]
    private CreatureView _creaturePrefab;

    [SerializeField]
    private ProjectileView _projectile;

    [SerializeField]
    private FxView _fx;

    private readonly Dictionary<Type, Action<Actor>> _bindings = new Dictionary<Type, Action<Actor>>();
    private readonly Dictionary<Actor, IActorView> _actorViews = new Dictionary<Actor, IActorView>();

    private FightState _state;

    void Awake()
    {
      Bind<CreatureActor, CreatureView>(_creaturePrefab);
      Bind<ProjectileActor, ProjectileView>(_projectile);
      Bind<FxActor, FxView>(_fx);
    }

    public void SetState(FightState world)
    {
      _state = world;
      _state.World.ActorManager.ActorAdded += ActorAdded;
      _state.World.ActorManager.ActorRemoved += ActorRemoved;

      AddExistingActors();
    }

    private void ActorRemoved(Actor obj)
    {
      _actorViews[obj].Remove();
      _actorViews.Remove(obj);
    }

    public IActorView GetView(Actor actor)
    {
      IActorView view;
      _actorViews.TryGetValue(actor, out view);
      return view;
    }

    private void Bind<T, T2>(T2 view) 
      where T : Actor 
      where T2 : ActorView<T> 
    {
      if (view == null)
      {
        Debug.LogError("NULL VIEW " + typeof(T2).Name);
      }

      _bindings[typeof (T)] = (a) =>
      {
        var resultView = view.gameObject.Create<T2>();
        resultView.Set(this, _state, a as T);
        _actorViews[a] = resultView;
      };
    }

    private void AddExistingActors()
    {
      foreach (var actor in _state.World.ActorManager.Actors)
      {
        ActorAdded(actor);
      }
    }

    private void ActorAdded(Actor actor)
    {
      _bindings[actor.GetType()](actor);
    }
  }
}
