﻿using GameName1.Actors;
using GameName1.States;
using UnityEngine;

namespace GameName1.View
{
  public interface IActorView
  {
    GameObject GameObject { get; }
    void Remove();
  }

  public abstract class ActorView<T>:MonoBehaviour, IActorView where T:Actor
  {
    protected World World {
      get { return State.World; }
    }

    public GameObject GameObject
    {
      get { return gameObject; }
    }

    protected FightState State { get; private set; }

    protected ActorBinder Binder { get; private set; }

    private T _actor;

    public void Set(ActorBinder binder, FightState state, T actor)
    {
      State = state;
      Binder = binder;

      _actor = actor;
      _actor.PositionChanged += ActorOnPositionChanged;
      ActorOnPositionChanged();

      OnAdded(actor);
    }

    public void Remove()
    {
      _actor.PositionChanged -= ActorOnPositionChanged;
      OnRemoved();
      Destroy(gameObject);
      _actor = null;
    }

    protected virtual void OnAdded(T actor)
    {
      
    }

    protected virtual void OnRemoved()
    {

    }

    protected virtual void ActorOnPositionChanged()
    {
      //sort z by x position, so head of a creature is not deep inside other creature's ass
      //(in case if creature graphics look to the right) like with mouse bear
      transform.position = (Vector3)_actor.Position + new Vector3(0,0, _actor.Position.x/100f);
    }

    protected T Actor { get { return _actor; } }
  }
}
