﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Actors.ActionSystem;
using GameName1.Fight;
using GameName1.Levels;
using GameName1.PathFinding;
using GameName1.States;
using GameName1.UI;
using GameName1.View;
using Glide;
using SLua;
using UnityEngine;

public class FightStateView : UIView
{
  public Color PathColor = Color.blue;

  private MovementGrid _movementGrid;

  private FightState _state;

  private CreatureActor _currentCreature;
  private List<SelectionCursor> _cursorPath = new List<SelectionCursor>();

  private readonly List<SelectionCursor> _cursorAttackArea = new List<SelectionCursor>();
  private readonly List<SelectionCursor> _skillCursors = new List<SelectionCursor>();

  public void SetState(FightState state)
  {
    _state = state;

    foreach (var player in state.FightManager.Players)
    {
      player.SelectedCreatureChanged += PlayerOnSelectedCreatureChanged;
    }

    GameObject
      .FindObjectOfType<RTSCameraControl>()
      .ClampRect = new Rect(
        0,
        0,
        TileHelper.TileSize*_state.World.TileMap.Width*1f,
        TileHelper.TileSize*_state.World.TileMap.Height*1f);

    PopupTextThrower.Init();

    GameObject
      .FindObjectOfType<ActorBinder>()
      .SetState(state);

    _movementGrid = new MovementGrid(state.World.TileMap);

    _cursorPath= new List<SelectionCursor>();
  }

  void Update()
  {
    if (_state.FightEnded)
    {
      RTSCameraControl.I.enabled = false;
      return;
    }

    UpdateMovementGrid();
    UpdateCursor();
    UpdateCursorSkills();
    UpdateCursorAttackArea();
  }

  private void UpdateCursorAttackArea()
  {
    if (_currentCreature == null ||
      !IsCurrentCreatureLocal() ||
      _currentCreature.ActionRunning
      || _currentCreature.Abilities.SelectedSkill == null)
    {
      HidePathCursor(_cursorAttackArea);
      return;
    }

    var skill = _currentCreature.Abilities.SelectedSkill;

    float range = skill.GetRange();
    var tiles = new List<Point>();

    for (int i = (int)-range; i <= range; i++)
    {
      for (int j = (int)-range; j <= range; j++)
      {
        var target = new Point(_currentCreature.TileX + i, _currentCreature.TileY + j);

        if (!_state.World.TileMap.InRange(target)
            || !skill.IsInRange(target)) continue;

        tiles.Add(target);
      }
    }

    var param = new LuaTable(AppRoot.I.LuaLoader.State);
    param["targetTile"] = TileHelper.GetTileUnderMouse();
    var skillTiles = _currentCreature.Abilities.SelectedSkill.GetTargetTiles(param);

    for (var i = 0; i < tiles.Count; i++)
    {
      var point = tiles[i];

      if (_cursorAttackArea.Count == i)
      {
        _cursorAttackArea.Add(new SelectionCursor());
      }

      var cursor = _cursorAttackArea[i];

      if (point == _currentCreature.Tile || skillTiles.Contains(point))
      {
        cursor.Hide();
      }
      else
      {
        cursor.Show();
      }

      var tile = TileHelper.TileToPosition(point);
      cursor.Update(tile, Color.red);
    }
    for (var i = tiles.Count; i < _cursorAttackArea.Count; i++)
    {
      _cursorAttackArea[i].Hide();
    }
  }

  private void UpdateMovementGrid()
  {
    if (_movementGrid == null) return;
    if (_currentCreature == null 
      || !IsCurrentCreatureLocal() 
      || _currentCreature.Abilities.SelectedSkill != null
      || _currentCreature.ActionRunning)
    {
      _movementGrid.HideMovementGrid();
      return;
    }

    _movementGrid.Update();
  }

  private void UpdateCursor()
  {
    var tileUnderMouse = TileHelper.GetTileUnderMouse();

    if (_currentCreature == null ||
        !IsCurrentCreatureLocal() ||
        _currentCreature.ActionRunning
        || _currentCreature.Abilities.SelectedSkill != null)
    {
      HidePathCursor(_cursorPath);
      return;
    }

    var tileSolidChecker = new TileSolidChecker(_currentCreature);
    var astar = new AStar(_state.World.TileMap, tileSolidChecker.CheckTile);
    var tiles = astar.FindCellWay(_currentCreature.Tile, tileUnderMouse);
    if (tiles == null)
    {
      HidePathCursor(_cursorPath);
      return;
    }
    for (var i = 0; i < tiles.Count; i++)
    {
      var point = tiles[i];

      if (_cursorPath.Count == i)
      {
        _cursorPath.Add(new SelectionCursor());
      }

      var cursor = _cursorPath[i];

      if (point == _currentCreature.Tile)
      {
        cursor.Hide();
        continue;
      }
      else
      {
        cursor.Show();
      }

      var creatureOnTile = _state.World.TileMap.GetCreatureFromTile(point);
      bool enemy = creatureOnTile != null
                   && tileSolidChecker.CheckTile(_state.World.TileMap[point]);
        //if it is solid then there should be creature


      var tile = (Vector3) TileHelper.TileToPosition(point);
      tile.z = 0.03f;
      cursor.Update(tile, enemy ? Color.red : PathColor);
    }
    for (var i = tiles.Count; i < _cursorPath.Count; i++)
    {
      _cursorPath[i].Hide();
    }
  }

  private void HidePathCursor(List<SelectionCursor> cursors)
  {
    foreach (var selectionCursor in cursors)
    {
      selectionCursor.Hide();
    }
  }

  private void UpdateCursorSkills()
  {
    if (_currentCreature == null || 
      _currentCreature.Abilities.SelectedSkill == null ||
      !IsCurrentCreatureLocal())
    {
      HidePathCursor(_skillCursors);
      return;
    }

    var tileUnderMouse = TileHelper.GetTileUnderMouse();
    var param = new LuaTable(AppRoot.I.LuaLoader.State);
    param["targetTile"] = tileUnderMouse;

    bool inRange = _currentCreature.Abilities.SelectedSkill.IsInRange(tileUnderMouse);
    var color = inRange ? Color.green : Color.gray;

    var tiles = _currentCreature.Abilities.SelectedSkill.GetTargetTiles(param);

    for (var i = 0; i < tiles.Count; i++)
    {
      var point = tiles[i];

      if (_skillCursors.Count == i)
      {
        _skillCursors.Add(new SelectionCursor());
      }

      var skillCursor = _skillCursors[i];
      skillCursor.Show();

      var position = (Vector3)TileHelper.TileToPosition(point);
      position.z = 0.05f;
      skillCursor.Update(position, color);
    }
    for (var i = tiles.Count; i < _skillCursors.Count; i++)
    {
      _skillCursors[i].Hide();
    }
  }

  private void PlayerOnSelectedCreatureChanged(CreatureActor creature)
  {
    _currentCreature = creature;
    if (_movementGrid != null)
      _movementGrid.Show(creature);

    RTSCameraControl.I.enabled = false;
    CameraControl.I.Target = RTSCameraControl.I.transform;

    Tweener.I
      .Tween(RTSCameraControl.I.transform, new { position = (Vector3)_state.FightManager.CurrentCreatureActor.Position }, 0.5f)
      .Ease(Ease.CubeOut)
      .OnComplete(() =>
      {
      //  RTSCameraControl.I.enabled = true;
        RTSCameraControl.I.transform.position = RTSCameraControl.I.transform.position;
        CameraControl.I.Target = RTSCameraControl.I.transform;
      });
  }

  private bool IsCurrentCreatureLocal()
  {
    return _state.HumanFightPlayers.Any(p => 
    p.IsLocal &&
    p.AliveCreatures.Contains(_currentCreature));
  }
}
