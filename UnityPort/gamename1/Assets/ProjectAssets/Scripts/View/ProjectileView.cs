﻿using GameName1.Actors;
using GameName1.Levels;
using GameObjectExtensions;
using UnityEngine;

namespace GameName1.View
{
  public class ProjectileView:ActorView<ProjectileActor>
  {
    private GameObject _subview;

    protected override void OnAdded(ProjectileActor actor)
    {
      if (string.IsNullOrEmpty(actor.View))
      {
        Debug.LogError("Empty actor view string");
        return;
      }

      _subview = Resources.Load<GameObject>("prefabs/"+actor.View);

      if (_subview == null)
      {
        Debug.LogError("Could not load projectile: " + actor.View);
      }

      _subview = _subview.Create();

      _subview.gameObject.transform.SetParent(gameObject.transform, false);

      transform.LookAt2D(TileHelper.TileToPosition(actor.TargetTile));
    }

    protected override void OnRemoved()
    {
      _subview.SendMessage("Destroyed", SendMessageOptions.DontRequireReceiver);
    }
  }
}
