﻿using GameName1;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Levels;
using GameName1.Tiles;
using UnityEngine;
using UnityEngine.UI;

public class SelectedCreatureView : UIView
{
  public GameObject Root;
  public bool EnabledOnStart = false;

  public override void OnEnable()
  {
    Root.SetActive(EnabledOnStart);
  }

  private TileMap _tileMap;
  private CreatureActor _selectedCreature;
  private UnitData _unitData;

  public Text Name;
  public Image Sprite;

  public Text Hp;
  public Text Armor;
  public Text Damage;
  public Text Crit;

  void Update()
  {
    if (_tileMap != null)
    {
      UpdateSelection();
      UpdateView();
    }
  }

  private void UpdateSelection()
  {
    var tileUnderMouse = TileHelper.GetTileUnderMouse();
    var creature = _tileMap.GetCreatureFromTile(tileUnderMouse);

    if (creature != null && !creature.IsDead)
    {
      _selectedCreature = creature;
      _unitData = _selectedCreature.Data;
      Root.SetActive(true);
    }
    else
    {
      _selectedCreature = null;
      _unitData = null;
      Root.SetActive(false);
    }

    UpdateView();
  }

  public void SetTileMap(TileMap tileMap)
  {
    _tileMap = tileMap;
  }

  public void SetUnit(UnitData unitData)
  {
    _unitData = unitData;
    Root.SetActive(true);
    UpdateView();
  }

  private void UpdateView()
  {
    if (_unitData == null)
    {
      return;
    }

    if (_selectedCreature == null)
    {

      Hp.text = string.Format("{0}/{1}", _unitData.Stats.MaxHealth, _unitData.Stats.MaxHealth);
      Armor.text = _unitData.Stats.Protection.Get(DamageType.Physical).ToString() + "x";
      Damage.text = _unitData.Stats.Damage.ToString("F1");
      Crit.text = (_unitData.Stats.CritChance).ToString("F1") + "x";
    }
    else
    {
      float tileMapProtection = _tileMap[_selectedCreature.Tile].TileData.ProtectionLevel;

      Hp.text = string.Format("{0}/{1} {2:F1}x", _selectedCreature.Health, _selectedCreature.Stats.MaxHealth, _selectedCreature.HealthPercent);
      Armor.text = _selectedCreature.Stats.Protection.Get(DamageType.Physical).ToString() + "x + " + tileMapProtection.ToString("F1") + "x";
      Damage.text = _selectedCreature.Stats.Damage.ToString("F1");
      Crit.text = _selectedCreature.Stats.CritChance.ToString("F1") + "x";
    }

    Sprite.sprite = ResourceManager.LoadSprite(_unitData.Sprite);
    Name.text = _unitData.Name;
  }
}
