﻿using UnityEngine;

namespace GameName1.Extensions
{
    public static class PointExtenstions
    {
        public static Point Add(this Point p, Point p2)
        {
            return new Point(p.X + p2.X, p.Y + p2.Y);
        }

        public static Point Add(this Point p, int x, int y)
        {
            return new Point(p.X + x, p.Y + y);
        }

        public static Point Minus(this Point p, Point p2)
        {
            return new Point(p.X - p2.X, p.Y - p2.Y);
        }

        public static Point Minus(this Point p, int x, int y)
        {
            return new Point(p.X - x, p.Y -y);
        }


        public static Vector2 ToVector(this Point p)
        {
            return new Vector2(p.X,p.Y);
        }
    }
}
