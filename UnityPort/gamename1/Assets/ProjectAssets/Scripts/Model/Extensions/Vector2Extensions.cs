﻿
using UnityEngine;

namespace GameName1.Extensions
{
    public static class Vector2Extensions
    {
        public static Point ToSize(this Vector2 v)
        {
            return new Point((int)v.x, (int)v.y);
        }

        public static Point ToSizeRound(this Vector2 v)
        {
            return new Point(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y));
        }
    }
}
