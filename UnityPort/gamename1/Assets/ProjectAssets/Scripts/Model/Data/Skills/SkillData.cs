﻿using GameName1.Lua;
using SLua;

namespace GameName1.Data
{
  [CustomLuaClass]
  public class SkillData
  {
    public string SkillId { get; set; }
    public SkillLuaWrapper SkillWrapper;
  }
}
