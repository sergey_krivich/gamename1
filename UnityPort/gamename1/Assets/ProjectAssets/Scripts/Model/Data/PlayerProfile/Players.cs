﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameName1.Data;

namespace GameName1.Travel
{
  public class Players
  {
    public event Action<PlayerProfile> Added;

    private readonly List<PlayerProfile> _playerProfiles = new List<PlayerProfile>();

    public List<PlayerProfile> GetAll()
    {
      return _playerProfiles.OrderBy(p => p.Id).ToList();
    }

    public void Add(PlayerProfile profile)
    {
      _playerProfiles.Add(profile);

      if (Added != null)
      {
        Added(profile);
      }
    }

    public PlayerProfile Get(long playerProfileId)
    {
      return _playerProfiles.Find(p => p.Id == playerProfileId);
    }

    public bool CreatureBelongsToLocalPlayer(UnitData unitData)
    {
      return AllLocal.Any(p => p.CreatureBelongsToPlayer(unitData));
    }

    public void Clear()
    {
      _playerProfiles.Clear();
    }

    public PlayerProfile Local
    {
      get { return _playerProfiles.Find(p => p.Local); }
    }


    public List<PlayerProfile> AllLocal
    {
      get { return _playerProfiles.Where(p => p.Local).ToList(); }
    }

    public int Count {
      get { return _playerProfiles.Count; }
    }
  }
}
