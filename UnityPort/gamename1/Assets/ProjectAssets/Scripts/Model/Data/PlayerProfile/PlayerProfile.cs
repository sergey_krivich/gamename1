﻿using System;
using System.Collections.Generic;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Items;

namespace GameName1.Travel
{
  public class PlayerProfile
  {
    public bool Local { get; set; }
    public string Nickname { get; private set; }

    private readonly long _id;
    private readonly List<UnitData> _creatures;

    public PlayerProfile(long id, string nickname, bool local)
    {
      Local = local;
      _id = id;
      Nickname = nickname;

      Inventory = new Inventory();
      _creatures = new List<UnitData>();
    }

    public void AddCreature(UnitData creature)
    {
      _creatures.Add(creature.CreateClone());
    }

    public void AddXp(int amount)
    {
      int amountEach = (int)Math.Round((float)amount / _creatures.Count, MidpointRounding.AwayFromZero);

      foreach (var creatureInfo in _creatures)
      {
        creatureInfo.AddXp(amountEach);
      }
    }

    public List<UnitData> Creatures
    {
      get { return _creatures; }
    }


    public Inventory Inventory { get; private set; }

    public long Id
    {
      get { return _id; }
    }

    public UnitData GetCreature(int id)
    {
      return Creatures.Find(c => c.Id == id);
    }

    public bool CreatureBelongsToPlayer(UnitData creatureData)
    {
      var creature = GetCreature(creatureData.Id);
      return creature != null;
    }
  }
}
