﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameName1.Data
{
    public static class Sprites
    {
        private static Dictionary<string, Dictionary<string, Sprite>> _sprites = new Dictionary<string, Dictionary<string, Sprite>>();

        public static void Init()
        {
            _sprites = new Dictionary<string, Dictionary<string, Sprite>>();
        }

        public static void LoadSpriteAtlas(string atlasName)
        {
          Sprite[] sprites = Resources.LoadAll<Sprite>("Graphics/AtlasesIgnore/" + atlasName);

            if (sprites.Length == 0)
            {
                Debug.LogError("COULD NOT LOAD ATLAS " + atlasName);
            }

            if (!_sprites.ContainsKey(atlasName))
            {
                _sprites[atlasName] = new Dictionary<string, Sprite>();
            }
            foreach (Sprite sprite in sprites)
            {
                _sprites[atlasName][sprite.name] = sprite;
            }
        }

        public static Sprite GetSprite(string atlas, string name)
        {
            if (!_sprites.ContainsKey(atlas))
            {
                Debug.LogError("Atlas not found: " + atlas);
                return null;
            }
            if (!_sprites[atlas].ContainsKey(name))
            {
                Debug.LogError("Sprite not found: " + name);
                return null;
            }

            return _sprites[atlas][name];
        }

        public static void UnloadEverything()
        {
            _sprites.Clear();

            Resources.UnloadUnusedAssets();
        }
    }
}
