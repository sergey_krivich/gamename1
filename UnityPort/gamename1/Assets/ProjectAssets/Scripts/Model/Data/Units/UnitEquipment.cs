﻿using System;
using GameName1.Actors;
using GameName1.Items;

namespace GameName1.Data
{
  public class UnitEquipment
  {
    public Item Weapon;
    public Item Armor;

    public Item RemoveWeapon()
    {
      var weapon = Weapon;
      Weapon = null;
      return weapon;
    }

    public Item RemoveArmor()
    {
      var armor = Armor;
      Armor = null;
      return armor;
    }

    public void EquipWeapon(Item weapon)
    {
      Weapon = weapon;
    }

    public void EquipArmor(Item armor)
    {
      Armor = armor;
    }

    public void ApplyBonuses(CreatureStats stats)
    {
      AddItemBonusStats(stats, Weapon);
      AddItemBonusStats(stats, Armor);
    }

    public UnitEquipment Clone()
    {
      return MemberwiseClone() as UnitEquipment;//todo:
    }

    private void AddItemBonusStats(CreatureStats stats, Item item)
    {
      if (item != null)
      {
        if (item.Type.BonusStatsAdd != null)
        {
          stats += item.Type.BonusStatsAdd;
        }
      }
    }

    public Item EquipItem(Item item)
    {
      if (item.Type.Group == ItemGroup.Weapon)
      {
        var oldWeapon = Weapon;
        EquipWeapon(item);
        return oldWeapon;
      }
      if (item.Type.Group == ItemGroup.Armor)
      {
        var oldArmor = Armor;
        EquipArmor(item);
        return oldArmor;
      }

      throw new ArgumentOutOfRangeException();
    }
  }
}
