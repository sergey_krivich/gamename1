﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Fight;
using GameName1.Lua;
using SLua;

namespace GameName1.Data
{
  [CustomLuaClass]
  public class SkillSlot
  {
    public int SlotLevel;

    public SkillSlot Clone()
    {
      return MemberwiseClone() as SkillSlot;
    }
  }

  [CustomLuaClass]
  public class SpawnData
  {
    public float SpawnRange;
    public float SpawnInDesiredDirectonCoef = 1f;
    public int SpawnCost = 1;
    public SpawnData CreateClone()
    {
      var clone = MemberwiseClone() as SpawnData;
      return clone;
    }
  }

    [CustomLuaClass]
  public enum UnitType
  {
    Warrior,
    Archer,
    Mage
  }

  /// <summary>
  /// Status effect which will be applied on battle start for battle duration
  /// </summary>
  public class TemporaryEffect
  {
    public string EffectName;//StatusEffect to use on battle start
    public int DurationLeft;//How many events must be passed before it wears off

    public DictLua Param;

    public TemporaryEffect Clone()
    {
      var clone = MemberwiseClone() as TemporaryEffect;

      clone.Param = clone.Param.Clone();

      return clone;
    }
  }

  [CustomLuaClass]
  public class UnitData
  {
    private static int _unitDataId;

    public int Id { get; set; }

    public string Name { get; set; }
    public string Description { get; set; }

    //Creature may be displayed by static sprite or animator
    public string Sprite { get; set; }
    public string Animator { get; set; }

    public CreatureStats Stats { get; set; }

    public string DefaultAttackSkillId { get; set; }

    public int DeathXp { get; set; }
    public int SpawnPointIndex { get; set; }

    public float CurrentHp { get; set; }
    public int CurrentXp { get; set; }
    public int Level { get; set; }
    public FightSide Side { get; set; }
    public UnitType Type { get; set; }

    public string AiType { get; set; }

    public string DeathFx { get; set; }
    public string HitFx { get; set; }

    //used for summoning spells
    public FightPlayer PlayerCreator { get; set; }

    public SpawnData SpawnData { get; set; }

    public List<string> SkillIds { get; set; }
    public List<SkillSlot> SkillSlots { get; set; }

    public UnitEquipment Equipment = new UnitEquipment();

    public UnitData()
    {
      SkillIds = new List<string>();
      SkillSlots = new List<SkillSlot>();
      SpawnData = new SpawnData();
      Stats = new CreatureStats();
      Id = _unitDataId++;
    }

    public void AddSkill(string skillName)
    {
      SkillIds.Add(skillName);
    }

    public UnitData CreateClone()
    {
      var clone = MemberwiseClone() as UnitData;

      clone.SpawnData = SpawnData.CreateClone();
      clone.SkillIds = SkillIds.ToList();
      clone.Equipment = Equipment.Clone();
      clone.SkillSlots = SkillSlots.Select(s => s.Clone()).ToList();
      clone.Id = 1 + _unitDataId;
      _unitDataId++;

      return clone;
    }


    public void AddXp(int amount)
    {
      CurrentXp += amount;

      while (CurrentXp > XpToNextLevel())
      {
        CurrentXp -= XpToNextLevel();

        Level += 1;

        AddSkillSlot();
        IncreaseMaxHealth();
      }
    }

    private void AddSkillSlot()
    {
      switch (Level)
      {
        case 1:
        case 2:
        case 5:
        case 10:
        case 20:
          SkillSlots.Add(new SkillSlot
          {
            SlotLevel = Level
          });
          break;
      }
    }

    private void IncreaseMaxHealth()
    {
      int maxHealthToAdd = 0;
      switch (Type)
      {
        case UnitType.Warrior:
          maxHealthToAdd = 3;
          break;
        case UnitType.Archer:
          maxHealthToAdd = 2;
          break;
        case UnitType.Mage:
          maxHealthToAdd = 1 + Level % 2;
          break;
        default:
          throw new ArgumentOutOfRangeException();
      }

      Stats.MaxHealth += maxHealthToAdd;
    }

    public int XpToNextLevel()
    {
      return 10 + (Level + 1) * 3 * (Level + 1);
    }

    public void AddHpPercent(float percent)
    {
      CurrentHp += Stats.MaxHealth * percent;
      if (CurrentHp > Stats.MaxHealth)
      {
        CurrentHp = Stats.MaxHealth;
      }
    }
  }
}
