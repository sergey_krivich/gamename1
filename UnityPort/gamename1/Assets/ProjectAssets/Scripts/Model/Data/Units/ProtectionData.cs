﻿using System.Collections.Generic;
using GameName1.Actors;
using SLua;

namespace GameName1.Data
{
  [CustomLuaClass]
  public class ProtectionData
  {
    private readonly Dictionary<DamageType, float> _damageProtection;

    public ProtectionData()
    {
      _damageProtection = new Dictionary<DamageType, float>();
    }

    public void Add(DamageType type, float amount)
    {
      _damageProtection[type] = Get(type) + amount;
    }

    public void Mul(DamageType type, float amount)
    {
      _damageProtection[type] = Get(type) * amount;
    }

    public void Set(DamageType type, float amount)
    {
      _damageProtection[type] = amount;
    }

    public float Get(DamageType type)
    {
      if (!_damageProtection.ContainsKey(type))
        return 0.0f;
      
      return _damageProtection[type];
    }

    public void Merge(ProtectionData data)
    {
      foreach (var kvp in data._damageProtection)
      {
        Add(kvp.Key, kvp.Value);
      }
    }
  }
}
