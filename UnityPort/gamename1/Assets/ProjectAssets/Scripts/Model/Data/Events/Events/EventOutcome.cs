﻿using System.Collections.Generic;
using GameName1.Fight;
using GameName1.Items;
using SLua;

namespace GameName1.Data
{
  [CustomLuaClass]
  public class EventOutcomeReward
  {
    public int Gold;
    public int Exp;
    public List<Item> Items = new List<Item>();
    public List<UnitData> Units = new List<UnitData>();

    public void AddUnit(UnitData unit)
    {
      Units.Add(unit);
    }

    public void AddItem(ItemType itemType, int count)
    {
      if(count <= 0)
        return;

      var item = new Item
      {
        Count = count,
        Id = itemType.Id,
        Type = itemType
      };

      Items.Add(item);
    }
  }

  public class OutcomeCondition 
  {
    private readonly LuaTable _table;

    public OutcomeCondition(LuaTable table)
    {
      _table = table;
    }

    public bool Check(OutcomeCheckData checkData)
    {
      var result = (bool)_table.invoke("Check", checkData);
      return result;
    }
  }

  [CustomLuaClass]
  public class OutcomeCheckData
  {
    public List<FightPlayer> OtherFightPlayers;
    public List<FightPlayer> HumanFightPlayers;

    public FightPlayer OtherFightPlayerWithSide(FightSide side)
    {
      return OtherFightPlayers.Find(f => f.Side == side);
    }
  }

  [CustomLuaClass]
  public class EventOutcome
  {
    public string EventId { get; set; }
    public string Description { get; set; }
    public float Chance { get; set; }

    public bool IsFight { get { return Fight != null; } }
    public EventOutcomeReward Reward { get; set; }
    public FightInfo Fight;

    public List<OutcomeCondition> Conditions = new List<OutcomeCondition>();

    public EventOutcome()
    {
      Reward = new EventOutcomeReward();
    }

    public void SetFight()
    {
      Fight = new FightInfo();
    }

    public bool CheckConditions(OutcomeCheckData checkData)
    {
      foreach (var outcomeCondition in Conditions)
      {
        if (!outcomeCondition.Check(checkData))
        {
          return false;
        }
      }

      return true;
    }

    public void AddCondition(LuaTable condition)
    {
      Conditions.Add(new OutcomeCondition(condition));
    }
  }
}
