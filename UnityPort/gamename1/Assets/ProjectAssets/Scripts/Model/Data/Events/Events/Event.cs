﻿using System.Collections.Generic;
using SLua;

namespace GameName1.Data
{
  [CustomLuaClass]
  public class Event
  {
    public string Id { get; set; }
    public string Description { get; set; }
    public List<EventChoice> Choices { get; set; }
  }
}
