﻿using System.Collections.Generic;
using SLua;

namespace GameName1.Data
{
  [CustomLuaClass]
  public class EventChoice
  {
    public string Description { get; set; }
    public List<EventOutcome> Outcomes { get; set; }

    public EventChoice()
    {
      Outcomes = new List<EventOutcome>();
    }

    public void AddOutcome(EventOutcome outcome)
    {
      Outcomes.Add(outcome);
    }
  }
}
