﻿using GameName1.Actors;
using GameName1.Lua;

namespace GameName1.StatusEffects
{
  public class StatusEffect
  {
    private readonly StatusEffectLuaWrapper _wrapper;
    private readonly bool _infinite;

    public StatusEffect(StatusEffectLuaWrapper wrapper, bool infinite)
    {
      _wrapper = wrapper;
      _infinite = infinite;
    }

    public void OnStart()
    {
      _wrapper.OnStart();
    }

    public void OnStepStarted()
    {
      _wrapper.OnStepStart();

      Elapsed += 1;

      if (EffectEnded)
      {
        _wrapper.OnEnd();
      }
    }

    public void OnTakeDamage(DamageInfo damageInfo)
    {
      if (_wrapper != null)
      {
        if (!EffectEnded)
        {
          _wrapper.OnTakeDamage(damageInfo);
        }
      }
    }

    public int Elapsed { get; private set; }

    public int Duration
    {
      get { return _wrapper.GetDuration(); }
    }

    public bool EffectEnded
    {
      get { return Elapsed >= Duration && !_infinite; }
    }

    public void ApplyStatsChange(CreatureStats stats)
    {
      _wrapper.ApplyStatsChange(stats);

    }
  }
}
