﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Lua;
using GameName1.Tiles;
using SLua;
using UnityEngine;

namespace GameName1.StatusEffects
{
  public class StatusEffectsManager
  {
    private readonly CreatureActor _creatureActor;
    private readonly List<StatusEffect> _effects;
    private StatusEffectLuaLoader _luaLoader;
    private TileMap _tileMap;

    public StatusEffectsManager(CreatureActor creatureActor)
    {
      _creatureActor = creatureActor;
      _effects = new List<StatusEffect>();
    }

    public void OnStepStarted()
    {
      foreach (var effect in _effects.ToList())
      {
        effect.OnStepStarted();
      }

      _effects.RemoveAll(p => p.EffectEnded);
    }

    public void OnTakeDamage(DamageInfo damageInfo)
    {
      foreach (var effect in _effects.ToList())
      {
        effect.OnTakeDamage(damageInfo);
      }
    }

    public void Add(string name, CreatureActor effectCreator, LuaTable data, bool infinite = false)
    {
      var effectLuaWrapper = _luaLoader.GetStatusEffect(name, _creatureActor, effectCreator, _tileMap);
      effectLuaWrapper.Init(data);

      var statusEffect = new StatusEffect(effectLuaWrapper, infinite);
      Add(statusEffect);
    }

    public void Add(StatusEffect statusEffect)
    {
      statusEffect.OnStart();
      _effects.Add(statusEffect);
    }

    public void InitLoader(StatusEffectLuaLoader statusEffectLuaLoader, TileMap tileMap)
    {
      _luaLoader = statusEffectLuaLoader;
      _tileMap = tileMap;
    }

    public void ApplyStatsChange(CreatureStats stats)
    {
      foreach (var statusEffect in _effects)
      {
        statusEffect.ApplyStatsChange(stats);
      }
    }
  }
}
