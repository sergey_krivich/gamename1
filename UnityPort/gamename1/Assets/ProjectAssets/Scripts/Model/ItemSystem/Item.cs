﻿namespace GameName1.Items
{
  public class Item
  {
    public ItemType Type;
    public int Count;
    public string Id;
  }
}
