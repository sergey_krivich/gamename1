﻿using System.Collections.Generic;
using System.Linq;

namespace GameName1.Items
{
  public class Inventory
  {
    public List<Item> Items
    {
      get { return _items.ToList(); }
    }

    public int Gold { get; private set; }

    private readonly List<Item> _items;

    public Inventory()
    {
      _items = new List<Item>();
    }

    public void AddGold(int amount)
    {
      Gold += amount;
    }

    public bool UseGold(int amount)
    {
      if (!HasEnoughGold(amount))
      {
        return false;
      }

      Gold -= amount;

      return true;
    }

    public bool HasEnoughGold(int price)
    {
      return Gold >= price;
    }

    public void AddItem(Item item)
    {
      if (item == null)
      {
        return;
      }

      var existingItem = GetItemById(item.Id);
      if (existingItem != null)
      {
        existingItem.Count += item.Count;
      }
      else
      {
        _items.Add(item);
      }
    }

    public void RemoveItem(string id, int count)
    {
      var existingItem = GetItemById(id);

      if (existingItem != null)
      {
        existingItem.Count -= count;
      }

      if (existingItem.Count <= 0)
      {
        _items.Remove(existingItem);
      }
    }

    public bool HasItem(string id)
    {
      var item = GetItemById(id);
      return item != null && item.Count > 0;
    }

    public Item GetItemById(string id)
    {
      return _items.Find(i => i.Id == id);
    }

    public Item GetItemByGroup(ItemGroup group)
    {
      return _items.Find(i => i.Type.Group == group);
    }

    public int GetItemCountByGroup(ItemGroup group)
    {
      var item = _items.Find(i => i.Type.Group == group);
      if (item == null)
        return 0;
      return item.Count;
    }
  }
}
