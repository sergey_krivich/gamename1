﻿using System.Collections.Generic;
using GameName1.Actors;
using GameName1.Data;
using SLua;

namespace GameName1.Items
{
  [CustomLuaClass]
  public enum ItemGroup
  {
    HpPotion,
    Currency,
    ResurectionStone,
    Weapon,
    Armor,
    Artifact
  }

  [CustomLuaClass]
  public class ItemType
  {
    public ItemGroup Group;
    public List<UnitType> UnitTypes = new List<UnitType>();
    public string Id;
    public string Icon;
    public string Description;
    public string Name;

    public CreatureStats BonusStatsAdd;

    public void AddAvailableUnitType(UnitType type)
    {
      UnitTypes.Add(type);
    }

    public bool IsAvailable(UnitType type)
    {
      return UnitTypes.Count == 0 || UnitTypes.Contains(type);
    }
  }
}
