﻿using GameName1.Actors;
using GameName1.Fight;
using GameName1.Tiles;
using SLua;

namespace GameName1.Lua
{
  public class AILuaBehaviourWrapper:LuaWrapperBase
  {
    public CreatureActor Actor { get; private set; }
    public FightManager Manager { get; private set; }
    public FightPlayer Player { get; private set; }
    public TileMap TileMap{ get; private set; }

    public AILuaBehaviourWrapper(string id, FightManager fightManager, FightPlayer fightPlayer, CreatureActor actor, TileMap tileMap) 
      : base(id)
    {
      Manager = fightManager;
      Player = fightPlayer;
      Actor = actor;
      TileMap = tileMap;
    }

    public void Update()
    {
      Object.invoke("Update", Object);
    }
  }

  public class AILuaLoader:LuaLoader<AILuaBehaviourWrapper>
  {
    public AILuaLoader() 
      : base("aiTable")
    {
      
    }

    public AILuaBehaviourWrapper GetBehaviour(string name, FightManager fightManager, FightPlayer fightPlayer, CreatureActor actor,TileMap tileMap)
    {
      var behaviour = new AILuaBehaviourWrapper(name, fightManager, fightPlayer, actor, tileMap);
      ReloadWrapper(behaviour);
      return behaviour;
    }

    protected override LuaTable CreateLuaObject(AILuaBehaviourWrapper wrapper)
    {
      return Table.invoke("Create", wrapper.Id, wrapper.Manager, wrapper.Player, wrapper.Actor, wrapper.TileMap) as LuaTable;
    }
  }
}
