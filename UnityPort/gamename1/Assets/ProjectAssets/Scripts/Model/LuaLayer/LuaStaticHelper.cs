﻿using GameName1;
using GameName1.Items;
using SLua;

[CustomLuaClass]
public static class LuaStaticHelper
{
  [StaticExport]
  public static GameStateData GetGameState()
  {
    return AppRoot.I.GameStateData;
  }

  [StaticExport]
  public static void AddInventoryItem(ItemType type)
  {
    foreach (var playerProfile in AppRoot.I.Players.GetAll())
    {
      playerProfile.Inventory.AddItem(new Item
      {
        Id = type.Id,
        Count = 1,
        Type = type
      });
    }
  }
}
