﻿using System;
using GameName1.Actors;
using Glide;
using SLua;
using UnityEngine;

[CustomLuaClass]
public static class LuaTweener
{
  [StaticExport]
  public static void Position(CreatureActor actor, Vector2 targetPosition, EaseType type, float time, Action onCompleted)
  {
    Tweener.I.Tween(actor, new {X = targetPosition.x, Y = targetPosition.y}, time)
      .Ease(Ease.FromType(type))
      .OnComplete(() => { onCompleted(); });
  }
}
