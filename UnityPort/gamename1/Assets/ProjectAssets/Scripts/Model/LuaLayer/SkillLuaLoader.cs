﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Tiles;
using SLua;
using UnityEngine;

namespace GameName1.Lua
{
  public class SkillLuaWrapper: LuaWrapperBase
  {
    public TileMap TileMap { get; private set; }
    public CreatureActor CreatureActor { get; private set; }

    public int CooldownLeft
    {
      get
      { 
        var value =  (double)Object["CooldownLeft"]; 
        return (int)value;
      }
    }

    public SkillLuaWrapper(string id, TileMap tileMap, CreatureActor actor)
      :base(id)
    {
      TileMap = tileMap;
      CreatureActor = actor;
    }

    public bool Apply(LuaTable param)
    {
      var result = Object.invoke("Apply", Object, param);
      if(result == null) return true;
      return (bool)result;
    }

    public float GetStaminaUsage()
    {
      return AppRoot.I.LuaLoader.SkillsMeta.GetStaminaCost(Id);
    }

    public float GetRange()
    {
      return AppRoot.I.LuaLoader.SkillsMeta.GetRange(Id);
    }

    public void OnDone()
    {
      Object.invoke("OnDone", Object);
    }

    public bool IsDone()
    {
      return (bool)Object.invoke("IsDone", Object);
    }

    public void Update()
    {
      Object.invoke("Update", Object);
    }

    public void OnTurnStart()
    {
      Object.invoke("OnTurnStart", Object);
    }

    public bool IsOnCooldown()
    {
      return (bool)Object.invoke("IsOnCooldown", Object);
    }

    public bool PlayDefaultAnimation()
    {
      var value = Object["PlayDefaultAnimation"];
      if (value == null)
        return true;

      return (bool)value;
    }

    public bool PlayDefaultAnimationOutOfRange()
    {
      var value = Object["PlayDefaultAnimationOutOfRange"];
      if (value == null)
        return false;

      return (bool)value;
    }


    public bool CanUse(LuaTable param)
    {
      return (bool)Object.invoke("CanUse", Object, param);
    }

    public bool IsInRange(Point target)
    {
      return (bool)Object.invoke("IsInRange", Object, target);
    }

    public List<Point> GetTargetTiles(LuaTable param)
    {
      var table = Object.invoke("GetTargetTiles", Object, param) as LuaTable;

      if (table == null) return new List<Point>();

      return table.Select(p => (Point)p.value).ToList();
    }
  }

  public class SkillLuaLoader: LuaLoader<SkillLuaWrapper>
  {
    public SkillLuaLoader() 
      : base("skillsTable")
    {
    }

    public virtual SkillLuaWrapper GetSkill(string name, TileMap tileMap, CreatureActor actor)
    {
      var skillWrapper = new SkillLuaWrapper(name, tileMap, actor);
      ReloadWrapper(skillWrapper);
      return skillWrapper;
    }

    protected override LuaTable CreateLuaObject(SkillLuaWrapper wrapper)
    {
      return Table.invoke("Create", wrapper.Id, wrapper.TileMap, wrapper.CreatureActor) as LuaTable;
    }

    public List<string> SelectAvailable(UnitType type, int level)
    {
      var table = Table.invoke("SelectAvailable", type, level) as LuaTable;
      if (table == null)
      {
        Debug.LogError("SelectAvailable null");
        return new List<string>();
      }

      return table.Select(v => v.value as string).ToList();
    }
  }
}
