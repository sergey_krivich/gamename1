﻿using GameName1.Actors;
using GameName1.Tiles;
using SLua;

namespace GameName1.Lua
{
  public class StatusEffectLuaWrapper : LuaWrapperBase
  {
    public CreatureActor Creator { get; private set; }
    public CreatureActor Actor { get; private set; }
    public TileMap TileMap { get; private set; }

    public StatusEffectLuaWrapper(string id, CreatureActor actor, CreatureActor creator, TileMap tileMap)
      : base(id)
    {
      Creator = creator;
      Actor = actor;
      TileMap = tileMap;
    }

    public void Init(LuaTable data)
    {
      Object.invoke("Init", Object, data);
    }

    public void OnStepStart()
    {
      Object.invoke("OnStepStart", Object);
    }

    public void OnStart()
    {
      Object.invoke("OnStart", Object);
    }

    public void OnEnd()
    {
      Object.invoke("OnEnd", Object);
    }

    public void OnTakeDamage(DamageInfo damageInfo)
    {
      Object.invoke("OnTakeDamage", Object, damageInfo);
    }

    public int GetDuration()
    {
      return (int)(double)Object.invoke("GetDuration", Object);
    }

    public void ApplyStatsChange(CreatureStats stats)
    {
      Object.invoke("ApplyStatsChange", Object, stats);
    }
  }

  public class StatusEffectLuaLoader : LuaLoader<StatusEffectLuaWrapper>
  {
    public StatusEffectLuaLoader()
      : base("statusEffectTable")
    {

    }

    public StatusEffectLuaWrapper GetStatusEffect(string name, CreatureActor actor, CreatureActor creator, TileMap tileMap)
    {
      var behaviour = new StatusEffectLuaWrapper(name, actor, creator, tileMap);
      ReloadWrapper(behaviour);
      return behaviour;
    }

    protected override LuaTable CreateLuaObject(StatusEffectLuaWrapper wrapper)
    {
      return Table.invoke("Create", wrapper.Id, wrapper.Actor, wrapper.Creator, wrapper.TileMap) as LuaTable;
    }
  }
}
