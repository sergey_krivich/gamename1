﻿using SLua;

namespace GameName1.Lua
{
  public class ConfigLuaWrapper
  {
    private LuaTable _config;

    public void Reload(LuaState luaState)
    {
      _config = luaState.getTable("Config");
    }

    public void OnLaunched()
    {
      _config.invoke("OnLaunched");
    }

    public bool FastStart
    {
      get { return (bool) _config["FastStart"]; }
    }

    public bool FastEvents
    {
      get { return (bool) _config["FastEvents"]; }
    }

    public string LaunchEvent
    {
      get { return (string) _config["LaunchEvent"]; }
    }

    public bool FastPreEvent
    {
      get { return (bool) _config["FastPreEvent"]; }
    }

    public bool FastSkillLearn
    {
      get { return (bool)_config["FastSkillLearn"]; }
    }

    public bool AutoGame
    {
      get { return (bool)_config["AutoGame"]; }
    }

    public float XpMult
    {
      get { return (float)(double)_config["XpMult"]; }
    }

    public float DifficultyMult
    {
      get { return (float)(double)_config["DifficultyMult"]; }
    }
  }
}
