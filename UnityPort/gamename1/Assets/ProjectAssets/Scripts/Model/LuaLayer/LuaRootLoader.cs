﻿using System;
using System.Collections.Generic;
using SLua;

namespace GameName1.Lua
{
  public class LuaRootLoader
  {
    private readonly bool _createGo;
    public event Action LoadFinished;
    public AILuaLoader AiLoader { get; private set; }
    public SkillLuaLoader SkillLoader { get; private set; }
    public StatusEffectLuaLoader StatusEffectsLoader { get; private set; }
    public LevelLuaLoader LevelLoader { get; private set; }
    public EventsLuaLoader EventsLoader { get; private set; }
    public ConfigLuaWrapper ConfigLoader { get; private set; }
    public SkillsLuaMeta SkillsMeta { get; private set; }
    public LuaState State{
      get { return LuaSvr.mainState; }
    }

    private readonly List<ILuaLoader> _loaders;

    private LuaSvr _svr;

    public LuaRootLoader(bool createGo = true)
    {
      _createGo = createGo;
      _loaders = new List<ILuaLoader>();
    }

    public void Init()
    {
      _svr = new LuaSvr();
      _svr.init(null, () =>
      {
        _svr.start("main");

        AiLoader = new AILuaLoader();
        _loaders.Add(AiLoader);

        StatusEffectsLoader = new StatusEffectLuaLoader();
        _loaders.Add(StatusEffectsLoader);

        SkillLoader = new SkillLuaLoader();
        _loaders.Add(SkillLoader);

        LevelLoader = new LevelLuaLoader();
        _loaders.Add(LevelLoader);

        EventsLoader = new EventsLuaLoader();
        _loaders.Add(EventsLoader);

        SkillsMeta = new SkillsLuaMeta();
        SkillsMeta.Reload(State);

        ConfigLoader = new ConfigLuaWrapper();
        ConfigLoader.Reload(State);

        foreach (var luaLoader in _loaders)
        {
          LoadLoader(luaLoader);
        }

        if (LoadFinished !=null)
          LoadFinished();
      });
    }

    public void Reload()
    {
      if(_svr != null)
        State.Dispose();

      _svr = new LuaSvr();
      _svr.init(null, () =>
      {
        _svr.start("main");
        ConfigLoader.Reload(State);

        foreach (var luaLoader in _loaders)
        {
          LoadLoader(luaLoader);
        }

        SkillsMeta.Reload(State);
      });
    }

    public void LoadLoader(ILuaLoader loader)
    {
      loader.Load(State);
    }

    public void CleanCache()
    {
      foreach (var luaLoader in _loaders)
      {
        luaLoader.Clean();
      }
    }
  }
}
