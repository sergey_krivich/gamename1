﻿using System.Collections.Generic;
using System.Linq;
using SLua;

namespace GameName1.Lua
{
  public class DictLua: Dictionary<string, object>
  {
    public LuaTable ToLua(LuaState state)
    {
      var  table = new LuaTable(state);

      foreach (var kvp in this)
      {
        table[kvp.Key] = kvp.Value;
      }

      return table;
    }


    public DictLua Clone()
    {
      var dict = new DictLua();

      foreach (var kvp in this)
      {
        dict[kvp.Key] = kvp.Value;
      }

      return dict;
    }
  }
}
