﻿using SLua;
using UnityEngine;

namespace GameName1.Lua
{
  public class SkillsLuaMeta
  {
    private LuaTable _table;

    public void Reload(LuaState state)
    {
      _table = state["skillsMetaTable"] as LuaTable;
    }

    public float GetStaminaCost(string skillId)
    {
      var data = GetSkillData(skillId);
      var cost = data["EnergyCost"];

      if (cost == null)
        return 0f;

      return (float)(double)cost;
    }

    public float GetCooldown(string skillId)
    {
      var data = GetSkillData(skillId);
      var cost = data["Cooldown"];

      if (cost == null)
        return 0f;

      return (float)(double)cost;
    }

    public string GetSpriteName(string skillId)
    {
      var data = GetSkillData(skillId);
      return data["SpriteName"] as string;
    }

    public float GetRange(string skillId)
    {
      var data = GetSkillData(skillId);
      return (float)(double)data["Range"];
    }

    public string GetDescription(string skillId)
    {
      var data = GetSkillData(skillId);
      return (string)data["Description"];
    }

    private LuaTable GetSkillData(string skillId)
    {
      var skillData = _table[skillId] as LuaTable;

      if (skillData == null)
      {
        Debug.LogError("Meta data not found: " + skillId);
      }

      return skillData;
    }

    public string GetName(string skillId)
    {
      //todo:
      return skillId;
    }
  }
}
