﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Data;
using SLua;
using UnityEngine;
using Event = GameName1.Data.Event;

namespace GameName1.Lua
{
  public class EventLuaWrapper : LuaWrapperBase
  {
    public EventLuaWrapper(string id) 
      : base(id)
    {
    }

    public Event GetEvent()
    {
      return new Event()
      {
        Choices = GetChoices(),
        Description = GetDescription(),
        Id = Id
      };
    }

    private List<EventChoice> GetChoices()
    {
      var table = Object.invoke("GetChoices", Object) as LuaTable;

      if (table == null)
      {
        Debug.LogError("GetChoices null");
        return new List<EventChoice>();
      }

      return table.Select(v => v.value as EventChoice).ToList();
    }

    private string GetDescription()
    {
      var description = Object["Description"] as string;
      if (description == null)
      {
        Debug.LogError("description null");
        return "ERROR_NOT_LOADED";
      }

      return description;
    }
  }

  public class EventsLuaLoader:LuaLoader<EventLuaWrapper>
  {
    public EventsLuaLoader() 
      : base("eventsTable")
    {
    }

    public Event GetEvent(string name)
    {
      var wrapper = new EventLuaWrapper(name);
      ReloadWrapper(wrapper);
      return wrapper.GetEvent();
    }

    protected override LuaTable CreateLuaObject(EventLuaWrapper wrapper)
    {
      return Table.invoke("Create", wrapper.Id) as LuaTable;
    }

    public List<string> SelectAvailable(float difficulty)
    {
      var table = Table.invoke("SelectAvailable", difficulty) as LuaTable;
      if (table == null)
      {
        Debug.LogError("SelectAvailable null");
        return new List<string>();
      }

      return table.Select(v => v.value as string).ToList();
    }
  }
}
