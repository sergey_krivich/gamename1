﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Fight;
using GameName1.Levels;
using GameName1.Tiles;
using SLua;

namespace GameName1.Lua
{
  public class LevelLuaWrapper:LuaWrapperBase
  {
    public LevelLuaWrapper(string id) 
      : base(id)
    {
    }

    public void Init(LuaTable levelParams)
    {
      Object.invoke("Init", Object, levelParams);
    }

    public void OnStepStart()
    {
      Object.invoke("OnStepStart", Object);
    }

    public Level GenLevel()
    {
      var table = Object.invoke("GenLevel", Object);
      return table as Level;
    }

    public List<Actor> GenFx(Level level)
    {
      var table = Object.invoke("GenFx", Object, level) as LuaTable;

      var result = new List<Actor>();
      if (table == null)
      {
        return result;
      }

      foreach (var kvp in table)
      {
        result.Add((Actor)kvp.value);
      }

      return result;
    }
  }

  public class LevelLuaLoader:LuaLoader<LevelLuaWrapper>
  {
    public LevelLuaLoader() 
      : base("levelTable")
    {
      
    }

    public LevelLuaWrapper GetLevel(string name)
    {
      var level = new LevelLuaWrapper(name);
      ReloadWrapper(level);
      return level;
    }

    protected override LuaTable CreateLuaObject(LevelLuaWrapper wrapper)
    {
      return Table.invoke("Create", wrapper.Id) as LuaTable;
    }
  }
}
