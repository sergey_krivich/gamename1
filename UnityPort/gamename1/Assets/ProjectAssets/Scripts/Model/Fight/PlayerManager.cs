﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using SLua;

namespace GameName1.Fight
{
  [CustomLuaClass]
  public class PlayerManager
  {
    public List<FightPlayer> Players { get; private set; }

    private FightPlayer _currentFightPlayer;
    private int _currentPlayerIndex;


    public PlayerManager()
    {
      Players = new List<FightPlayer>();
    }

    public void SetPlayers(List<FightPlayer> players)
    {
      Players = players;
      _currentFightPlayer = Players.FirstOrDefault();
    }

    public void AddPlayer(FightPlayer fightPlayer)
    {
      Players.Add(fightPlayer);
      _currentFightPlayer = fightPlayer;
    }

    public void Start()
    {
      if (_currentFightPlayer != null)
      {
        StartPlayerStep();
      }
    }

    private void StartPlayerStep()
    {
      _currentFightPlayer.StartStep();
      PlayerStepStarted();
    }

    public virtual void Update()
    {
      if (_currentFightPlayer != null)
      {
        _currentFightPlayer.Update();
        if (_currentFightPlayer.StepEnded)
        {
          EndStep();
        }
      }
    }

    public void EndStep()
    {
      _currentPlayerIndex++;
      if (_currentPlayerIndex >= Players.Count)
      {
        _currentPlayerIndex = 0;
      }
      if (_currentPlayerIndex >= Players.Count)
      {
        return;
      }

      _currentFightPlayer = Players[_currentPlayerIndex];
      if (!_currentFightPlayer.HasCreatures)
      {
        _currentFightPlayer = null;
        Players.Remove(_currentFightPlayer);
        EndStep();
        return;
      }

      if (_currentFightPlayer != null)
      {
        StartPlayerStep();
        if (!_currentFightPlayer.SkipFrameOnStepStart)
        {
          Update();
        }
      }
    }


    protected virtual void PlayerStepStarted()
    {

    }

    public FightPlayer CurrentFightPlayer
    {
      get { return _currentFightPlayer; }
    }

    public CreatureActor CurrentCreatureActor
    {
      get { return _currentFightPlayer.SelectedCreatureActor; }
    }
  }
}
