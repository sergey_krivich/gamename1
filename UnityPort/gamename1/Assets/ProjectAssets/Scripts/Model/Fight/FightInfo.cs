﻿using System.Collections.Generic;
using GameName1.Data;
using SLua;

[CustomLuaClass]
public enum FightSide
{
  Player,
  Enemy
}

namespace GameName1.Fight
{


  [CustomLuaClass]
  public class FightInfo
  {
    public List<UnitData> Creatures { get; set; }
    public List<EventOutcome> Outcomes { get; set; }
    public LuaTable LevelParams{ get; set; }
    public string LevelName { get; set; }

    public FightInfo()
    {
      Creatures = new List<UnitData>();
      Outcomes = new List<EventOutcome>();
    }

    public void AddCreatureSide(UnitData creatureInfo, FightSide side = FightSide.Enemy, int spawnPointIndex = 1)
    {
      var clone = creatureInfo.CreateClone();
      clone.Side = side;
      clone.SpawnPointIndex = spawnPointIndex;
      Creatures.Add(clone);
    }

    public void AddOutcome(EventOutcome outcome)
    {
      Outcomes.Add(outcome);
    }
  }
}
 