﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using GameName1.Actors;
using SLua;

namespace GameName1.Fight
{
  [CustomLuaClass]
  public abstract class FightPlayer : ISaveable
  {
    public bool SaveCreatures = true;
    public bool ActivateLight = false;
    public FightSide Side;

    public event Action<CreatureActor> SelectedCreatureChanged;
    public Action StepStarted;

    public bool IsLocal { get; private set; }

    public List<CreatureActor> Creatures = new List<CreatureActor>();
    private int _currentCreature;

    public FightPlayer()
    {
      _currentCreature = 0;
    }

    public void MarkLocal()
    {
      IsLocal = true;
    }

    public void SetCreatures(List<CreatureActor> creatures)
    {
      foreach (CreatureActor creature in creatures)
      {
        AddCreature(creature);
      }
    }

    protected virtual void OnCreatureAdd(CreatureActor actor)
    {
    }


    public void AddCreature(CreatureActor actor)
    {
      Creatures.Add(actor);
      actor.SetPlayer(this);
      OnCreatureAdd(actor);
    }

    public void ActivateCreatures(bool active)
    {
      foreach (var creature in Creatures)
      {
        creature.Active = active;
      }
    }

    public virtual void StartStep()
    {
      StepEnded = false;
      foreach (var aliveCreature in AliveCreatures)
      {
        aliveCreature.OnStepStarted();
      }

      if (SelectedCreatureActor == null || SelectedCreatureActor.IsDead)
      {
        if (SelectedCreatureActor != null)
        {
          RemoveSelectedCreature();
        }
        if (Creatures.Count == 0)
        {
          StepEnded = true;
          return;
        }
        GetNextCreature();
      }
      else
      {
        CallSelectedCreatureChanged(SelectedCreatureActor);
      }
    }

    protected virtual void CallSelectedCreatureChanged(CreatureActor creature)
    {
      if(SelectedCreatureChanged != null)
        SelectedCreatureChanged(creature);
    }

    protected virtual void OnStepEnded()
    {

    }

    public void Update()
    {
      CheckForNextCreature();
      CleanUpDeadCreatures();

      if (SelectedCreatureActor != null)
      {
        if (!SelectedCreatureActor.ActionRunning)
        {
          InternalUpdate();
        }
      }
    }

    protected virtual void InternalUpdate()
    {

    }

    private void CheckForNextCreature()
    {
      if (SelectedCreatureActor != null)
      {
        if ((!SelectedCreatureActor.HasActionPoints() && !SelectedCreatureActor.ActionRunning) 
          || SelectedCreatureActor.IsDead)
        {
          GetNextCreature();
        }
        return;
      }

      GetNextCreature();
    }

    public void SelectCreature(CreatureActor creatureActor)
    {
      if (creatureActor.ActionPoints != 0 && !creatureActor.IsDead)
      {
        _currentCreature = AliveCreatures.ToList().IndexOf(creatureActor);
        CheckForNextCreature();
        CallSelectedCreatureChanged(creatureActor);
      }
    }

    public bool IsThisPlayerCreature(CreatureActor creatureActor)
    {
      return AliveCreatures.Contains(creatureActor);
    }

    private void GetNextCreature()
    {
      do
      {
        _currentCreature++;

        if (AllCreaturesHaveNoPoints())
        {
          _currentCreature = 0;
          StepEnded = true;
          break;
        }

        if (_currentCreature >= AliveCreatures.Count())
        {
          _currentCreature = 0;
        }

        if (SelectedCreatureActor != null)
        {
          CallSelectedCreatureChanged(SelectedCreatureActor);
        }


        if (SelectedCreatureActor != null
            && SelectedCreatureActor.IsDead)
        {
          RemoveSelectedCreature();
        }
      } while (SelectedCreatureActor != null && SelectedCreatureActor.IsDead && SelectedCreatureActor.ActionPoints == 0);
    }

    private bool AllCreaturesHaveNoPoints()
    {
      return AliveCreatures.All(p => p.ActionPoints == 0);
    }

    private void RemoveSelectedCreature()
    {
      SelectedCreatureActor.Destroy();
      Creatures.Remove(SelectedCreatureActor);
    }

    public void Save(XElement node)
    {
    }

    public void Load(XElement node)
    {
    }

    private void CleanUpDeadCreatures()
    {
      foreach (var creatureActor in Creatures)
      {
        if (creatureActor.IsDead)
        {
          creatureActor.Destroy();
        }
      }

      Creatures.RemoveAll(p => p.IsDead);
    }

    private bool _stepEnded;

    public bool StepEnded
    {
      get { return _stepEnded; }
      set
      {
        if (!_stepEnded && value)
        {
          OnStepEnded();
        }

        _stepEnded = value;
      }
    }

    public CreatureActor SelectedCreatureActor
    {
      get
      {
        if (_currentCreature >= Creatures.Count)
        {
          return null;
        }

        return Creatures[_currentCreature];
      }
    }

    public List<CreatureActor> AliveCreatures
    {
      get { return Creatures.Where(p => !p.IsDead).ToList(); }
    }

    public bool HasCreatures
    {
      get { return Creatures.Any(p => !p.IsDead); }
    }

    public bool SkipFrameOnStepStart { get; set; }
  }
}
