﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using SLua;
using UnityEngine;

namespace GameName1.Fight
{
  [CustomLuaClass]
  public class FightManager : PlayerManager
  {
    private FightPlayer _mainFightPlayer;

    public FightManager()
      : base()
    {
    }


    public CreatureActor GetClosestOppositeCreature(FightPlayer fightPlayer, Point tile)
    {
      var otherPlayers = GetOpposite(fightPlayer);

      float minDistance = float.MaxValue;
      CreatureActor result = null;
      foreach (var otherPlayer in otherPlayers)
      {
        foreach (var creature in otherPlayer.AliveCreatures)
        {
          float distance = tile.Distance(creature.Tile);

          if (distance < minDistance)
          {
            minDistance = distance;
            result = creature;
          }
        }
      }

      return result;
    }

    public List<FightPlayer> GetOpposite(FightPlayer fightPlayer)
    {
      return Players.FindAll(player => player.Side != fightPlayer.Side);
    }

    public bool FightEnded
    {
      get
      {
        return Players.Where(p => p.Side == FightSide.Player).All(p => !p.HasCreatures) ||
               Players.Where(p => p.Side == FightSide.Enemy).All(p => !p.HasCreatures);
      }
    }

    public bool MainPlayerLost
    {
      get
      {
        return Players
          .Where(p => p.Side == FightSide.Player)
          .All(p => !p.HasCreatures);
      }
    }
  }
}

