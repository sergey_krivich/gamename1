﻿using System.Collections.Generic;
using GameName1.Actors;
using GameName1.Lua;
using GameName1.Tiles;
using SLua;
using UnityEngine.Assertions;

namespace GameName1.Fight
{
  [CustomLuaClass]
  public class LuaAIPlayer:FightPlayer
  {
    public string AiName { get; private set; }
    private readonly TileMap _tileMap;
    private readonly AILuaLoader _ailua;
    private readonly FightManager _fightManager;

    private readonly Dictionary<CreatureActor, AILuaBehaviourWrapper> _behaviours =
      new Dictionary<CreatureActor, AILuaBehaviourWrapper>();

    public LuaAIPlayer(TileMap tileMap, AILuaLoader ailua, FightManager fightManager, string aiName = null) 
      : base()
    {
      AiName = aiName;
      _tileMap = tileMap;
      _ailua = ailua;
      _fightManager = fightManager;
    }

    protected override void OnCreatureAdd(CreatureActor creature)
    {
        var ai = AiName ?? creature.Data.AiType;
        var behaviour = _ailua.GetBehaviour(ai, _fightManager, this, creature, _tileMap);
        Assert.IsNotNull(behaviour);

        _behaviours[creature] = behaviour;
    }

    protected override void InternalUpdate()
    {
      base.InternalUpdate();
      CurrentBehaviour.Update();
    }

    private AILuaBehaviourWrapper CurrentBehaviour
    {
      get { return _behaviours[SelectedCreatureActor]; }
    }
  }
}
