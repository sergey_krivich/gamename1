﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameName1.Actors
{
    public static class GameObjectFabric
    {
        public static GameObject CreateSpriteGo(Sprite sprite = null, Material material = null)
        {
            var go = new GameObject();

            var spriteRenderer = go.AddComponent<SpriteRenderer>();
            spriteRenderer.sprite = sprite;
            if (material)
            {
                spriteRenderer.sharedMaterial = material;
            }
            return go;
        }
    }
}
