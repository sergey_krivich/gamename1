﻿using System;
using System.Collections.Generic;
using System.Linq;
using SLua;
using UnityEngine;

namespace GameName1.Actors
{
  [CustomLuaClass]
  public abstract class Actor
  {
    public event Action PositionChanged;

    private List<Actor> _addedActors;

    private Vector2 _position;
    private bool _active;
    private bool _visible;

    public Actor()
    {
      Id = RandomTool.D.NextInt();

      Active = true;
      Visible = true;
      Saveable = true;
    }

    public virtual void Created()
    {
      
    }

    public void Destroy()
    {
      IsDestroyed = true;
    }

    public virtual void Destroyed()
    {

    }

    public virtual void Update()
    {
    }

    public virtual void AddActor(Actor actor)
    {
      if (actor == null)
      {
        Debug.LogError("added actor is null!");
      }

      if (_addedActors == null)
      {
        _addedActors = new List<Actor>();
      }

      _addedActors.Add(actor);
    }

    public bool Active
    {
      get { return _active; }
      set { _active = value; }
    }

    public bool Visible
    {
      get { return _visible; }
      set { _visible = value; }
    }

    public Vector2 Position
    {
      get { return _position; }
      set
      {
        _position = value;
        if (PositionChanged != null)
          PositionChanged();
      }
    }

    public List<Actor> PurgeAddedActors()
    {
      if (_addedActors == null)
        return null;

      var addedActors = _addedActors;
      _addedActors = null;

      return addedActors;
    }

    public bool IsDestroyed { get; private set; }

    public bool Saveable { get; set; }

    public float X
    {
      get { return Position.x; }
      set { Position = new Vector2(value, Position.y); }
    }

    public float Y
    {
      get { return Position.y; }
      set { Position = new Vector2(Position.x, value); }
    }

    public int Id { get; private set; }
  }
}
