﻿using GameName1.Levels;
using Glide;
using SLua;
using UnityEngine;

namespace GameName1.Actors
{
  [CustomLuaClass]
  public class ProjectileActor : Actor
  {
    public string View { get; private set; }
    public float FlightDuration { get; private set; }

    private readonly Point _targetTile;
    private readonly float _speed;

    public ProjectileActor(Point targetTile, float speed, string view)
    {
      View = view;
      _targetTile = targetTile;
      _speed = speed;

    }

    public void Init()
    {
      var targetPosition = TileHelper.TileToPosition(_targetTile);
      var distance = Vector2.Distance(Position, targetPosition);
      FlightDuration = distance / _speed;
    }

    public override void Created()
    {
      Tweener.I.Tween(this,
        new { Position = TileHelper.TileToPosition(TargetTile) }, FlightDuration)
        .OnComplete(Destroy);
    }

    public Point TargetTile
    {
      get { return _targetTile; }
    }
  }
}
