﻿using SLua;
using UnityEngine;

namespace GameName1.Actors
{
  [CustomLuaClass]
  public enum DamageType
  {
    Physical,
    Magic,
    Fire
  }

  [CustomLuaClass]
  public class DamageInfo
  {
    public float Amount;
    public float ArmorPiercing;//percent
    public DamageType Type;
    public CreatureActor Dealer;

    public DamageInfo(float amount, float armorPiercing, DamageType type, CreatureActor dealer)
    {
      Amount = amount;
      ArmorPiercing = Mathf.Clamp01(armorPiercing);
      Type = type;
      Dealer = dealer;
    }

    [StaticExport]
    public static DamageInfo Simple(float amount, CreatureActor dealer)
    {
      return new DamageInfo(amount, 0, DamageType.Physical, dealer);
    }

    public static DamageInfo operator* (DamageInfo d1, float amountMult)
    {
      return new DamageInfo(d1.Amount* amountMult, d1.ArmorPiercing, d1.Type, d1.Dealer);
    }
  }
}
