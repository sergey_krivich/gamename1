﻿using System;
using System.Runtime.CompilerServices;
using SLua;
using UnityEngine;

namespace GameName1.Actors
{
  [CustomLuaClass]
  public class FxEvent
  {
    
  }

  [CustomLuaClass]
  public class FxActor:Actor
  {
    public readonly CreatureActor AttachTo;
    public string View { get; private set; }
    public float Depth { get; set; }

    private readonly Timer _destroyTimer;

    public FxActor(string viewName)
    {
      View = viewName;
      _destroyTimer = new Timer();
    }

    public FxActor(string viewName, CreatureActor attachTo)
      :this(viewName)
    {
      AttachTo = attachTo;
    }

    public void DestroyInSeconds(float seconds)
    {
      if (IsDestroyed || _destroyTimer.IsRunning) return;

      _destroyTimer.Duration = seconds;
      _destroyTimer.OnTick += OnDestroyTimerTick;
      _destroyTimer.Run();
    }

    public override void Update()
    {
      _destroyTimer.Update(Time.deltaTime);
    }

    public void SendFxEvent(FxEvent fxEvent)
    {
      if(IsDestroyed) return;

      //todo: probably add
    }

    private void OnDestroyTimerTick(object sender, EventArgs eventArgs)
    {
      this.Destroy();
    }
  }
}
