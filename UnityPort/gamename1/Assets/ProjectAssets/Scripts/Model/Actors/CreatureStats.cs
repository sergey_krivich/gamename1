using GameName1.Data;
using SLua;

namespace GameName1.Actors
{
  [CustomLuaClass]
  public class CreatureStats
  {
    public float MaxHealth;
    public float MaxStamina;
    public float StaminaRestoring;
    public int MaxActionPoints;
    public float CritChance;
    public float Damage;
    public ProtectionData Protection = new ProtectionData();

    public void CopyTo(CreatureStats other)
    {
      other.MaxHealth = MaxHealth;
      other.MaxStamina = MaxStamina;
      other.StaminaRestoring = StaminaRestoring;
      other.CritChance = CritChance;
      other.Damage = Damage;
      other.Protection = Protection;
      other.MaxActionPoints = MaxActionPoints;
    }

    public static CreatureStats operator +(CreatureStats c1, CreatureStats c2)
    {
      c1.CritChance += c2.CritChance;
      c1.Damage += c2.Damage;
      c1.MaxActionPoints += c2.MaxActionPoints;
      c1.MaxHealth += c2.MaxHealth;
      c1.MaxStamina += c2.MaxStamina;
      c1.StaminaRestoring += c2.StaminaRestoring;
      c1.Protection.Merge(c2.Protection);

      return c1;
    }
  }
}