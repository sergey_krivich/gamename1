﻿using System;
using GameName1.Levels;
using GameName1.Tiles;
using SLua;

namespace GameName1.Actors
{
  [CustomLuaClass]
  public abstract class TileActor : Actor
  {
    protected TileMap TileMap { get; private set; }
    public event Action<TileActor, Point> TileChanged;
    private Point _tile;

    public TileActor(TileMap tileMap)
      : base()
    {
      TileMap = tileMap;
      Solid = true;
    }

    public void SetTile(Point tile, bool updatePosition)
    {
      if (TileChanged != null)
        TileChanged(this, tile);

      if (updatePosition)
      {
        Position = TileHelper.TileToPosition(tile);
      }

      _tile = tile;
    }

    public override void Destroyed()
    {
      base.Destroyed();
      TileMap.RemoveActor(this);
    }


    public Point Tile
    {
      get
      {
        return _tile;
      }
    }

    public int TileX
    {
      get
      {
        return _tile.X;
      }
    }

    public int TileY
    {
      get
      {
        return _tile.Y;
      }
    }

    public bool Solid { get; set; }
  }
}
