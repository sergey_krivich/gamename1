﻿namespace GameName1.Actors.ActionSystem
{
  public class ActionController
  {
    private readonly CreatureActor _creatureActor;
    private Action _currentAction;

    public ActionController(CreatureActor creatureActor)
    {
      _creatureActor = creatureActor;
    }

    public void Update()
    {
      if (_currentAction != null)
      {
        _currentAction.Update();
        if (_currentAction.IsEnded)
        {
          _currentAction = null;
        }
      }
    }

    public bool Do(Action action)
    {
      if (_creatureActor.HasActionPoints())
      {
        if (_currentAction == null || _currentAction.IsEnded)
        {
          _currentAction = action;
          if (_currentAction.Start())
          {
            return true;
          }

          _currentAction = null;
        }
      }
      return false;
    }

    public Action CurrentAction
    {
      get { return _currentAction; }
    }

    public bool ActionRunning
    {
      get
      {
        if (_currentAction == null)
        {
          return false;
        }

        return !_currentAction.IsEnded;
      }
    }
  }
}
