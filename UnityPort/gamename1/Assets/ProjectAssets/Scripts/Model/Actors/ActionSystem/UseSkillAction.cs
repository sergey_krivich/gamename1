﻿using GameName1.Levels;
using GameName1.Lua;
using Glide;
using SLua;
using UnityEngine;

namespace GameName1.Actors.ActionSystem
{
  public class UseSkillAction : Action
  {
    private readonly SkillLuaWrapper _skill;
    private readonly LuaTable _luaParams;

    private bool _animationEnd;
    private bool _failedToApply;

    public UseSkillAction(SkillLuaWrapper skill, CreatureActor creatureActor, LuaTable luaParams)
      : base(creatureActor)
    {
      _skill = skill;
      _luaParams = luaParams;
    }

    protected override bool DoStart()
    {
      if (!CreatureActor.HasActionPoints())
        return false;

      if (!_skill.CanUse(_luaParams))
      {
        return false;
      }

      if (!CreatureActor.CanUseStamina(_skill.GetStaminaUsage()))
      {
        return false;
      }

      if (!PlaySkillAnimation())
        return false;
      return true;
    }

    private bool PlaySkillAnimation()
    {
      if (!_skill.PlayDefaultAnimation())
      {
        _animationEnd = true;
        return DoApplySkill();
      }

      Vector2 endPosition;

      if (_skill.GetRange() > 1f && !_skill.PlayDefaultAnimationOutOfRange())
      {
        endPosition = CreatureActor.Position;
      }
      else
      {
        var target = (Point) _luaParams["targetTile"];
        var targetVec = TileHelper.TileToPosition(target);
        endPosition = Vector2.Lerp(CreatureActor.Position, targetVec, 0.45f);
      }

      var startPos = CreatureActor.Position;

      float time = 0.15f;
      if (endPosition == startPos)
        time = 0f;

      Tweener.I.Tween(CreatureActor, new {X = endPosition.x, Y = endPosition.y}, time)
        .Ease(Ease.BackOut)
        .OnComplete(() =>
        {
          if (!DoApplySkill())
          {
            _failedToApply = true;
          }

          Tweener.I.Tween(CreatureActor, new {X = startPos.x, Y = startPos.y}, time)
            .Ease(Ease.BackOut)
            .OnComplete(
              () =>
              {
                _animationEnd = true;
              });
        });

      return true;
    }

    private bool DoApplySkill()
    {
      if (!_skill.Apply(_luaParams)) return false;

      CreatureActor.UseAllActionPoints();
      CreatureActor.UseStamina(_skill.GetStaminaUsage());
      return true;
    }

    public override void Update()
    {
      base.Update();

      if (_skill.IsDone() || _failedToApply)
      {
        if (_animationEnd)
        {
          End();

          if (!_failedToApply)
          {
            _skill.OnDone();
          }
        }
      }
      else
      {
        _skill.Update();
      }
    }
  }
}
