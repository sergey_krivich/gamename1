﻿using GameName1.Tiles;

namespace GameName1.Actors.ActionSystem
{
  public class TileSolidChecker
  {
    private readonly CreatureActor _creature;

    public TileSolidChecker(CreatureActor creature)
    {
      _creature = creature;
    }

    public bool CheckTile(Tile t)
    {
      bool doesNotHaveOtherCreature =
        !t.HasAnyTileObjects<CreatureActor>(Predicate);
      return doesNotHaveOtherCreature && t.TileData.WalkCost <= _creature.Data.Stats.MaxActionPoints;
    }

    private bool Predicate(CreatureActor t2)
    {
      var v1 = (t2 != _creature);
      var v2 = t2.Data.Side == _creature.Data.Side;
      return v1 && v2;
    }
  }
}
