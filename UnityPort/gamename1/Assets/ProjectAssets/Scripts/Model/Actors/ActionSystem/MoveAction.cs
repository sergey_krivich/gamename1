﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Lua;
using GameName1.PathFinding;
using GameName1.Tiles;
using Glide;
using UnityEngine;

namespace GameName1.Actors.ActionSystem
{
  public class MoveAction : Action
  {
    private readonly AStar _aStar;

    private List<Point> _path;

    private readonly TileMap _tileMap;
    private readonly SkillLuaWrapper _attackSkill;
    private readonly Point _targetTile;
    private readonly int _stopDistance;
    private Point _currentTile;
    private Vector2 _endPosition;

    private UseSkillAction _skillAction;


    public MoveAction(TileMap tileMap, CreatureActor creatureActor, SkillLuaWrapper attackSkill, Point targetTile, int stopDistance)
      : base(creatureActor)
    {
      _tileMap = tileMap;
      _attackSkill = attackSkill;
      _targetTile = targetTile;
      _stopDistance = stopDistance;

      var tileSolidChecker = new TileSolidChecker(creatureActor);
      _aStar = new AStar(tileMap, tileSolidChecker.CheckTile);
    }

    protected override bool DoStart()
    {
      base.DoStart();

      if (_tileMap.IsFree(_targetTile))
      {
        return TryMoveToTile(_targetTile);
      }


      return false;
    }

    private bool TryMoveToTile(Point newTile)
    {
      if (!_tileMap.InRange(newTile))
      {
        return false;
      }

      _path = _aStar.FindCellWay(CreatureActor.Tile, newTile);

      if (_path == null)
      {
        return false;
      }

      if (_path.Count > _stopDistance)
        _path.RemoveRange(_path.Count - _stopDistance, _stopDistance);
      else
      {
        _path.Clear();
      }

      //var creatureOnTile = _tileMap.GetCreatureFromTile(newTile);
      if (_path.Count > 1)
      {
        if (_tileMap[_path[1]].TileData.WalkCost > CreatureActor.ActionPoints)
          return false;

        ContinueMovement();
        return true;
      }

      return false;
    }


    private bool InAttackRange(Point tile)
    {
      float distance = CreatureActor.Tile.Distance(tile);
      if (distance <= _attackSkill.GetRange())
      {
        return true;
      }

      return false;
    }


    private bool MoveByTiles(Point newTile)
    {
      _currentTile = newTile;

      var creatureOnTile = _tileMap.GetCreatureFromTile(_currentTile);
      _endPosition = Levels.TileHelper.TileToPosition(_currentTile);

      if (creatureOnTile != null)
      {
        Attack(creatureOnTile);
        return true;
      }


      Move();
      return false;
    }

    private void Move()
    {
      var cost = _tileMap[_currentTile].TileData.WalkCost;
      if (cost <= 0)
        cost = 1f;

      if (CreatureActor.HasEnoughActionPoints(cost))
      {
        CreatureActor.UseActionPoints(cost);

        Tweener.I.Tween(CreatureActor, new {X = _endPosition.x, Y = _endPosition.y}, 0.2f)
          .Ease(Ease.BackOut)
          .OnComplete(
            OnMoveCompleted);
      }
      else
      {
        End();
      }
    }

    private void OnMoveCompleted()
    {
      CreatureActor.SetTile(_currentTile, false);
      ContinueMovement();
    }

    private void ContinueMovement()
    {
      _path.RemoveAt(0);
      if (_path.Count > 0)
      {
        var currentTile = _path.First();
        var creatureOnTile = _tileMap.GetCreatureFromTile(_targetTile);
        if (creatureOnTile != null && InAttackRange(_targetTile))
        {
          _endPosition = creatureOnTile.Position;
          Attack(creatureOnTile);
        }
        else
        {
          AudioManager.I.Play("Step");
          MoveByTiles(currentTile);
        }
      }
      else
      {
        End();
      }
    }

    private void Attack(CreatureActor creatureActorOnTile)
    {
      var luaDict = new DictLua
      {
        {"targetTile", creatureActorOnTile.Tile}
      };
      _skillAction = new UseSkillAction(_attackSkill, CreatureActor, luaDict.ToLua(AppRoot.I.LuaLoader.State));
      _skillAction.Start();
    }

    public override void Update()
    {
      if (_skillAction != null)
      {
        _skillAction.Update();

        if (_skillAction.IsEnded)
        {
          End();
        }
      }
    }
  }
}