﻿using System.Linq;
using GameName1.Tiles;

namespace GameName1.Actors.ActionSystem
{
    public class UseAction:Action
    {
      private readonly TileMap _tileMap;

      public UseAction(TileMap tileMap, CreatureActor creatureActor) 
            : base(creatureActor)
      {
        _tileMap = tileMap;
      }

      protected override bool DoStart()
        {
            var tileObjects = _tileMap.GetTileObjects(CreatureActor.Tile).Where(p => p is IUseable).ToList();

            if(tileObjects.Count > 0)
            {
                IUseable useable = tileObjects.First() as IUseable;
                if (useable.CanUse(CreatureActor))
                {
                    useable.Use(CreatureActor);
                    End();
                    return true;
                }
            }

            return false;
        }
    }
}
