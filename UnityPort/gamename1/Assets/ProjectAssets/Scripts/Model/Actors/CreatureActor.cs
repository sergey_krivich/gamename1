﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameName1.Actors.ActionSystem;
using GameName1.Data;
using GameName1.Fight;
using GameName1.Lua;
using GameName1.Skills;
using GameName1.StatusEffects;
using GameName1.Tiles;
using SLua;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameName1.Actors
{
  [CustomLuaClass]
  public class CreatureActor : TileActor
  {
    protected readonly ActionController ActionController;
    public readonly StatusEffectsManager StatusEffects;

    public event Action<float> HealthChanged;
    public event Action<float> StaminaChanged;

    public readonly Abilities Abilities;


    public CreatureStats Stats { get; private set; }


    private float _health;
    private float _stamina;

    public CreatureActor(TileMap tileMap, UnitData data)
      : base(tileMap)
    {
      Assert.IsNotNull(data, "info");
      Assert.IsNotNull(tileMap, "tileMap");

      Data = data;
      Stats = new CreatureStats();

      ActionController = new ActionController(this);
      StatusEffects = new StatusEffectsManager(this);


      RecalculateStats(true);
      Health = Data.CurrentHp;
      Stamina = Stats.MaxStamina;

      Abilities = new Abilities();

      ResetActionPoints();
    }

    public void InitSkills(SkillLuaLoader skillLuaLoader)
    {
      var skillIds = Data.SkillIds.ToList();
      skillIds.Insert(0, Data.DefaultAttackSkillId);

      Abilities.InitSkills(skillLuaLoader, TileMap, this, skillIds);
    }

    public void InitStatusEffects(StatusEffectLuaLoader statusEffectLuaLoader)
    {
      StatusEffects.InitLoader(statusEffectLuaLoader, TileMap);
    }

    public virtual void OnStepStarted()
    {
      RecalculateStats();

      Stamina += Stats.StaminaRestoring;
      ClampStats();

      ResetActionPoints();

      StatusEffects.OnStepStarted();
      Abilities.OnTurnStart();
    }

    public virtual void SetPlayer(FightPlayer fightPlayer)
    {
      FightPlayer = fightPlayer;
    }

    public void UseAllActionPoints()
    {
      ActionPoints = 0;
    }

    public void UseActionPoint()
    {
      if(ActionPoints > 0)
        ActionPoints -= 1;
    }

    public void UseActionPoints(float amount)
    {
      if (ActionPoints >= amount)
        ActionPoints -= amount;
    }


    public void ResetActionPoints()
    {
      ActionPoints = Stats.MaxActionPoints;
    }

    public void Skip()
    {
      if (!ActionController.ActionRunning)
      {
        ActionPoints = 0;
      }
    }

    public void AddStatusEffectInfinite(string name, CreatureActor creator, LuaTable data)
    {
      StatusEffects.Add(name, creator, data, true);
      RecalculateStats();
    }

    public void AddStatusEffect(string name, CreatureActor creator, LuaTable data)
    {
      StatusEffects.Add(name, creator, data);
      RecalculateStats();
    }

    public bool Use()
    {
      return ActionController.Do(new UseAction(TileMap, this));
    }

    public bool Move(Point target)
    {
      return Move(target.X, target.Y);
    }

    public bool Move(int x, int y, int stopDistance = 0)
    {
      var targetTile = new Point(x, y);
      var skillWrapper = Abilities.GetSkillWrapperById(Data.DefaultAttackSkillId);

      var moveAction = new MoveAction(TileMap, this, skillWrapper, targetTile, stopDistance);
      return ActionController.Do(moveAction);
    }

    public bool UseSkill(string skillId, LuaTable param)
    {
      ResetSkill();

      var skillWrapper = Abilities.GetSkillWrapperById(skillId);
      if (skillWrapper == null)
      {
        return false;
      }

      var skillAction = new UseSkillAction(skillWrapper, this, param);
      return ActionController.Do(skillAction);
    }

    public void RecieveDamageSimple(float amount, CreatureActor dealer)
    {
      RecieveDamage(DamageInfo.Simple(amount, dealer));
    }

    public void RecieveDamage(DamageInfo damageInfo)
    {
      float protection = Data.Stats.Protection.Get(damageInfo.Type);
      float landProtection = TileMap[Tile].TileData.ProtectionLevel;

      float totalProtection = protection + landProtection;
      totalProtection = Mathf.Clamp(totalProtection, -100f, 0.99f);//can receive more damage if negative protection

      var damage = damageInfo.Amount;
      if (totalProtection > 0)
        damage -= damage * (totalProtection * (1f - damageInfo.ArmorPiercing));

      Health -= damage;

      if (!string.IsNullOrEmpty(Data.HitFx))
      {
        var fxActor = new FxActor(Data.HitFx)
        {
          Position = Position
        };
        AddActor(fxActor);
      }

      damageInfo.Amount = damage;
      StatusEffects.OnTakeDamage(damageInfo);

      if (IsDead)
      {
        Die();
      }

      ClampStats();
    }

    public float HealthPercent {
      get { return Health / Stats.MaxHealth; }
    }

    public void Heal(float amount)
    {
      Health += amount;
      ClampStats();
    }

    private void ClampStats()
    {
      Health = Mathf.Clamp(Health, 0, Stats.MaxHealth);
      Stamina = Mathf.Clamp(Stamina, 0, Stats.MaxStamina);
    }

    public void Die()
    {
      if (!string.IsNullOrEmpty(Data.DeathFx))
      {
        var fxActor = new FxActor(Data.DeathFx)
        {
          Position = Position
        };
        AddActor(fxActor);
      }
      Destroy();
    }

    public override void Update()
    {
      ActionController.Update();
      base.Update();
    }

    public float GetAttackDamage()
    {
      float critMult = 1f;

      if (RandomTool.D.NextBool(Stats.CritChance))
      {
        critMult = 1.5f;
      }

      return Stats.Damage*critMult;
    }

    public bool CanUseStamina(float staminaUsage)
    {
      return staminaUsage <= Stamina;
    }

    public bool UseStamina(float staminaUsage)
    {
      if (CanUseStamina(staminaUsage))
      {

        Stamina -= staminaUsage;
        return true;
      }

      return false;
    }

    private void RecalculateStats(bool initial =false)
    {
      var hpPercent = initial ? 1f : HealthPercent;

      Data.Stats.CopyTo(Stats);
      StatusEffects.ApplyStatsChange(Stats);
      Data.Equipment.ApplyBonuses(Stats);

      Health = hpPercent*Stats.MaxHealth;
      ClampStats();
    }

    public float ActionPoints { get; private set; }

    public bool HasActionPoints()
    {
      return ActionPoints > 0;
    }

    public bool HasEnoughActionPoints(float amount)
    {
      return ActionPoints >= amount;
    }

    public float Health
    {
      get
      {
        return _health;
      }
      private set
      {
        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if (_health == value) return;

        var change = value - _health;
        _health = value;

        if (_health > Stats.MaxHealth)
        {
          _health = Stats.MaxHealth;
        }

        if (HealthChanged != null)
        {
          HealthChanged(change);
        }
      }
    }

    public float Stamina
    {
      get
      {
        return _stamina;
      }
      private set
      {
        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if (_stamina == value) return;

        var change = value - _stamina;
        _stamina = value;

        if (_stamina > Stats.MaxStamina)
        {
          _stamina = Stats.MaxStamina;
        }

        if (StaminaChanged != null)
        {
          StaminaChanged(change);
        }
      }
    }
    public virtual bool IsDead
    {
      get { return Health <= 0; }
    }

    public FightPlayer FightPlayer { get; private set; }

    public bool ActionRunning
    {
      get { return ActionController.ActionRunning; }
    }

    public UnitData Data { get; private set; }

    public void SelectSkill(string skillId)
    {
      Abilities.SelectSkill(skillId);
    }

    public void ResetSkill()
    {
      Abilities.ResetSelectedSkill();
    }
  }
}
