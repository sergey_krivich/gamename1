﻿using System;
using GameName1.Fight;
using GameName1.Levels;
using GameName1.Tiles;

namespace GameName1.Actors
{
  public class World
  {
    public TileMap TileMap
    {
      get { return Level.TileMap; }
    }

    public ActorManager ActorManager { get; private set; }

    public Level Level{ get; private set; }

    public World()
    {
      ActorManager = new ActorManager();
      ActorManager.ActorAdded += ActorManagerOnActorAdded;
      ActorManager.ActorRemoved += ActorManagerOnActorRemoved;
    }

    public void AddActor(Actor actor)
    {
      ActorManager.Add(actor);
    }

    public void Save(FightManager fightManager = null)
    {
      GameSave save = new GameSave();
      save.Save(Level, ActorManager.Actors, null, fightManager == null ? null : fightManager.Players);
    }

    private void ActorManagerOnActorAdded(Actor actor)
    {
      if (actor is TileActor)
      {
        TileMap.AddActor(actor as TileActor);

      }

      if (actor is CreatureActor)
      {
        if (AppRoot.I != null)
        {
          //todo:
          (actor as CreatureActor).InitSkills(AppRoot.I.LuaLoader.SkillLoader);
          (actor as CreatureActor).InitStatusEffects(AppRoot.I.LuaLoader.StatusEffectsLoader);
        }
      }
    }

    private void ActorManagerOnActorRemoved(Actor actor)
    {
    }

    public void Load()
    {
      GameSave save = new GameSave();
      save.Load();
    }

    public void Update()
    {
      ActorManager.Update();
    }

    public void GoTo(Level level)
    {
      Level = level;
    }
  }
}
