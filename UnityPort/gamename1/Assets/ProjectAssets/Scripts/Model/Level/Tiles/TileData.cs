﻿using System.Collections.Generic;
using SLua;

namespace GameName1.Tiles
{
  [CustomLuaClass]
  public enum SpriteIndex
  {
    Default = 0,
    TopLeft = 1,
    Top = 2,
    TopRight = 3,
    Left = 4,
    Middle = 5,
    Right = 6,
    BottomLeft= 7,
    Bottom = 8,
    BottomRight =9
  }

  [CustomLuaClass]
  public class Mask
  {
    [DoNotToLua]
    public int[,] Array = new int[3,3];//slua does not export 2d arrays very well.
    public SpriteIndex SpriteIndex;
    public int TileCount = -1;
    public void SetMask(int i, int j, int value)
    {
      Array[i, j] = value;
    }
  }

  [CustomLuaClass]
  public class TileData
  {
    public List<Mask> Masks = new List<Mask>();
    public string Sprite;
    public List<int> SpriteVariations = new List<int>();
    public List<float> SpriteVariationsChance = new List<float>();

    public bool IsSolid;
    public int Type;//for tile equals checks in mask
    public float ProtectionLevel;
    public float WalkCost = 1.0f;

    public void AddMask(Mask mask)
    {
      Masks.Add(mask);
    }

    public void AddVariation(int spriteIndex, float chance)
    {
      SpriteVariations.Add(spriteIndex);
      SpriteVariationsChance.Add(chance);
    }
  }
}
