﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;

namespace GameName1.Tiles
{
  public class Tile
  {
    private List<TileActor> _tileObjects;

    public Point Position { get; private set; }
    public TileData TileData { get; set; }
    public SpriteIndex SpriteIndex { get; set; }

    public void Init(TileData tileData, Point tile)
    {
      Position = tile;
      TileData = tileData;
    }

    public void AddTileObject(TileActor tileActor)
    {
      TileObjects.Add(tileActor);
    }

    public void RemoveTileObject(TileActor tileActor)
    {
      TileObjects.Remove(tileActor);
    }

    public bool HasTileObject(TileActor tileActor)
    {
      return TileObjects.Contains(tileActor);
    }

    private List<TileActor> TileObjects
    {
      get
      {
        if (_tileObjects == null)
        {
          _tileObjects = new List<TileActor>();
        }
        return _tileObjects;
      }
    }


    public List<TileActor> GetTileObjects()
    {
      if (_tileObjects == null)
      {
        return new List<TileActor>();
      }

      return TileObjects.ToList();
    }

    public bool HasAnyTileObjects()
    {
      if (_tileObjects == null)
      {
        return false;
      }

      return TileObjects.Count > 0;
    }

    public bool HasAnyTileObjects<T>(Predicate<T> predicate = null) where T : TileActor
    {
      if (_tileObjects == null)
      {
        return false;
      }

      for (int i = 0; i < TileObjects.Count; i++)
      {
        if (TileObjects[i].Active)
        {
          if (TileObjects[i] is T)
          {
            if (predicate != null)
            {
              return predicate((T) TileObjects[i]);

            }
            return true;
          }
        }
      }

      return false;
    }
  }
}
