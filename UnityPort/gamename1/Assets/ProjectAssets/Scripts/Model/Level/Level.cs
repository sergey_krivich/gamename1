﻿using System.Collections.Generic;
using GameName1.Tiles;
using SLua;
using UnityEngine;

namespace GameName1.Levels
{
  [CustomLuaClass]
  public enum SpawnAlgorithmType
  {
    Column,
    Random,
    Fill
  }

  public class SpawnPoint
  {
    public Point Tile;
    public Vector2 Direction;
    public SpawnAlgorithmType SpawnAlgorithm;
  }

  [CustomLuaClass]
  public class Level
  {
    public string SpriteAtlas;

    public Color LightColor = Color.white;
    public float LightIntensity = 1.5f;

    private TileMap _tileMap;
    private readonly List<SpawnPoint> _spawnPoints = new List<SpawnPoint>();

    public TileMap SetTileMap(TileMap tileMap)
    {
      _tileMap = tileMap;
      _tileMap.CalculateSpriteIndexes();

      return _tileMap;
    }

    public void AddSpawnPoint(Point point, Vector3 direction, SpawnAlgorithmType spawnAlgorithm)
    {
      _spawnPoints.Add(new SpawnPoint
      {
        Direction = direction,
        Tile = point,
        SpawnAlgorithm = spawnAlgorithm
      });
    }

    public void SetLightSettings(float r, float g, float b, float intensity)
    {
      LightColor = new Color(r,g,b,1);
      LightIntensity = intensity;
    }

    public void Clean()
    {
      _tileMap.Deinit();
    }

    public TileMap TileMap
    {
      get { return _tileMap; }
    }

    public SpawnPoint GetSpawnPoint(int spawnPointIndex)
    {
      return _spawnPoints[spawnPointIndex];
    }
  }
}
