﻿using System;
using System.Collections;
using GameName1.Network;
using UnityEngine;
using UnityEngine.UI;

namespace GameName1.UI
{
  public class MainMenuView : UIView
  {
    public InputField Input;

    public event Action<string> OnJoinClicked;
    public event Action OnCreateClicked;
    public event Action OnSendClicked;


    public override void OnEnable()
    {
#if UNITY_EDITOR
      if(AppRoot.I.Config.FastStart)
        StartCoroutine(FakeStart());
#endif
    }

    private IEnumerator FakeStart()
    {
      yield return null;
      StartClicked();
      yield return null;
      SendClicked();
      yield return null;
      SendClicked();
    } 

    public void SendClicked()
    {
      if (OnSendClicked != null)
        OnSendClicked();
    }

    public void JoinClicked()
    {
      if (OnJoinClicked != null)
        OnJoinClicked(Input.text);
    }

    public void StartClicked()
    {
      if (OnCreateClicked != null)
        OnCreateClicked();
    }


  }
}
