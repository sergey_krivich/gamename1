﻿using GameName1;
using UnityEngine;
using UnityEngine.UI;

public class SkillDescriptionView : MonoBehaviour
{
  public Image Image;
  public Text Text;
  public Text Name;
  public Text StaminaCost;
  public Text Cooldown;

  public void Set(string skillId)
  {
    var spritePath = AppRoot.I.LuaLoader.SkillsMeta.GetSpriteName(skillId);
    var description = AppRoot.I.LuaLoader.SkillsMeta.GetDescription(skillId);
    var skillName = AppRoot.I.LuaLoader.SkillsMeta.GetName(skillId);
    var staminaCost = AppRoot.I.LuaLoader.SkillsMeta.GetStaminaCost(skillId);
    var cooldown = AppRoot.I.LuaLoader.SkillsMeta.GetCooldown(skillId);

    Image.sprite = ResourceManager.LoadSprite(spritePath);
    Text.text = description;
    Name.text = skillName;
    StaminaCost.text = "Stamina: " + staminaCost.ToString("F1");
    Cooldown.text = "Cooldown: " + (int) cooldown;
  }
}
