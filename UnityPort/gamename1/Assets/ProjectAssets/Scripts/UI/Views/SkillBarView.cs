﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Data;
using GameName1.States;
using GameObjectExtensions;
using UnityEngine;
using UnityEngine.UI;

public class SkillBarView : UIView
{
  public RectTransform ButtonsRoot;
  public SkillButtonView ButtonPrefab;
  private FightState _state;
  private CreatureActor _creature;
  public SkillDescriptionView SkillDescriptionView;

  private bool _shouldUpdate;

  public void SetState(FightState state)
  {
    _state = state;

    foreach (var player in _state.FightManager.Players)
    {
      player.SelectedCreatureChanged += PlayerOnSelectedCreatureChanged;
    }
  }

  private void PlayerOnSelectedCreatureChanged(CreatureActor creature)
  {
    _creature = creature;
    UpdateButtons();
  }

  private void UpdateButtons()
  {
    ClearRoot(ButtonsRoot);
    _shouldUpdate = true;
    foreach (var skill in _creature.Abilities.GetAvailableSkills())
    {
      var spritePath = AppRoot.I.LuaLoader.SkillsMeta.GetSpriteName(skill.SkillWrapper.Id);

      if (string.IsNullOrEmpty(spritePath))
      {
        continue;
      }

      var button = ButtonPrefab.gameObject.Create<SkillButtonView>();
      button.SkillDescriptionView = SkillDescriptionView;
      button.Set(skill.SkillId);
      button.Set(_creature, skill.SkillWrapper);
      button.gameObject.transform.SetParent(ButtonsRoot, false);
      button.Button.onClick.AddListener(() => OnButtonClick(skill));
    }
  }


  private void OnButtonClick(SkillData skill)
  {
    if (AppRoot.I.Players.CreatureBelongsToLocalPlayer(_creature.Data))
    {
      _creature.SelectSkill(skill.SkillId);
    }
  }

  void LateUpdate()
  {
    if (_shouldUpdate)
    {
      //fixes skill buttons layout....
      ButtonsRoot.gameObject.SetActive(false);
      ButtonsRoot.gameObject.SetActive(true);
    }
  }

  private void OnDestroy()
  {
    SkillDescriptionView.gameObject.SetActive(false);
  }
}
