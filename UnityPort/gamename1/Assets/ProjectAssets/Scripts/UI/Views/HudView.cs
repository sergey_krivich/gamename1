﻿using GameName1.Actors;
using GameName1.States;
using UnityEngine;
using UnityEngine.UI;

public class HudView : UIView
{
  public Button CancelButton;
  public Button SkipButton;
  public Text SkillText;
  public Text DifficultyText;
  public Text ActionsText;

  public ProgressBar HpBar;
  public ProgressBar StaminaBar;
  public Text HpBarText;
  public Text StaminaBarText;

  public GameObject SkillTextRoot;
  private FightState _state;
  private CreatureActor _creature;


  public void SetState(FightState state)
  {
    SkipButton.onClick.AddListener(OnSkipClick);
    CancelButton.onClick.AddListener(OnCancelClick);
    _state = state;

    foreach (var player in _state.FightManager.Players)
    {
      player.SelectedCreatureChanged += PlayerOnSelectedCreatureChanged;
    }
  }

  private void PlayerOnSelectedCreatureChanged(CreatureActor creature)
  {
    _creature = creature;
    UpdateButtons();
  }

  private void UpdateButtons()
  {
    if (_creature == null)
    {
      CancelButton.gameObject.SetActive(false);
      SkipButton.gameObject.SetActive(false);
      SkillText.text = "";
      SkillTextRoot.SetActive(false);
      HpBar.gameObject.SetActive(false);
      StaminaBar.gameObject.SetActive(false);
      return;
    }

    if (_creature.Abilities.SelectedSkillId != null)
    {
      CancelButton.gameObject.SetActive(true);
      SkillText.text = _creature.Abilities.SelectedSkill.Id;
      SkillTextRoot.SetActive(true);
    }
    else
    {
      SkillText.text = "";
      CancelButton.gameObject.SetActive(false);
      SkillTextRoot.SetActive(false);
    }

    if (AppRoot.I.Players.CreatureBelongsToLocalPlayer(_creature.Data))
    {
      SkipButton.gameObject.SetActive(true);
    }
    else
    {
      SkipButton.gameObject.SetActive(false);
    }

    HpBar.SetValue(_creature.HealthPercent);
    StaminaBar.SetValue(_creature.Stamina / _creature.Stats.MaxStamina);
    HpBarText.text = "Health: " + _creature.Health + "/" + _creature.Stats.MaxHealth;
    StaminaBarText.text = "Stamina: " + _creature.Stamina + "/" + _creature.Stats.MaxStamina;
    ActionsText.text = "Actions: " + _creature.ActionPoints.ToString("F1");
  }

  private void OnCancelClick()
  {
    _creature.ResetSkill();
  }

  private void OnSkipClick()
  {
    _creature.Skip();
  }

  void Update ()
  {
    DifficultyText.text = "Difficulty: " + AppRoot.I.GameStateData.Difficulty.ToString();
    UpdateButtons();
  }
}
