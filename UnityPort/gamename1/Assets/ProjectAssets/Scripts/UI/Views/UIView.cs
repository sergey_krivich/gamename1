﻿using UnityEngine;

public class UIView : MonoBehaviour
{
  public virtual void OnEnable()
  {
    
  }

    public void SetVisible(bool active)
    {
        gameObject.SetActive(active);
    }

    public void ClearRoot(Transform root)
    {
        foreach (Transform transform in root)
        {
            GameObject.Destroy(transform.gameObject);
        }
    }
}
