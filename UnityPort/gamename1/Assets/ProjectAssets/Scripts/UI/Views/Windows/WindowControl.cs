﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WindowControl : MonoBehaviour
{
  public static WindowControl I;

  private List<UIView> _windows;

  void Awake()
  {
    I = this;
    _windows = GetComponentsInChildren<UIView>(true).ToList();
  }

  void OnDestroy()
  {
    I = null;
  }

  public T Show<T>() where T:MonoBehaviour
  {
    var window = _windows.Find(w => w.GetComponent<T>() != null);

    if (window != null)
    {
      window.gameObject.SetActive(true);
      return window.GetComponent<T>();
    }

    Debug.LogError("Window not found " + typeof(T).Name);
    return null;
  }

  public void Hide<T>() where T : MonoBehaviour
  {
    var window = _windows.Find(w => w.GetComponent<T>() != null);
    if (window != null)
    {
      window.gameObject.SetActive(false);
    }
    else
    {
      Debug.LogError("Window not found " + typeof(T).Name);
    }
  }
}
