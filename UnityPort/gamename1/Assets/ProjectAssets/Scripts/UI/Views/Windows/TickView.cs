﻿using System.Collections.Generic;
using UnityEngine;

public class TickView : MonoBehaviour
{
  public List<GameObject> Ticks;

  public Transform Root;
  public GameObject TickPrefab;
	void OnEnable ()
	{
	  //N_ticked.Clear();
	  ResetView();
	}

  public void AddTick()
  {
    var prefab = Instantiate(TickPrefab, Root, false);
    Ticks.Add(prefab);
  }

  private void ResetView()
  {
    foreach (var tick in Ticks)
    {
      tick.SetActive(false);
    }
  }
}
