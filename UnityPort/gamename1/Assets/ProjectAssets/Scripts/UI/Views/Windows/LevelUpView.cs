﻿using System;
using System.Collections.Generic;
using GameName1;
using GameObjectExtensions;
using UnityEngine;
using UnityEngine.UI;

public class LevelUpView : UIView
{
  public event Action<string> OnSkillSelected;

  public Button ConfirmButton;
  public GameObject SkillButtonPrefab;

  public Image Character;
  public Transform NewSkillsRoot;
  public Transform LearnedSkillsRoot;
  public SkillDescriptionView DescriptionView;

  private string _selectedItem;

  private readonly List<GameObject> _buttons = new List<GameObject>();

  public class Data
  {
    public List<string> AvailableSkills;
    public List<string> AlreadyLearned;
    public string CharacterSprite;
  }

	void Start ()
  {
    ConfirmButton.onClick.AddListener(() =>
    {
      if (_selectedItem != null)
      {
        if (OnSkillSelected != null)
        {
          OnSkillSelected(_selectedItem);
        }
      }
    });
  }

  public void SetData(Data data)
  {
    OnSkillSelected = null;
    _selectedItem = null;
    ClearRoot(NewSkillsRoot);
    ClearRoot(LearnedSkillsRoot);
    _buttons.Clear();

    SetNewSkills(data);

    foreach (var learnedSkill in data.AlreadyLearned)
    {
      var item = SkillButtonPrefab.Create();
      item.transform.SetParent(LearnedSkillsRoot, false);
      item.transform
        .Find("SelectedImage")
        .GetComponent<Image>().enabled = false;

      SetSkill(learnedSkill, item);
    }

    Character.sprite = ResourceManager.LoadSprite(data.CharacterSprite);
  }

  private void SetNewSkills(Data data)
  {
    foreach (var skillName in data.AvailableSkills)
    {
      if (data.AlreadyLearned.Contains(skillName))
      {
        continue;
      }

      var item = SkillButtonPrefab.Create();
      item.transform.SetParent(NewSkillsRoot, false);

      SetSkill(skillName, item);

      item.GetComponent<Button>().onClick.AddListener(() =>
      {
        UpdateSelection(item);
        _selectedItem = skillName;
      });

      _buttons.Add(item);
    }

    UpdateSelection(null);
  }

  private void SetSkill(string skillId, GameObject item)
  {
    var view = item.GetComponent<SkillButtonView>();
    view.SkillDescriptionView = DescriptionView;
    view.Set(skillId);
  }

  private void UpdateSelection(GameObject item)
  {
    foreach (var button in _buttons)
    {
      button.transform
        .Find("SelectedImage")
        .GetComponent<Image>().enabled = button == item;
    }
  }
}
