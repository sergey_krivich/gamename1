﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class FightLostView : UIView
{
  public Image Background;
  public Text Text;

  public Image Background2;
  public Text Text2;

  public event Action ContinueClicked;

  public override void OnEnable()
  {
    base.OnEnable();

    Background.color = new Color(0,0,0,0);
    Text.color = new Color(Text.color.r, Text.color.g, Text.color.b, 0);
    Background2.color = new Color(Background2.color.r, Background2.color.g, Background2.color.b, 0);
    Text2.color = new Color(Text.color.r, Text.color.g, Text.color.b, 0);
  }

  void Update()
  {
    Background.color = Color.Lerp(Background.color, new Color(0, 0, 0, 0.7f), Time.deltaTime * 4);
    Text.color = Color.Lerp(Text.color, new Color(Text.color.r, Text.color.g, Text.color.b, 0.7f), Time.deltaTime * 4);

    Background2.color = Color.Lerp(Background2.color, new Color(Background2.color.r, Background2.color.g, Background2.color.b, 1), Time.deltaTime * 4);
    Text2.color = Color.Lerp(Text2.color, new Color(Text2.color.r, Text2.color.g, Text2.color.b, 1), Time.deltaTime * 4);
  }

  public void ContinueClick()
  {
    if(ContinueClicked != null)
      ContinueClicked();
  }
}
