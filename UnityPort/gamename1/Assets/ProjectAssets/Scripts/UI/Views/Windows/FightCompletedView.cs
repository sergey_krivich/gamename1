﻿using System;
using GameName1.Data;
using UnityEngine;
using UnityEngine.UI;

public class FightCompletedView : UIView
{
  public class Result
  {
    public EventOutcomeReward Reward;
  }

  public GameObject ListItemPrefab;
  public Transform ItemsRoot;

  public Button ContinueButton;

  public event Action OnContinueClick;

  void Start()
  {
    ContinueButton.onClick.AddListener(() =>
    {
      if(OnContinueClick != null)
        OnContinueClick();
    });
  }

  void Update()
  {
    if (Input.GetKeyDown(KeyCode.Return))
    {
      if (OnContinueClick != null)
        OnContinueClick();
    }
  }

  public void SetResult(Result result)
  {
    return;
    //foreach (var itemType in result.Reward.Items)
    //{
    //  var item = ListItemPrefab.Create();
    //  item.transform.SetParent(ItemsRoot, false);
    //  item.GetComponentInChildren<Text>().text = itemType.Name;
    //  item.GetComponentInChildren<Image>().sprite = ResourceManager.LoadSprite(itemType.Icon);
    //}
  }
}
