﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameName1.Items;
using GameName1.States;
using GameObjectExtensions;
using UnityEngine;
using UnityEngine.UI;

namespace GameName1.UI
{
  public class EventChoiceView : UIView
  {
    public GameObject ChoicePrefab;
    public Text Header;
    public Transform ChoicesRoot;

    public GameObject Rewards;
    public Transform RewardsRoot;
    public GameObject RewardPrefab;

    private readonly List<GameObject> _buttons = new List<GameObject>();
    private readonly Dictionary<int, Action> _callbacks = new Dictionary<int, Action>();
    private EventChoiceState _state;

    public void SetState(EventChoiceState eventChoiceState)
    {
      _state = eventChoiceState;
      _state.TravelEventChanged += StateOnEventChanged;
      _state.OutcomeChanged += StateOnEventChanged;
      _state.ChoiceAdded += StateOnChoiceAdded;
      Rebuild();
    }

    public void Update()
    {
      foreach (var callback in _callbacks.ToDictionary(k => k.Key, v => v.Value))
      {
        if (Input.GetKeyDown(KeyCode.Alpha1 + callback.Key) || AppRoot.I.Config.FastEvents)
        {
          callback.Value();
          break;
        }
      }
    }

    private void StateOnEventChanged()
    {
      Clear();
      Rebuild();
    }

    private void StateOnChoiceAdded(int index)
    {
      AddTick(index);
    }

    public void Rebuild()
    {
      Clear();

      if (_state.Outcome == null)
      {
        RebuildEvent();
      }
      else
      {
        RebuildOutcome();
      }
    }

    private void RebuildOutcome()
    {
      Header.text = _state.Outcome.Description;

      AddOutcomeChoices();
      AddOutcomeRewards();
    }

    private void AddOutcomeChoices()
    {
      var choice = ChoicePrefab.Create();
      choice.transform.SetParent(ChoicesRoot, false);

      choice.GetComponentInChildren<Text>().text = "1. Continue";
      Action callback = () => { _state.MakeChoice(0); };
      choice.GetComponentInChildren<Button>().onClick.AddListener(() => callback());
      _buttons.Add(choice);
      _callbacks[0] = callback;
    }

    private void AddOutcomeRewards()
    {
      if (_state.Outcome.Reward == null)
      {
        Rewards.SetActive(false);
      }
      else
      {
        Rewards.SetActive(true);
        foreach (var itemType in _state.Outcome.Reward.Items)
        {
          CreateRewardGo(itemType.Type.Name, itemType.Type.Icon);
        }

        foreach (var itemType in _state.Outcome.Reward.Units)
        {
          CreateRewardGo(itemType.Name, itemType.Sprite);
        }
      }
    }

    private void CreateRewardGo(string name, string icon)
    {
      var item = RewardPrefab.Create();
      item.transform.SetParent(RewardsRoot, false);
      item.GetComponentInChildren<Text>().text = name;
      item.GetComponentInChildren<Image>().sprite = ResourceManager.LoadSprite(icon);
    }

    private void RebuildEvent()
    {
      Header.text = _state.TravelEvent.Description;

      for (var index = 0; index < _state.TravelEvent.Choices.Count; index++)
      {
        var eventChoice = _state.TravelEvent.Choices[index];
        var choice = ChoicePrefab.Create();
        choice.transform.SetParent(ChoicesRoot, false);

        choice.GetComponentInChildren<Text>().text = (index + 1) + ". " + eventChoice.Description;

        var indexLocal = index;
        Action callback = () =>
        {
          _state.MakeChoice(indexLocal);
        };
        _callbacks[index] = callback;

        choice.GetComponentInChildren<Button>().onClick.AddListener(() => { callback(); });

        _buttons.Add(choice);
      }
    }

    private void AddTick(int index)
    {
      _buttons[index].GetComponent<TickView>().AddTick();
    }

    private void Clear()
    {
      Rewards.SetActive(false);
      ClearRoot(RewardsRoot);

      foreach (var button in _buttons)
      {
        Destroy(button);
      }
      _callbacks.Clear();
      _buttons.Clear();
    }
  }
}
