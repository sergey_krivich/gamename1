﻿using System;
using System.Collections.Generic;
using GameName1.Items;
using GameObjectExtensions;
using UnityEngine;

public class InventoryView : UIView
{
  public GameObject ItemPrefab;
  public Transform ItemTransform;

  public event Action<Item> ItemClicked;

  public void OnCloseClicked()
  {
    WindowControl.I.Hide<InventoryView>();
  }

  public void SetItems(List<Item> items)
  {
    ClearRoot(ItemTransform);

    foreach (var item in items)
    {
      var go = ItemPrefab.Create();
      go.transform.SetParent(ItemTransform, false);

      var itemView = go.GetComponent<ItemSlotView>();

      var itemCopy = item;
      itemView.Clicked += () =>
      {
        if (ItemClicked != null)
        {
          ItemClicked(itemCopy);
        }
      };

      itemView.SetItem(item);
    }
  }
}
