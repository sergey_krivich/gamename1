﻿using System;
using GameName1;
using GameName1.Actors;
using GameName1.Items;
using UnityEngine;
using UnityEngine.UI;

public class ItemSlotView : MonoBehaviour
{
  public Image Image;
  public Text Text;
  public Text Description;
  public GameObject BonusesRoot;
  public GameObject BonusesRoot2;

  public StatView Attack;
  public StatView Def;

  public Button Button;
  public Button RemoveButton;

  public event Action Clicked;
  public event Action RemoveClicked;


  public Sprite DefaultSprite;
  public Color DefaultSpriteColor;
  public string DefaultText = "Empty";

  public void OnClicked()
  {
    if(Clicked != null)
      Clicked();
  }

  public void OnRemoveClicked()
  {
    if (RemoveClicked != null)
      RemoveClicked();
  }

  public void SetItem(Item item)
  {
    if (item == null)
    {
      Text.text = DefaultText;
      SetDescription("");
      if (DefaultSprite != null)
      {
        Image.color = DefaultSpriteColor;
        Image.sprite = DefaultSprite;
      }
      else
        Image.enabled = false;

      DeactivateStats();

      if (BonusesRoot != null)
      {
        BonusesRoot.SetActive(false);
        BonusesRoot2.SetActive(false);
      }
      if (RemoveButton != null)
        RemoveButton.gameObject.SetActive(false);
    }
    else
    {
      Image.color = Color.white;
      Image.enabled = true;
      Image.sprite = ResourceManager.LoadSprite(item.Type.Icon);
      Text.text = item.Type.Name;
      SetDescription(item.Type.Description);
      DeactivateStats();
      SetStats(item);

      if (BonusesRoot != null)
      {
        BonusesRoot.SetActive(true);
        BonusesRoot2.SetActive(true);
      }
      if (RemoveButton != null)
        RemoveButton.gameObject.SetActive(true);
    }
  }

  private void DeactivateStats()
  {
    Attack.gameObject.SetActive(false);
    Def.gameObject.SetActive(false);
  }

  private void SetStats(Item item)
  {
    if (item.Type.BonusStatsAdd.Damage > 0f)
    {
      Attack.gameObject.SetActive(true);
      Attack.Text.text = item.Type.BonusStatsAdd.Damage.ToString();
    }
    if (item.Type.BonusStatsAdd.Protection.Get(DamageType.Physical) > 0f)
    {
      Def.gameObject.SetActive(true);
      Def.Text.text = item.Type.BonusStatsAdd.Protection.ToString();
    }
  }

  private void SetDescription(string text)
  {
    if (Description != null)
    {
      Description.text = text;  
    }
  }
}
