﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SLua;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LuaConsole : MonoBehaviour
{
  public InputField Input;

  public GameObject PanelRoot;

  public bool Opened;

  private readonly List<string> _functions = new List<string>();

	IEnumerator Start ()
  {
    while (AppRoot.I.LuaLoader.State == null)
    {
      yield return null;
    }

    var table = AppRoot.I.LuaLoader.State["consoleHelper"] as LuaTable;


    foreach (var kvp in table)
    {
      var key = kvp.key as string;

      _functions.Add(key);
    }

    PanelRoot.SetActive(false);
  }

  void Update ()
  {
    if(!Opened)
      return;

	  if (UnityEngine.Input.GetKeyDown(KeyCode.Return))
	  {
	    var table = AppRoot.I.LuaLoader.State["consoleHelper"] as LuaTable;
	    if (table == null)
	    {
	      Debug.LogError("Console helper not found");
        return;
	    }

	    var values = Input.text.Split(' ');
	    var command = values[0];

	    var param = values.ToList();
      param.RemoveAt(0);

	    object[] paramObj = param.ToArray();

	    table.invoke(command, paramObj);

	    Input.text = "";
    }

    if (UnityEngine.Input.GetKeyDown(KeyCode.Tab))
    {
      foreach (var function in _functions)
      {
        if (function.ToLower().StartsWith(Input.text.ToLower()))
        {
          Input.text = function + " ";
          Input.caretPosition = Input.text.Length;
          break;
        }
      }
    }


    if (UnityEngine.Input.GetKeyDown(KeyCode.BackQuote))
    {
      Opened = !Opened;
      PanelRoot.SetActive(Opened);

      if (Opened)
      {
        Input.ActivateInputField();
        Input.Select();
      }

      Input.text = "";
    }
  }
}
