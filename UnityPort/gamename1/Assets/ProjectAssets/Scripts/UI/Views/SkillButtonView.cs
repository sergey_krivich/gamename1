﻿using GameName1;
using GameName1.Actors;
using GameName1.Lua;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillButtonView : MonoBehaviour
{
  public Button Button;
  public Image Image;
  public Text CooldownText;

  public Color ColorOnCooldown;

  [HideInInspector] public SkillDescriptionView SkillDescriptionView;

  private string _skillId;
  private SkillLuaWrapper _wrapper;
  private CreatureActor _actor;


  void Start()
  {
    EventTrigger trigger = GetComponent<EventTrigger>();
    EventTrigger.Entry entry = new EventTrigger.Entry();
    entry.eventID = EventTriggerType.PointerEnter;
    entry.callback.AddListener((data) => { PointerEnter((PointerEventData) data); });
    trigger.triggers.Add(entry);

    entry = new EventTrigger.Entry();
    entry.eventID = EventTriggerType.PointerDown;
    entry.callback.AddListener((data) => { PointerEnter((PointerEventData) data); });
    trigger.triggers.Add(entry);

    entry = new EventTrigger.Entry();
    entry.eventID = EventTriggerType.PointerExit;
    entry.callback.AddListener(data => { PointerExit((PointerEventData) data); });
    trigger.triggers.Add(entry);

    entry = new EventTrigger.Entry();
    entry.eventID = EventTriggerType.PointerUp;
    entry.callback.AddListener(data => { PointerExit((PointerEventData) data); });
    trigger.triggers.Add(entry);
  }

  public void PointerEnter(PointerEventData data)
  {
    SkillDescriptionView.gameObject.SetActive(true);
    SkillDescriptionView.Set(_skillId);
  }

  public void PointerExit(PointerEventData data)
  {
    SkillDescriptionView.gameObject.SetActive(false);
  }

  public void Set(string skillId)
  {
    var spritePath = AppRoot.I.LuaLoader.SkillsMeta.GetSpriteName(skillId);
    var sprite = ResourceManager.LoadSprite(spritePath);
    Image.sprite = sprite;
    _skillId = skillId;
  }

  public void Set(CreatureActor actor, SkillLuaWrapper wrapper)
  {
    _actor = actor;
    _wrapper = wrapper;
  }

  void Update()
  {
    var rect = GetComponent<RectTransform>();
    rect.sizeDelta = new Vector2(rect.sizeDelta.y, rect.sizeDelta.y);

    UpdateSkillStatus();
  }

  private void UpdateSkillStatus()
  {
    if (CooldownText == null)
      return;

    CooldownText.text = "";
    Image.color = Color.white;

    if (_wrapper != null)
    {
      if (_wrapper.IsOnCooldown())
      {
        CooldownText.text = _wrapper.CooldownLeft.ToString();
        Image.color = ColorOnCooldown;
      }
      else if (_wrapper.GetStaminaUsage() > _actor.Stamina)
      {
        Image.color = ColorOnCooldown;
      }
    }
  }
}
