﻿using System;
using System.Collections.Generic;
using GameName1.Data;
using GameName1.States;
using GameName1.Travel;
using GameObjectExtensions;
using UnityEngine;
using UnityEngine.UI;

public class PartyStatusPlayerElement : MonoBehaviour
{
  public Transform HeroesRoot;
  public GameObject HeroElementPrefab;
  public Text Name;

  public event Action<PlayerProfile, UnitData> HealClicked;

  private List<GameObject> _created = new List<GameObject>();

  private PlayerProfile _playerProfile;

  private PreEventState _state;

  public void Set(PreEventState state, PlayerProfile player)
  {
    _state = state;
    _playerProfile = player;

    Name.text = _playerProfile.Nickname;

    foreach (var unitData in _playerProfile.Creatures)
    {
      var element = HeroElementPrefab.Create();
      element.transform.SetParent(HeroesRoot, false);
      _created.Add(element);


      var heroElement = element.GetComponent<PartyStatusHeroElement>();
      heroElement.Set(player, _state, unitData);
      heroElement.HealClicked += HeroElementHealClicked;
    }
  }

  private void HeroElementHealClicked(UnitData data)
  {
    if (HealClicked != null)
      HealClicked(_playerProfile, data);
  }
}
