﻿using System.Linq;
using GameName1.Data;
using GameName1.Items;
using GameName1.States;
using GameName1.Travel;
using GameObjectExtensions;
using UnityEngine;
using UnityEngine.UI;

public class PartyStatusView : MonoBehaviour
{
  public GameObject PlayerPrefab;
  public Transform PlayersRoot;

  public Text GoldText;
  public Text PotionText;

  private PreEventState _state;

  public void Set(PreEventState preEventState)
  {
    _state = preEventState;
    UpdateView();
  }

  private void UpdateView()
  {
    foreach (var player in AppRoot.I.Players.GetAll())
    {
      var playerElement = PlayerPrefab.Create();
      playerElement.transform.SetParent(PlayersRoot, false);

      var playerElementComp = playerElement.GetComponent<PartyStatusPlayerElement>();
      playerElementComp.Set(_state, player);
      playerElementComp.HealClicked += HealClicked;
    }

    GoldText.text = "Gold: "+ AppRoot.I.Players.AllLocal.Sum(p => p.Inventory.Gold);
    PotionText.text = "Potions: "+ AppRoot.I.Players.AllLocal.Sum(p => p.Inventory.GetItemCountByGroup(ItemGroup.HpPotion));
  }

  private void HealClicked(PlayerProfile player, UnitData unitData)
  {
    _state.HealUnit(player, unitData);
    UpdateView();
  }
}
