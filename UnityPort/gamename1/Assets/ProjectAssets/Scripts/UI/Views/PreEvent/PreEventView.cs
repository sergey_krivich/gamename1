﻿using GameName1.States;
using UnityEngine;

public class PreEventView : MonoBehaviour
{
  private PreEventState _state;
  public void Set(PreEventState state)
  {
    _state = state;
  }

  public void OnAdventureClick()
  {
    _state.ContinueAdventure();
  }
}
