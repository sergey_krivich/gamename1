﻿using System.Linq;
using GameName1.Data;
using GameName1.Items;
using GameName1.Travel;

public class HeroInfoView : UIView
{
  public ItemSlotView Weapon;
  public ItemSlotView Armor;

  private UnitData _unit;
  private PlayerProfile _player;

  public void SetHero(PlayerProfile player, UnitData unit)
  {
    _unit = unit;
    _player = player;

    GetComponentInChildren<SelectedCreatureView>().SetUnit(unit);

    UpdateItems(unit);

    SubscribeClicks();
  }

  private void SubscribeClicks()
  {
    Weapon.Button.onClick.AddListener(() => { OnSlotClicked(Weapon, ItemGroup.Weapon); });
    Armor.Button.onClick.AddListener(() => { OnSlotClicked(Weapon, ItemGroup.Armor); });
  }

  private void UpdateItems(UnitData unit)
  {
    Weapon.SetItem(unit.Equipment.Weapon);
    Armor.SetItem(unit.Equipment.Armor);
  }

  public void OnSlotClicked(ItemSlotView slot, ItemGroup group)
  {
    var inventoryView = WindowControl.I.Show<InventoryView>();

    var selectedItems = _player.Inventory.Items
      .Where(i => i.Type.Group == group && i.Type.IsAvailable(_unit.Type))
      .ToList();

    inventoryView.SetItems(selectedItems);
    inventoryView.ItemClicked += item =>
    {
      var removedItem = _unit.Equipment.EquipItem(item);
      _player.Inventory.AddItem(removedItem);
      _player.Inventory.RemoveItem(item.Id, 1);

      WindowControl.I.Hide<InventoryView>();
      UpdateItems(_unit);
    };
  }

  public void OnCloseClicked()
  {
    WindowControl.I.Hide<HeroInfoView>();
  }
}
