﻿using System;
using GameName1;
using GameName1.Data;
using GameName1.Network.Commands;
using GameName1.States;
using GameName1.Travel;
using UnityEngine;
using UnityEngine.UI;

public class PartyStatusHeroElement : MonoBehaviour
{
  public Text Name;
  public Text Level;
  public Image Sprite;
  public ProgressBar HpBar;
  public Text HpText;

  public event Action<UnitData> HealClicked;

  private UnitData _unitData;
  private PlayerProfile _profile;
  private PreEventState _state;
  private EventDispatcher.EventHandle _eventHandler;

  public void Set(PlayerProfile playerProfile, PreEventState state, UnitData unitData)
  {
    _state = state;
    _profile = playerProfile;
    _state.UnitDataChanged += StateOnUnitDataChanged;
    _unitData = unitData;

    _eventHandler = AppRoot.I.EventDispatcher.Subscribe(
      (HealUnitWithItemCmd.UnitHealedEventData data) =>
      {
        if (data.Unit == _unitData)
          UpdateView();
      });
    UpdateView();
  }

  void OnDisable()
  {
    AppRoot.I.EventDispatcher.Unsubscribe(_eventHandler);
  }

  public void OnOpenInventoryClicked()
  {
    var heroInfoView = WindowControl.I.Show<HeroInfoView>();
    heroInfoView.SetHero(_profile, _unitData);
  }

  public void OnHealClicked()
  {
    if (HealClicked != null)
    {
      HealClicked(_unitData);
    }
  }

  private void StateOnUnitDataChanged(UnitData unitData)
  {
    if (_unitData == unitData)
    {
      UpdateView();
    }
  }

  private void UpdateView()
  {
    Name.text = _unitData.Name;
    Level.text = "Level " + (_unitData.Level + 1) + " - " + _unitData.CurrentXp + "," + _unitData.XpToNextLevel();
    HpBar.SetValue((float)_unitData.CurrentHp / _unitData.Stats.MaxHealth);
    HpText.text = _unitData.CurrentHp + "/" + _unitData.Stats.MaxHealth;
    var sprite = ResourceManager.LoadSprite(_unitData.Sprite);
    Sprite.sprite = sprite;
  }
}
