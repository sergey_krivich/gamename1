﻿using UnityEngine;
using System.Collections;
using Glide;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
  public float ChangeSpeed = 0.5f;
  public EaseType EaseType;

  private Slider _slider;
  private Tween _tween;

  private float _lastValue = -100000;

  void Awake()
  {
    _slider = GetComponent<Slider>();
  }

  public void SetValue(float value)
  {
    if (value == _lastValue)
    {
      return;
    }

    _lastValue = value;

    if (_tween != null)
    {
      _tween.Cancel();
    }


    if (_slider == null)
    {
      return;
    }
    _tween = Tweener.I.Tween(_slider, new {value = value}, ChangeSpeed).Ease(Ease.FromType(EaseType));

  }
}
