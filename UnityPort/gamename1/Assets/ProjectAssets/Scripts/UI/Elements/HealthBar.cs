﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour
{
    public ProgressBar ProgressBar;

    public void SetValue(float current, float max)
    {
        ProgressBar.SetValue(current/max);
    }
}
