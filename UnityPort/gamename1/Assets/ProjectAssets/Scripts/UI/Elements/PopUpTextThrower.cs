﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameObjectExtensions;
using Glide;
using UnityEngine; 

namespace GameName1.UI
{
    internal class PopUpText
    {
        public string Text {
            set { _damageText.Text.text = value; }
        }

        public Vector2 Position
        {
            get { return _damageText.transform.position; }
            set { _damageText.transform.position = value; }
        }
        public Vector2 Direction;
        private float _endSpeed;
        private float _speed;
        private float _changeSpeed;
        private float _changeColorSpeed;
        public Color Color {
            get { return _damageText.Text.color; }
            set { _damageText.Text.color = value; }
        }

        private float _r;
        private float _g;
        private float _b;
        private float _a;


        private Color _endColor;

        public bool Active
        {
            get { return _damageText.gameObject.activeInHierarchy; }
            set
            {
                if (value)
                {
                    Tweener.I.Tween(this, new {_r = _endColor.r, _g = _endColor.g, _b = _endColor.b, _a = _endColor.a}, _changeColorSpeed).Ease(Ease.BackIn);
                }

                _damageText.gameObject.SetActive(value);
            }
        }

        private DamageText _damageText;
        public PopUpText(string text, Color startColor, Color endColor, Vector2 position,
                         Vector2 direction, float startSpeed, float endSpeed, float changeSpeedSpeed, float changeColorSpeed)
        {
            Set(text, startColor, endColor, position, direction, startSpeed, endSpeed, changeSpeedSpeed, changeColorSpeed);
            Active = true;
        }

        public void Set(string text, Color startColor, Color endColor, Vector2 position,
                        Vector2 direction, float startSpeed, float endSpeed, float changeSpeedSpeed, float changeColorSpeed)
        {
            var go = ResourceManager.LoadGO("DamageText");
            go = go.Create();
            _damageText = go.GetComponent<DamageText>();

            Text = text;
            Position = position;
            Direction = direction;
            _endSpeed = endSpeed;
            _speed = startSpeed;
            Color = startColor;
            _endColor = endColor;
            _changeSpeed = changeSpeedSpeed;
            _changeColorSpeed = changeColorSpeed;
                
            _r = Color.r;
            _g = Color.g;
            _b = Color.b;
            _a = Color.a;
        }

        public void Update(float dt)
        {
            if (!Active)
                return;

            _speed = Mathf.Lerp(_speed, _endSpeed, dt * _changeSpeed);
            Position += Direction * _speed*Time.deltaTime*50;
//            if (_speed < 0.2)
//                Color = new Color(Mathf.Lerp(Color.r, _endColor.r, dt * _changeColorSpeed),
//                    Mathf.Lerp(Color.g, _endColor.g, dt * _changeColorSpeed),
//                    Mathf.Lerp(Color.b, _endColor.b, dt * _changeColorSpeed),
//                    Mathf.Lerp(Color.a, _endColor.a, dt * _changeColorSpeed));

            Color = new Color(_r, _g, _b, _a);

            if (_speed < 0.001f)
                _speed = 0;
            if (Color.a < 0.05f)
                Active = false;

            _damageText.Text.color = Color;
        }
    }

    public class PopupTextThrower
    {
        private static PopupTextThrower _instance;
        static public PopupTextThrower I
        {
            get { return _instance; }
        }

        private readonly List<PopUpText> _items;

        static public void Init()
        {
            _instance = new PopupTextThrower();
        }

        protected PopupTextThrower()
        {
            _items = new List<PopUpText>(32);
        }

        public void CreatePopUp(string text, Color startColor, Color endColor, Vector2 position,
                                Vector2 direction, float startSpeed, float endSpeed, float changeSpeedSpeed, float changeColorSpeed)
        {
            for (int i = 0; i < _items.Count; i++)
            {
                if (!_items[i].Active)
                {
                    _items[i].Set(text, startColor, endColor, position, direction, startSpeed, endSpeed, changeSpeedSpeed, changeColorSpeed);
                    _items[i].Active = true;
                    return;
                }
            }
            _items.Add(new PopUpText(text, startColor, endColor, position, direction, startSpeed, endSpeed, changeSpeedSpeed, changeColorSpeed));
        }

        public void Update()
        {
            foreach (var popUpText in _items)
            {
                popUpText.Update(Time.deltaTime);
            }
        }

        public void Clear()
        {
            _items.Clear();
        }
    }
}
