﻿using UnityEngine;

namespace GameObjectExtensions
{
    public static class GameObjectExtension
    {
        public static GameObject Create(this GameObject gameObject)
        {
            return GameObject.Instantiate(gameObject);
        }

        public static T Create<T>(this GameObject gameObject) where T:MonoBehaviour
        {
            return GameObject.Instantiate(gameObject).GetComponent<T>();
        }

        #region Rotation / Local Rotation
        public static void SetRotation(this GameObject go, Quaternion rot)
        {
            go.transform.rotation = rot;
        }

        public static Quaternion GetRotation(this GameObject go)
        {
            return go.transform.rotation;
        }

        public static void SetLocalRotation(this GameObject go, Quaternion rot)
        {
            go.transform.localRotation = rot;
        }

        public static Quaternion GetLocalRotation(this GameObject go)
        {
            return go.transform.localRotation;
        }

        public static void SetRotation(this Component c, Quaternion rot)
        {
            c.transform.rotation = rot;
        }

        public static Quaternion GetRotation(this Component c)
        {
            return c.transform.rotation;
        }

        public static void SetLocalRotation(this Component c, Quaternion rot)
        {
            c.transform.localRotation = rot;
        }

        public static Quaternion GetLocalRotation(this Component c)
        {
            return c.transform.localRotation;
        }

        #endregion

        #region Position
        public static void SetPosition(this GameObject go, Vector3 pos)
        {
            go.transform.position = pos;
        }

        public static Vector3 GetPosition(this GameObject go)
        {
            return go.transform.position;
        }

        public static void SetPosition(this Component c, Vector3 pos)
        {
            c.transform.position = pos;
        }

        public static Vector3 GetPosition(this Component c)
        {
            return c.transform.position;
        }
        #endregion

        #region Local position
        public static void SetLocalPosition(this GameObject go, Vector3 pos)
        {
            go.transform.localPosition = pos;
        }

        public static Vector3 GetLocalPosition(this GameObject go)
        {
            return go.transform.localPosition;
        }

        public static void SetLocalPosition(this Component c, Vector3 pos)
        {
            c.transform.localPosition = pos;
        }

        public static Vector3 GetLocalPosition(this Component c)
        {
            return c.transform.localPosition;
        }
        #endregion
    }
}
