﻿using UnityEngine;

public static class TransformExtension
{
    public static void LookAt2D(this Transform transform, Vector3 position)
    {
        var relative = transform.InverseTransformPoint(position);
        var angle = Mathf.Atan2(relative.y, relative.x) * Mathf.Rad2Deg;
        transform.Rotate(0, 0, angle);
    }

    public static Quaternion GetLookAt2DRotation(this Transform transform, Vector3 position)
    {
        var relative = position - transform.position;
        var angle = Mathf.Atan2(relative.y, relative.x) * Mathf.Rad2Deg;
        return Quaternion.Euler(0, 0, angle);
    }
}
