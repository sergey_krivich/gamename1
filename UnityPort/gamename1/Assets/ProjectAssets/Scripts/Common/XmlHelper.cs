﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using GameName1.Items;
using UnityEngine;

namespace GameName1
{
    public static class XmlHelper
    {
        public static XElement CreateChild(XElement node, string childName)
        {
            var newNode = new XElement(childName);
            node.Add(newNode);
            return newNode;
        }
        public static void WriteSimple<T>(XElement root, string name, T value)
        {
            XElement element = new XElement(name);

            element.SetValue(value);

            root.Add(element);
        }

        public static void WriteVector2(XElement root, string name, Vector2 vector2)
        {
            XElement element = CreateChild(root, name);

            WriteSimple(element, "X", vector2.x);
            WriteSimple(element, "Y", vector2.y);
        }

        public static void WriteBool(XElement root, string name, bool value)
        {
            WriteSimple(root, name, value);
        }




        public static T ReadValue<T>(XElement root, string name, Func<string, T> convertor)
        {
            XElement element = GetElement(root, name);
            if (element == null)
            {
                return default(T);
            }

            return convertor(element.Value);
        }

        public static float ReadFloat(XElement root, string name)
        {
            return ReadValue<float>(root, name, float.Parse);
        }

        public static Vector2 ReadVector2(XElement root, string name)
        {
            var vectorRoot = GetChild(root, name);

            var x = ReadFloat(vectorRoot, "X");
            var y = ReadFloat(vectorRoot, "Y");

            return new Vector2(x, y);
        }

        public static string ReadString(XElement root, string name)
        {
            XElement element = GetElement(root, name);
            if (element == null)
            {
                return null;
            }

            return element.Value;
        }

        private static XElement GetElement(XElement root, string name)
        {
            var xname = XName.Get(name);
            var element = root.Element(xname);
            if (element == null)
            {
                return element;
            }
            return element;
        }

        public static bool ReadBool(XElement root, string name)
        {
            return ReadValue<bool>(root, name, bool.Parse);
        }

        public static XElement GetChild(XElement root, string name)
        {
            return root.Element(name);
        }

        public static List<XElement> GetChildren(XElement root, string name)
        {
            return root.Elements(name).ToList();
        }

        public static int ReadType(XElement root, string name)
        {
            return ReadValue<int>(root, name, int.Parse);
        }

        public static int ReadInt(XElement root, string name)
        {
            return ReadValue<int>(root, name, int.Parse);
        }

        public static T ReadClass<T>(XElement node) where T:class
        {
            var typeName = GetElement(node, "Type").Value;
            var type = Type.GetType(typeName);

            var result = Activator.CreateInstance(type) as T;
            return result;
        }        

        public static void WriteClass(XElement root, Type type)
        {
            WriteSimple(root, "Type", type.FullName);
        }

        public static void WriteType(XElement root, string name, int type)
        {
            WriteSimple(root, name, type);
        }
    }
}
