using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SLua;
using UnityEngine.SceneManagement;

public partial class AppRoot
{
  private readonly Stack<StateBase> _previousStates = new Stack<StateBase>();
  private StateBase _currentState;
  private AsyncOperation _aoLoadScene;


  public void SetState(StateBase state, bool canGoBack = false)
  {
    if (canGoBack)
    {
      if (_currentState != null)
      {
        _previousStates.Push(_currentState);
      }
    }

    StartCoroutine(SetStateCoroutine(state));
  }

  public void GoToPreviousState()
  {
    if (_previousStates.Any())
    {
      SetState(_previousStates.Pop());
    }
  }

  public void InjectPreviousState(StateBase type)
  {
    _previousStates.Push(type);
  }

  public void ResetPreviousStates()
  {
    _previousStates.Clear();
  }


  public void OnApplicationPause(bool pauseStatus)
  {
    if (_currentState != null)
    {
      _currentState.OnApplicationPause(pauseStatus);
    }
  }

  public void OnApplicationQuit()
  {
    if(_currentState != null)
      _currentState.OnApplicationQuit();
  }

  private IEnumerator SetStateCoroutine(StateBase state)
  {
    //
    if (_currentState != null)
    {
      _currentState.Deactivate();
      _currentState = null;
    }


    if (!string.IsNullOrEmpty(state.StateSceneName))
    {
      if (SceneManager.GetActiveScene().name != state.StateSceneName)
      {
        string curState = state.StateSceneName;
        _aoLoadScene = SceneManager.LoadSceneAsync(curState);
        yield return _aoLoadScene;
        _aoLoadScene = null;
      }
    }
    Debug.Log("Actgivate state " + state.GetType().Name);

    _currentState = state;
    _currentState.Activate();
  }

  private void UpdateStates()
  {
    if (_currentState != null)
    {
      _currentState.Update();
    }
  }

  private void OnSceneLoadedStates()
  {
    if (_currentState != null)
    {
      _currentState.OnSceneLoaded();
    }
  }

  public StateBase CurrentState
  {
    get { return _currentState; }
  }

  public LuaTable GetLuaTable()
  {
   return new LuaTable(LuaLoader.State);
  }
}
