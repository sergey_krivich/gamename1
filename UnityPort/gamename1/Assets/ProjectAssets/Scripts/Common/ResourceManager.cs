﻿using UnityEngine;

namespace GameName1
{
  public class ResourceManager
  {
    public static GameObject LoadGO(string resource)
    {
      var go = Load<GameObject>("Prefabs/" + resource);
      return go;
    }

    public static Sprite LoadSprite(string resource)
    {
      var sprite = Load<Sprite>("Graphics/Sprites/" + resource);
      return sprite;
    }

    public static RuntimeAnimatorController LoadAnimator(string resource)
    {
      var animator = Load<RuntimeAnimatorController>("Graphics/Sprites/" + resource);
      return animator;
    }

    public static T Load<T>(string resource) where T : UnityEngine.Object
    {
      var obj = Resources.Load<T>(resource);
      return obj;
    }

  }
}
