﻿using System;
using System.Collections.Generic;
using UnityEngine.Assertions;


public class EventDispatcher
{
  public class EventHandle
  {
    public Type Type;
  }

  private readonly Dictionary<Type, Dictionary<EventHandle, Action<object>>> _subscribers;
  private readonly Dictionary<Type, List<object>> _storedEvents;

  public EventDispatcher()
  {
    _subscribers = new Dictionary<Type, Dictionary<EventHandle, Action<object>>>();
    _storedEvents = new Dictionary<Type, List<object>>();
  }

  public EventHandle Subscribe<T>(Action<T> callback, bool purgeStored = true) where T : class
  {
    var handler = new EventHandle { Type = typeof(T) };

    if (!_subscribers.ContainsKey(typeof(T)))
    {
      _subscribers[typeof(T)] = new Dictionary<EventHandle, Action<object>>();
    }

    _subscribers[typeof(T)][handler] = p =>
    {
      callback(p as T);
    };

    PurgeStoredEvents(callback, purgeStored);

    return handler;
  }

  private void PurgeStoredEvents<T>(Action<T> callback, bool purgeStored) where T : class
  {
    if (purgeStored)
    {
      if (_storedEvents.ContainsKey(typeof(T)))
      {
        var stored = _storedEvents[typeof(T)];

        foreach (var data in stored)
        {
          callback(data as T);
        }
      }
    }
  }

  public void Unsubscribe(EventHandle handle)
  {
    Assert.AreNotEqual(handle, null);

    var callbacks = _subscribers[handle.Type];
    if (callbacks != null)
    {
      callbacks.Remove(handle);
    }
  }

  public void Post<T>(T data, bool stored = false) where T : class
  {
    var type = typeof(T);
    bool posted = false;
    Dictionary<EventHandle, Action<object>> handlers = null;

    if (_subscribers.ContainsKey(type))
    {
      handlers = _subscribers[type];
    }

    if (handlers != null)
    {
      var copy = new Dictionary<EventHandle, Action<object>>(handlers);
      foreach (var keyValuePair in copy)
      {
        var callback = keyValuePair.Value;
        callback(data);
        posted = true;
      }
    }

    TryStoreEvent(data, stored, posted);
  }

  private void TryStoreEvent<T>(T data, bool stored, bool posted) where T : class
  {
    Type type = typeof(T);
    if (!posted && stored)
    {
      List<object> storedEvent = null;

      if (_storedEvents.ContainsKey(type))
      {
        storedEvent = _storedEvents[type];
      }

      if (storedEvent == null)
      {
        storedEvent = new List<object>();
        _storedEvents.Add(type, storedEvent);
      }

      storedEvent.Add(data);
    }
  }
}
