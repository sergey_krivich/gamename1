﻿using System.Collections.Generic;
using System.Xml.Linq;
using GameName1.Actors;
using GameName1.Fight;
using GameName1.Levels;

namespace GameName1
{
  public class GameSave
  {
    public void Save(Level level, List<Actor> actors, List<FightPlayer> players, List<FightPlayer> fightPlayers = null)
    {
    }


    public void Load()
    {
    }
  }

  public interface ISaveable
  {
    void Save(XElement node);
    void Load(XElement node);
  }
}
