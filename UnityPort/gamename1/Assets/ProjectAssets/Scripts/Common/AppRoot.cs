﻿using System;
using GameName1;
using GameName1.Data;
using GameName1.Lua;
using GameName1.Network;
using GameName1.States;
using GameName1.Travel;
using Glide;
using UnityEngine;
using UnityEngine.SceneManagement;

public partial class AppRoot : MonoBehaviour
{
  public LuaRootLoader LuaLoader { get; private set; }
  public EventDispatcher EventDispatcher { get; private set; }
  public Players Players { get; private set; }
  public GameStateData GameStateData { get; set; }

  private static AppRoot _instance;

  void Awake()
  {
    _instance = this;


    LuaLoader = new LuaRootLoader();
    LuaLoader.LoadFinished += LuaLoaderOnLoadFinished;
    LuaLoader.Init();

    RestartGame();

    SceneManager.sceneLoaded+= SceneManagerOnSceneLoaded;
  }

  private void LuaLoaderOnLoadFinished()
  {
    SetState(new StateMainMenu());
  }

  public void Start()
  {
    Sprites.Init();
  }

  public void Update()
  {
    Dispatcher.InvokePending();
    Tweener.I.Update(Time.deltaTime);
    UpdateStates();

    if (Input.GetKeyDown(KeyCode.R))
    {
      LuaLoader.Reload();
    }
  }

  public void LateUpdate()
  {
  }

  public void RestartGame()
  {
    EventDispatcher = new EventDispatcher();
    Players = new Players();
    NetworkManager.I.Stop();
    LuaLoader.CleanCache();

    GameStateData = new GameStateData();
  }

  void OnDestroy()
  {
    NetworkManager.I.Stop();
  }

  private void SceneManagerOnSceneLoaded(Scene arg0, LoadSceneMode loadSceneMode)
  {
    OnSceneLoadedStates();
  }

  public static AppRoot I
  {
    get { return _instance; }
  }

  public ConfigLuaWrapper Config {
    get { return LuaLoader.ConfigLoader; }
  }
}
