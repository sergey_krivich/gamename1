﻿Shader "Sprites/Water2dReflection"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_ReflectionTexture ("Texture", 2D) = "white" {}
		_BumpMap ("Normalmap", 2D) = "bump" {}

		_Color ("Tint", Color) = (1,1,1,1)
		 _ReflectionAmount ("Reflection Amount", Range(0,10)) = 0.5
		 _BumpAmount ("Bump", Range(0,10)) = 0.5


		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite On
		Blend One OneMinusSrcAlpha

		CGPROGRAM
		#pragma surface surf Lambert vertex:vert nofog keepalpha
		#pragma multi_compile _ PIXELSNAP_ON

		sampler2D _MainTex;
		sampler2D _ReflectionTexture;
		sampler2D _BumpMap;


		fixed4 _Color;
		fixed _ReflectionAmount;
		fixed _BumpAmount;

		struct Input
		{
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float3 screenPos;

			fixed4 color;
		};
		
		void vert (inout appdata_full v, out Input o)
		{
			#if defined(PIXELSNAP_ON)
			v.vertex = UnityPixelSnap (v.vertex);
			#endif
			
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.color = v.color * _Color;
		}

		void surf (Input IN, inout SurfaceOutput o)
		{
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap +_Time*0.1f));
			o.Normal += UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap -_Time*0.1f));
			o.Normal = normalize(o.Normal);
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex+o.Normal*_BumpAmount) * IN.color;
			fixed4 reflection = tex2D(_ReflectionTexture, IN.screenPos.xy*1.5 + o.Normal*_BumpAmount) * IN.color;
			o.Albedo = c.rgb * c.a;
			o.Alpha = c.a;
			o.Emission = reflection*_ReflectionAmount;

			//o.normal = float3(0.7,0.7,0.7);
		}
		ENDCG
	}

Fallback "Transparent/VertexLit"
}
