// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/TrailShader" {
    Properties {
		_Texture ("Texture", 2D) = "white" {} 

		_StartWidth ("Trail start width", Float) = 0.5
		_EndWidth ("Trail end width", Float) = 0.5

		_FadeTime ("Fade out time", Float) = 3.0

		_startColor		("Start color", Color)  = (1,1,1,1) 
		_startColorTime ("Start color time", Float) = 0.0
		_endColor		("End color", Color)  = (1,1,1,1) 
		_endColorTime	("End color time", Float) = 1.0
		
		_startAlpha		("Start alpha", Float) = 1.0
		_startAlphaTime ("Start alpha time", Float) = 0.0
		_endAlpha		("End alpha", Float) = 1.0
		_endAlphaTime	("End alpha time", Float) = 1.0
    }
    SubShader {
		Tags { "Queue"="Transparent" "RenderType" = "Transparent" }
        Pass {
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			AlphaTest Greater 0.5
			ZWrite Off

            CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			sampler2D _Texture;
			float _StartWidth;
			float _EndWidth;
			float _FadeTime;

			float3 _startColor;
			float _startColorTime;
			float3 _endColor;
			float _endColorTime;
							
			float _startAlpha;
			float _startAlphaTime;
			float _endAlpha;
			float _endAlphaTime;

			struct appdata {
				float3 prevPos : POSITION;
				float4 currPos : TANGENT;
				float4 nextPos : NORMAL;
				float2 offsetAndTime : TEXCOORD0;
			};

			struct v2f {
				float4 pos:SV_POSITION;
				float2 uv : TEXCOORD0;
				float2 period : TEXCOORD1;
				float4 color : COLOR;
			};

			float4 ttransform(float3 coord) : COLOR
			{
				return UnityObjectToClipPos(  float4(coord.x,coord.y,coord.z, 1.0) );
			}
			float2 project(float4 device){
				float3 device_normal = device.xyz/device.w;
				float2 clip_pos = (device_normal*0.5+0.5).xy;
				return clip_pos * _ScreenParams.xy;
			}

			v2f vert(appdata v)
			{
				v2f o;

				float period = (_Time.y - v.offsetAndTime.y) / _FadeTime;
				o.period = float2(period, 0);

				float2 sLast = project(ttransform(v.prevPos.xyz));
				float2 sNext = project(ttransform(v.nextPos.xyz));
				float2 sCurrent = project(ttransform(v.currPos.xyz));

				float2 normal1 = normalize(sLast - sCurrent);
				float2 normal2 = normalize(sCurrent - sNext);
				float2 normal = normalize(normal1 + normal2);
				float width = lerp(_StartWidth, _EndWidth, period);
				float3 pos = v.currPos.xyz + normalize(-cross(float3(normal, 0.0), float3(0.0, 0.0, 1.0))) * v.currPos.w * width;
				o.pos = ttransform(pos);
				float off = v.currPos.w * 0.5 + 0.5;
				o.uv = float2(off, v.offsetAndTime.x);

				float colorInterpolationValue = min(1, max(0, period - _startColorTime) / (_endColorTime - _startColorTime));
				float alphaInterpolationValue = min(1, max(0, period - _startAlphaTime) / (_endAlphaTime - _startAlphaTime));

				float3 rColor = lerp(_startColor, _endColor, colorInterpolationValue);
				float rAlpha = lerp(_startAlpha, _endAlpha, alphaInterpolationValue);
				o.color = float4(rColor, rAlpha);

				return o;
			}

			half4 frag(v2f i) : COLOR
			{ 
				half4 trailColor = tex2D(_Texture, float2(i.uv.x, frac(i.uv.y)));
				return trailColor * i.color;
			} 

			ENDCG
        }
    }
}