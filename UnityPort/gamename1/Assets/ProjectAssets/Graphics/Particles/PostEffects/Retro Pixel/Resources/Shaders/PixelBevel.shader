﻿Shader "AlpacaSound/PixelBevel"
{
	Properties
	{
		_Color0 ("Color 0", Color) = (0.00, 0.00, 0.00, 1.0)	
		_Color1 ("Color 1", Color) = (0.14, 0.14, 0.14, 1.0)	
	 	_MainTex ("", 2D) = "white" {}

		_TiledTex ("Tiled Texture", 2D) = "white" {}
				_ResolutionX ("Resolution X", Float) = 160
		_ResolutionY ("Resolution Y", Float) = 120
	}
	 
	SubShader
	{
		Lighting Off
		ZTest Always
		Cull Off
		ZWrite Off
		Fog { Mode Off }
	 
	 	Pass
	 	{
	  		CGPROGRAM
	  		#pragma exclude_renderers flash
	  		#pragma vertex vert_img
	  		#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
	  		#include "UnityCG.cginc"
	    
			uniform fixed4 _Color0;
			uniform fixed4 _Color1;
	  		uniform sampler2D _MainTex;
	  		uniform sampler2D _TiledTex;

			uniform fixed _ResolutionX;
			uniform fixed _ResolutionY;
	    
	  		fixed4 frag (v2f_img i) : COLOR
	  		{
	   			fixed4 original = tex2D (_MainTex, i.uv);
	   			fixed4 original2 = tex2D (_MainTex, i.uv + fixed2(0.003, 0));
	   			fixed4 original3 = tex2D (_MainTex, i.uv + fixed2(-0.003, 0));
	   			fixed4 original4 = tex2D (_MainTex, i.uv + fixed2(0, 0.003));


				i.uv.x = i.uv.x*_ResolutionX;
				i.uv.y = i.uv.y*_ResolutionY;
	   			fixed4 tiled = tex2D (_TiledTex, i.uv.xy);

	   			//fixed dist0 = distance (original, _Color0.rgb);
	   			//fixed dist1 = distance (original, _Color1.rgb);
	   			
	   			//fixed4 col = fixed4 (0,0,0,0);
	   			//fixed dist = 10.0;

				//if (dist0 < dist)
				//{
				//	dist = dist0;
				//	col = _Color0;
				//}
				
				//if (dist1 < dist)
				//{
				//	dist = dist1;
				//	col = _Color1;
				//}

				fixed p =(original.x + original.y + original.z)/3;
				fixed4 r = original;
				    r = original*2-tiled.a*tiled*1.5*_Color1*original.r;
				return  r;
	  		}
	  		
	  		ENDCG
	 	}
	}
	
	FallBack "Diffuse"
}
