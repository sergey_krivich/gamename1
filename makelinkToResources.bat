REM RMDIR UnityPort\gamename1\Assets\StreamingAssets\Scripts
RMDIR UnityPort\gamename1\Assets\Resources\Scripts /s /q
mkdir UnityPort\gamename1\Assets\Resources\Scripts

xcopy UnityPort\gamename1\Assets\StreamingAssets\Scripts\*.lua UnityPort\gamename1\Assets\Resources\Scripts /s /e /h
REM for /R %x in (UnityPort\gamename1\Assets\Resources\Scripts\*.lua) do ren "%x" *.txt

cd UnityPort\gamename1\Assets\Resources\Scripts\
forfiles /S /M *.lua /C "cmd /c rename @file @fname.txt"