# README #

This is a turn based strategy roguelike game with coop support.
It is created in unity3d, written in C# for main logic and lua for ai, skills and events.

To run: generate lua wrapper code by slua menu, run makelinkToResources if making android build or makelinkToStreamingAssets if windows.